package org.vtokarev.dto;

import org.vtokarev.domain.Room;
import org.vtokarev.domain.RoomClass;

import java.util.List;

/**
 * The {@code RoomEditDto} class is a data transfer object, that encapsulates {@link Room}, {@link RoomClass}
 * and calculated field {@code List<String> images}, which contains an actual list of images of the room.
 * The DTO is used for transferring data between layers.
 * Persistence -> DAO -> service -> controller({@link org.vtokarev.controller.RoomListServlet}) -> view({@code roomList.jsp}).
 *
 * @author Vyacheslav Tokarev
 */


public class RoomEditDto {
    private Room room;
    private RoomClass roomClass;

    private List<String> images;


    public RoomEditDto() {
    }

    public RoomEditDto(Room room, RoomClass roomClass, List<String> images) {
        this.room = room;
        this.roomClass = roomClass;
        this.images = images;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public RoomClass getRoomClass() {
        return roomClass;
    }

    public void setRoomClass(RoomClass roomClass) {
        this.roomClass = roomClass;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }
}

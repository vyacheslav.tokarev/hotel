package org.vtokarev.dao;

import org.vtokarev.domain.RoomRequest;
import org.vtokarev.dto.RoomRequestDto;

import java.sql.Connection;
import java.time.Instant;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * {@code RoomRequestDao} interface extends {@code GenericDao} interface.
 * It defines the methods for operations with room requests on a persistence layer.
 *
 * @author Vyacheslav Tokarev
 */


public interface RoomRequestDao extends GenericDao<RoomRequest> {
    List<RoomRequestDto> getRequestsByUserId(Long userId);
    void update(RoomRequest roomRequest, Connection connection);
    Optional<RoomRequest> get(long id, Connection connection);
    int updateBreachedNewRoomRequestStatus(LocalDate checkin);


    }

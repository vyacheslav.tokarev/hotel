package org.vtokarev.domain;

import java.io.Serializable;

/**
 * The {@code RoomRequestStatus} class represents the entity of status of the {@link RoomRequest}.
 * {@code NEW} means that the {@link RoomRequest} was created and the guest is waiting that it will be processed.
 * {@code PROVIDED} means that the {@link RoomRequest} was processed and room is provided hence the booking is created.
 * {@code CANCELLED} - the {@link RoomRequest} was canceled by the guest or by a manager due to no available rooms.
 *
 * @author Vyacheslav Tokarev
 */


public enum RoomRequestStatus implements Serializable {
    NEW,

    PROVIDED,
    CANCELLED
}

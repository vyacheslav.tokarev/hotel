package org.vtokarev.dao.mysql;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.vtokarev.dao.RoomClassDao;
import org.vtokarev.domain.RoomClass;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RoomClassDaoTests {

    @RegisterExtension
    public static SqlMemoryDatabase sqlMemoryDatabase = new SqlMemoryDatabase();


    @BeforeEach
    void init() throws SQLException {
        String initSql = "INSERT INTO room_classes VALUES (default, 'Standard'),(default, 'Superior'),(default, 'Studio'),(default, 'Suite')";
        Connection conn = sqlMemoryDatabase.dataSource().getConnection();
        Statement stmt = conn.createStatement();
        stmt.executeUpdate(initSql);
        stmt.close();
        conn.close();
    }

    @Test
    void getAll_test(){
        RoomClassDao roomClassDao= new RoomClassDaoImpl(sqlMemoryDatabase.dataSource());
        List<RoomClass> list = roomClassDao.getAll();
        assertEquals(4, list.size());
    }

    @Test
    void get_test(){
        RoomClassDao roomClassDao= new RoomClassDaoImpl(sqlMemoryDatabase.dataSource());
        Optional<RoomClass> rc1 = roomClassDao.get(1);
        assertTrue(rc1.isPresent());
        assertEquals("Standard", rc1.get().getName());

        Optional<RoomClass> rc2 = roomClassDao.get(2);
        assertTrue(rc2.isPresent());
        assertEquals("Superior", rc2.get().getName());

        Optional<RoomClass> rc3 = roomClassDao.get(3);
        assertTrue(rc3.isPresent());
        assertEquals("Studio", rc3.get().getName());

        Optional<RoomClass> rc4 = roomClassDao.get(4);
        assertTrue(rc4.isPresent());
        assertEquals("Suite", rc4.get().getName());
    }
}

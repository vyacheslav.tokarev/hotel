<%@ include file="header.jsp" %>
<%--@elvariable id="rooms" type="java.util.List<org.vtokarev.dto.RoomSearchDto>"--%>
<%--@elvariable id="userId" type="int"--%>
<%--@elvariable id="requestId" type="int"--%>
<%--@elvariable id="checkin" type="java.time.LocalDate"--%>
<%--@elvariable id="checkout" type="java.time.LocalDate"--%>
<%--@elvariable id="errorMessage" type="java.lang.String"--%>
<%--@elvariable id="emptyResult" type="boolean"--%>
<%--@elvariable id="pages" type="java.util.List<Integer>"--%>
<%--@elvariable id="page" type="int"--%>
<div class="row">
    <div class="col s12">
        <div class="right">
            <c:if test="${rooms.size() > 0}">
                <ul class="pagination">
                    <c:if test="${param.page == 1}">
                        <li class="disabled"><a
                                href="proceedRequest?page=${param.page}&requestId=${param.requestId}&userId=${param.userId}"><i
                                class="material-icons">chevron_left</i></a></li>
                    </c:if>
                    <c:if test="${param.page > 1}">
                        <li class="waves-effect"><a
                                href="proceedRequest?page=${param.page - 1}&requestId=${param.requestId}&userId=${param.userId}"><i
                                class="material-icons">chevron_left</i></a></li>
                    </c:if>
                    <c:forEach items="${pages}" var="p">
                        <c:if test="${p == page}">
                            <li class="active cyan darken-3"><a
                                    href="proceedRequest?page=${p}&requestId=${param.requestId}&userId=${param.userId}">${p}</a>
                            </li>
                        </c:if>
                        <c:if test="${p != page}">
                            <li class="waves-effect"><a
                                    href="proceedRequest?page=${p}&requestId=${param.requestId}&userId=${param.userId}">${p}</a>
                            </li>
                        </c:if>
                    </c:forEach>
                    <c:if test="${param.page == pages.size()}">
                        <li class="disabled"><a
                                href="proceedRequest?page=${param.page}&requestId=${param.requestId}&userId=${param.userId}"><i
                                class="material-icons">chevron_right</i></a></li>
                    </c:if>
                    <c:if test="${param.page < pages.size()}">
                        <li class="waves-effect"><a
                                href="proceedRequest?page=${param.page + 1}&requestId=${param.requestId}&userId=${param.userId}"><i
                                class="material-icons">chevron_right</i></a></li>
                    </c:if>
                </ul>
            </c:if>
        </div>
    </div>
    <div class="col s12">
        <c:if test="${emptyResult && param.page == 1}">
            <br><br><br>
            <div class="row">
                <div class="my-card col s4 offset-s4 z-depth-1-half">
                    <div class="container-fluid">
                        <form action="bookRoomManager" method="post">
                            <p><fmt:message key="${errorMessage}"/></p>
                            <div class="input-field">
                                <input type="hidden" name="empty" value="true">
                                <input type="hidden" name="requestId" value="${requestId}">
                                <textarea id="managerNotesEmpty" name="managerNotes"
                                          class="materialize-textarea">${param.guestNotes}</textarea>
                                <label for="managerNotesEmpty"><fmt:message key="notesForGuest.label"/></label>
                            </div>
                            <div class="input">
                                <button class="btn-small waves-effect waves-light red right" type="submit" name="book"
                                        value="true">
                                    <fmt:message key="cancelRequest.label"/>
                                </button>
                                <br>
                            </div>
                        </form>
                        <br>
                    </div>
                </div>
            </div>
        </c:if>
        <c:if test="${not empty errorMessage && not emptyResult}">
            <br><br><br><br><br><br>
            <div class="col s6 offset-s3">
                <blockquote class="red-text">
                    <h6><fmt:message key="${errorMessage}"/></h6>
                </blockquote>
            </div>
        </c:if>
        <c:forEach items="${rooms}" var="roomDto">
            <div class="my-card col s3 z-depth-1-half">
                <div class="s12">
                    <h6>${roomDto.room.name}</h6><br>
                    <span><fmt:message key="number.booking"/>: <b>${roomDto.room.number}</b></span><br>
                    <span><fmt:message key="roomClass.label"/>: ${roomDto.roomClassName}</span><br>
                    <span><fmt:message key="maxPersons.label"/>: ${roomDto.room.capacity}</span><br>
                    <span><fmt:message key="description.label"/>: ${roomDto.room.description}</span><br>
                    <h6><fmt:message key="price.label"/>: ${roomDto.room.price}</h6><br>
                </div>
                <div class="row">
                    <div class="s12">
                        <c:forEach items="${roomDto.images}" var="img">
                            <div class="col s2 my-img">
                                <img class="materialboxed" width="60" src="${img}" alt="">
                            </div>
                        </c:forEach>
                    </div>
                </div>
                <form action="bookRoomManager" method="post">
                    <input type="hidden" name="userId" value="${userId}">
                    <input type="hidden" name="requestId" value="${requestId}">
                    <input type="hidden" name="roomId" value="${roomDto.room.id}">
                    <input type="hidden" name="price" value="${roomDto.room.price}">
                    <input type="hidden" name="checkin" value="${checkin}">
                    <input type="hidden" name="checkout" value="${checkout}">
                    <div class="input-field">
                        <textarea id="managerNotes" name="managerNotes"
                                  class="materialize-textarea">${param.guestNotes}</textarea>
                        <label for="managerNotes"><fmt:message key="notesForGuest.label"/></label>
                    </div>
                    <div class="row">
                        <div class="s12">
                            <div class="col s4 left">
                                <h6 class="green-text"><b><fmt:message key="${roomDto.status}"/> </b></h6>
                            </div>
                            <div class="col s8 right">
                                <button class="btn-small waves-effect waves-light cyan darken-3 right" type="submit"
                                        name="book"
                                        value="true"><fmt:message key="book.action"/>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </c:forEach>
    </div>
</div>
<script>
    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('.materialboxed');
        var instances = M.Materialbox.init(elems);
    });
</script>
<%@ include file="footer.jsp" %>
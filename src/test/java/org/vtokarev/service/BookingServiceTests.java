package org.vtokarev.service;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.vtokarev.dao.BookingDao;
import org.vtokarev.domain.Booking;
import org.vtokarev.domain.BookingStatus;
import org.vtokarev.dto.BookingDto;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.params.provider.Arguments.arguments;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class BookingServiceTests {


    @ParameterizedTest
    @MethodSource("updateBookingStatusById_test_cases")
    void updateBookingStatusById_test(long bookingId, BookingStatus bookingStatusBefore, BookingStatus bookingStatus) {
        Booking booking = new Booking();
        booking.setStatus(bookingStatusBefore);

        BookingDao bookingDaoMock = mock(BookingDao.class);
        when(bookingDaoMock.get(bookingId)).thenReturn(Optional.of(booking));
        BookingService bookingService = new BookingService(bookingDaoMock, ZoneOffset.UTC);
        bookingService.updateBookingStatusById(bookingId, bookingStatus);
        assertEquals(bookingStatus, booking.getStatus());
    }

    static Stream<Arguments> updateBookingStatusById_test_cases() {
        Random r = new Random();

        return Stream.of(
                arguments(r.nextLong(Long.MAX_VALUE), BookingStatus.BOOKED, BookingStatus.RESERVED),
                arguments(r.nextLong(Long.MAX_VALUE), BookingStatus.RESERVED, BookingStatus.BOOKED),
                arguments(r.nextLong(Long.MAX_VALUE), BookingStatus.CANCELLED, BookingStatus.RESERVED),
                arguments(r.nextLong(Long.MAX_VALUE), BookingStatus.BOOKED, BookingStatus.BOOKED)
        );
    }

    @ParameterizedTest
    @MethodSource("calcBookingDtoTimeLeft_RESERVED_status_test_cases")
    void calcBookingDtoTimeLeft_RESERVED_status_test(String expected, BookingDto bookingDto) {
        BookingDao bookingDao = mock(BookingDao.class);
        BookingService bookingService = new BookingService(bookingDao, ZoneOffset.UTC);

        String[] time = expected.split(":");

        int expectedInSeconds = Integer.parseInt(time[0]) * 3600 +
                Integer.parseInt(time[1]) * 60 + Integer.parseInt(time[2]);

        time = bookingService.calcBookingDtoTimeLeft(bookingDto).getTimeLeft().split(":");

        int actualInSeconds = Integer.parseInt(time[0]) * 3600 +
                Integer.parseInt(time[1]) * 60 + Integer.parseInt(time[2]);

        assertEquals(expectedInSeconds, actualInSeconds, 2);
    }

    static Stream<Arguments> calcBookingDtoTimeLeft_RESERVED_status_test_cases() {
        BookingDto dto1 = new BookingDto();
        Booking b1 = new Booking();
        b1.setStatusChanged(Instant.now().minus(5, ChronoUnit.HOURS));
        b1.setStatus(BookingStatus.RESERVED);
        dto1.setBooking(b1);

        BookingDto dto2 = new BookingDto();
        Booking b2 = new Booking();
        b2.setStatusChanged(Instant.now().minus(1, ChronoUnit.DAYS));
        b2.setStatus(BookingStatus.RESERVED);
        dto2.setBooking(b2);

        BookingDto dto3 = new BookingDto();
        Booking b3 = new Booking();
        b3.setStatusChanged(Instant.now().minus(120, ChronoUnit.MINUTES));
        b3.setStatus(BookingStatus.RESERVED);
        dto3.setBooking(b3);

        return Stream.of(
                arguments("43:00:00", dto1),
                arguments("24:00:00", dto2),
                arguments("46:00:00", dto3)
        );
    }

    @ParameterizedTest
    @MethodSource("calcBookingDtoTimeLeft_BOOKED_CANCELLED_statuses_test_cases")
    void calcBookingDtoTimeLeft_BOOKED_CANCELLED_statuses_test(BookingDto bookingDto) {
        BookingDao bookingDao = mock(BookingDao.class);
        BookingService bookingService = new BookingService(bookingDao, ZoneOffset.UTC);
        assertNull(bookingService.calcBookingDtoTimeLeft(bookingDto).getTimeLeft());
    }

    static Stream<Arguments> calcBookingDtoTimeLeft_BOOKED_CANCELLED_statuses_test_cases() {
        BookingDto dto1 = new BookingDto();
        Booking b1 = new Booking();
        b1.setStatusChanged(Instant.now().minus(1, ChronoUnit.DAYS));
        b1.setStatus(BookingStatus.BOOKED);
        dto1.setBooking(b1);

        BookingDto dto2 = new BookingDto();
        Booking b2 = new Booking();
        b2.setStatusChanged(Instant.now().minus(1, ChronoUnit.DAYS));
        b2.setStatus(BookingStatus.CANCELLED);
        dto2.setBooking(b2);

        return Stream.of(
                arguments(dto1),
                arguments(dto2)
        );
    }
}

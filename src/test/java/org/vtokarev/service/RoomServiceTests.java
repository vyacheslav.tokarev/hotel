package org.vtokarev.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.vtokarev.dao.RoomDao;
import org.vtokarev.dto.RoomSearchDto;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.params.provider.Arguments.arguments;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class RoomServiceTests {

    @ParameterizedTest
    @MethodSource("getRoomsByParams_errors_result_test_cases")
    void getRoomsByParams_errors_result_test(LocalDate checkin, LocalDate checkout, String expectedErrorMessage) {
        RoomDao roomDao = mock(RoomDao.class);
        RoomService roomService= new  RoomService(roomDao);

        String errorMessage = roomService
                .getRoomsByParams(checkin, checkout, false, 1L, 1, "", 0, 0)
                .getB();

        assertEquals(expectedErrorMessage, errorMessage);
    }

    static Stream<Arguments> getRoomsByParams_errors_result_test_cases() {

        return Stream.of(
                arguments(null, LocalDate.parse("2023-10-20"), "incorrect.checkin.checkout.message"),
                arguments(LocalDate.parse("2023-10-05"), null, "incorrect.checkin.checkout.message"),
                arguments(LocalDate.parse("2023-10-05"), LocalDate.parse("2023-10-05"), "checkin.must.be.less.than.checkout.message"),
                arguments(LocalDate.parse("2023-10-20"), LocalDate.parse("2023-10-05"), "checkin.must.be.less.than.checkout.message"),
                arguments(LocalDate.parse("2021-10-05"), LocalDate.parse("2021-10-20"), "checkin.cannot.be.in.the.past.message"));

    }

    @Test
    void getRoomsByParams_empty_success_result_test() {

        LocalDate checkin = LocalDate.parse("2023-10-03");
        LocalDate checkout = LocalDate.parse("2023-10-20");
        boolean showOnlyAvailable = false;
        Long roomClassId = 1L;
        Integer personsNumber = 1;
        String orderBy = "price";
        Integer limit = 1;
        Integer offset = 1;

        List<RoomSearchDto> roomSearchDtoList = new ArrayList<>();

        RoomDao roomDao = mock(RoomDao.class);
        when(roomDao.getBookingRooms(checkin, checkout, showOnlyAvailable, roomClassId, personsNumber, orderBy, limit, offset))
                .thenReturn(roomSearchDtoList);

        RoomService roomService= new RoomService(roomDao);


        String emptyResult = roomService
                .getRoomsByParams(checkin, checkout, showOnlyAvailable, roomClassId, personsNumber, orderBy, limit, offset)
                .getB();

        assertEquals("empty", emptyResult);

        roomSearchDtoList.add(new RoomSearchDto());
        String successResult = roomService
                .getRoomsByParams(checkin, checkout, showOnlyAvailable, roomClassId, personsNumber, orderBy, limit, offset)
                .getB();

        assertEquals("success", successResult);
    }
}

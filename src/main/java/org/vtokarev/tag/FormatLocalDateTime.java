package org.vtokarev.tag;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

/**
 * The class implements {@code FormatLocalDateTime} custom JSP tag.
 * It uses for the string representation of the {@link Instant} depending on the given string pattern and timezone offset.
 *
 * @author Vyacheslav Tokarev
 */

public class FormatLocalDateTime extends SimpleTagSupport {

    private Instant value;
    private String pattern;
    private String zoneOffset;

    public Instant getValue() {
        return value;
    }

    public void setValue(Instant value) {
        this.value = value;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public String getZoneOffset() {
        return zoneOffset;
    }

    public void setZoneOffset(String zoneOffset) {
        this.zoneOffset = zoneOffset;
    }

    @Override
    public void doTag() throws IOException {
        ZoneOffset zo = ZoneOffset.of(zoneOffset);
        LocalDateTime ldt = LocalDateTime.ofInstant(value, zo);
        String result = ldt.format(DateTimeFormatter.ofPattern(pattern));

        PageContext pageContext = (PageContext) getJspContext();
        JspWriter writer = pageContext.getOut();

        writer.print(result);
    }
}

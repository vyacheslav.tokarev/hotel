package org.vtokarev.dao.mysql;

import org.vtokarev.dao.BookingDao;
import org.vtokarev.dao.RoomRequestDao;
import org.vtokarev.domain.Booking;
import org.vtokarev.domain.BookingStatus;
import org.vtokarev.domain.RoomRequest;
import org.vtokarev.domain.RoomRequestStatus;
import org.vtokarev.dto.BookingDto;

import javax.sql.DataSource;
import java.sql.*;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * {@code BookingDaoImpl} class is an implementation of {@link BookingDao} interface for MySQL persistence layer backend.
 *
 * @author Vyacheslav Tokarev
 */


public class BookingDaoImpl extends BaseDaoImpl implements BookingDao {

    RoomRequestDao roomRequestDao;

    private static final String UPDATE_BOOKING = """
            UPDATE bookings
            SET
            checkin = ?, checkout = ?, room_id = ?, status = ?, status_changed = ?, user_id = ?, price = ?
            WHERE id = ?""";

    private static final String GET_BOOKING_BY_ID = """
            SELECT * FROM bookings WHERE id = ?""";

    private static final String GET_BOOKING_DTO_BY_USER_ID = """
            SELECT b.*, r.name as roomName, r.number as roomNumber, r.description, r.capacity, rc.name as roomClassName
            FROM bookings b
                INNER JOIN rooms r
                ON b.room_id = r.id
                INNER JOIN room_classes rc
                ON r.room_class_id = rc.id
            WHERE b.id = ?""";

    private static final String GET_BOOKINGS_BY_USER_ID = """
            SELECT b.*, r.name as roomName, r.number as roomNumber, r.description, r.capacity, rc.name as roomClassName
            FROM bookings b
                INNER JOIN rooms r
                ON b.room_id = r.id
                INNER JOIN room_classes rc
                ON r.room_class_id = rc.id
            WHERE user_id = ?
            ORDER BY b.status_changed DESC, b.checkin""";

    private static final String UPDATE_BREACHED_RESERVED_STATUS = """
            UPDATE bookings
            SET bookings.status = 'CANCELLED'
            WHERE bookings.status = 'RESERVED' AND (bookings.status_changed < ? OR bookings.checkin < ?)""";

    private final static String CHECK_FREE_ROOM_FOR_BOOKING_BY_ID = """
            SELECT count(room_id) as count FROM bookings
            WHERE bookings.status != 'CANCELLED' AND bookings.room_id = ? AND (
                ? > bookings.checkin AND ? < bookings.checkout OR
                ? > bookings.checkin AND ? < bookings.checkout OR
                ? <= bookings.checkin AND ? >= bookings.checkout)""";

    private final static String INSERT_BOOKING = """
            INSERT INTO bookings
            (checkin, checkout, room_id, status, status_changed, user_id, price)
            VALUES
            (?, ?, ?, ?, ?, ?, ?)""";

    public BookingDaoImpl(DataSource dataSource, RoomRequestDao roomRequestDao) {
        super(dataSource);
        this.roomRequestDao = roomRequestDao;
    }

    public List<BookingDto> getBookingsByUserId(long userId) {
        List<BookingDto> bookings = new ArrayList<>();

        if (userId == 0)
            return bookings;

        Connection connection = getConnection();
        PreparedStatement statement = null;
        ResultSet rs = null;
        try {
            statement = connection.prepareStatement(GET_BOOKINGS_BY_USER_ID);
            statement.setLong(1, userId);
            rs = statement.executeQuery();

            while (rs.next()) {
                BookingDto bookingDto = new BookingDto();
                fillBookingDto(bookingDto, rs);
                bookings.add(bookingDto);
            }

        } catch (SQLException e) {
            logger.error("Failed getBookingsByUserId", e);
            throw new RuntimeException("Failed getBookingsByUserId", e);
        } finally {
            close(rs);
            close(statement);
            close(connection);
        }
        logger.debug("Success getBookingsByUserId");
        return bookings;
    }

    @Override
    public BookingDto getBookingById(long bookingId) {
        BookingDto bookingDto = new BookingDto();
        Connection connection = getConnection();
        PreparedStatement statement = null;
        ResultSet rs = null;
        try {
            statement = connection.prepareStatement(GET_BOOKING_DTO_BY_USER_ID);
            statement.setLong(1, bookingId);
            rs = statement.executeQuery();

            if (rs.next()) {
                fillBookingDto(bookingDto, rs);
            }

        } catch (SQLException e) {
            logger.error("Failed getBookingBId", e);
            throw new RuntimeException("Failed getBookingBId", e);
        } finally {
            close(rs);
            close(statement);
            close(connection);
        }
        logger.debug("Success getBookingBId");
        return bookingDto;
    }

    private void fillBookingDto(BookingDto bookingDto, ResultSet rs) throws SQLException {
        Booking booking = new Booking();
        booking.setId(rs.getLong("id"));
        booking.setCheckin(rs.getDate("checkin").toLocalDate());
        booking.setCheckout(rs.getDate("checkout").toLocalDate());
        booking.setRoomId(rs.getLong("room_id"));
        booking.setUserId(rs.getLong("user_id"));
        booking.setStatus(BookingStatus.valueOf(rs.getString("status")));
        booking.setPrice(rs.getDouble("price"));
        booking.setStatusChanged(rs.getTimestamp("status_changed").toInstant());
        bookingDto.setBooking(booking);
        bookingDto.setRoomName(rs.getString("roomName"));
        bookingDto.setRoomNumber(rs.getString("roomNumber"));
        bookingDto.setCapacity(rs.getInt("capacity"));
        bookingDto.setRoomClassName(rs.getString("roomClassName"));
        bookingDto.setDescription(rs.getString("description"));
    }

    public int updateBreachedReservedBookingStatus(Instant statusChanged, LocalDate checkin) {
        Connection connection = getConnection();
        PreparedStatement statement = null;
        int rowAffected;
        try {
            statement = connection.prepareStatement(UPDATE_BREACHED_RESERVED_STATUS);
            statement.setTimestamp(1, Timestamp.from(statusChanged));
            statement.setDate(2, Date.valueOf(checkin));
            rowAffected = statement.executeUpdate();
        } catch (SQLException e) {
            logger.error("Failed updateReservedBookingStatus", e);
            throw new RuntimeException(e);
        } finally {
            close(statement);
            close(connection);
        }
        logger.debug("Success updateReservedBookingStatus");
        return rowAffected;
    }

    public boolean bookRoom(LocalDate checkin, LocalDate checkout, long userId, long roomId, double price, Long roomRequestId, String managerNotes) {
        Connection connection = getConnection();
        PreparedStatement statement = null;
        ResultSet rs = null;

        try {
            connection.setAutoCommit(false);
            logger.debug("Transaction isolation level is {}", connection.getTransactionIsolation());

            // use select for update as a mutex
            statement = connection.prepareStatement("SELECT * from rooms where id = ? FOR UPDATE");
            statement.setLong(1, roomId);
            rs = statement.executeQuery();

            if(!rs.next()) {
                rollback(connection);
                return false;
            }

            close(rs);
            close(statement);
            statement = connection.prepareStatement(CHECK_FREE_ROOM_FOR_BOOKING_BY_ID);
            statement.setLong(1, roomId);
            statement.setDate(2, Date.valueOf(checkin));
            statement.setDate(3, Date.valueOf(checkin));
            statement.setDate(4, Date.valueOf(checkout));
            statement.setDate(5, Date.valueOf(checkout));
            statement.setDate(6, Date.valueOf(checkin));
            statement.setDate(7, Date.valueOf(checkout));

            rs = statement.executeQuery();

            if (rs.next() && rs.getInt("count") > 0) {
                rollback(connection);
                return false;
            }

            Booking booking = new Booking();
            booking.setPrice(price);
            booking.setRoomId(roomId);
            booking.setCheckin(checkin);
            booking.setCheckout(checkout);
            booking.setStatusChanged(Instant.now());
            booking.setUserId(userId);
            booking.setStatus(BookingStatus.RESERVED);

            create(booking, connection);

            if (roomRequestId != null) {
                Optional<RoomRequest> roomRequest = roomRequestDao.get(roomRequestId, connection);
                roomRequest.ifPresent(rr -> {
                    rr.setStatus(RoomRequestStatus.PROVIDED);
                    rr.setBookingId(booking.getId());
                    rr.setManagerNotes(managerNotes);
                    roomRequestDao.update(rr, connection);
                });
            }
            connection.commit();
            logger.debug("Success bookRoom");
            return true;
        } catch (SQLException e) {
            logger.error("Failed bookRoom", e);
            rollback(connection);
            return false;
        } finally {
            close(rs);
            close(statement);
            setAutoCommitToTrueAndCloseConnection(connection);
        }
    }

    @Override
    public Optional<Booking> get(long id) {
        Connection connection = getConnection();
        PreparedStatement statement = null;
        ResultSet rs = null;
        Booking booking = null;

        try {
            statement = connection.prepareStatement(GET_BOOKING_BY_ID);
            statement.setLong(1, id);
            rs = statement.executeQuery();

            if (rs.next()) {
                booking = new Booking();
                booking.setId(rs.getLong("id"));
                booking.setCheckin(rs.getDate("checkin").toLocalDate());
                booking.setCheckout(rs.getDate("checkout").toLocalDate());
                booking.setRoomId(rs.getLong("room_id"));
                booking.setStatus(BookingStatus.valueOf(rs.getString("status")));
                booking.setStatusChanged(rs.getTimestamp("status_changed").toInstant());
                booking.setUserId(rs.getLong("user_id"));
                booking.setPrice(rs.getDouble("price"));
            }
        } catch (SQLException e) {
            logger.error("Failed get", e);
            throw new RuntimeException(e);
        } finally {
            close(rs);
            close(statement);
            close(connection);
        }
        logger.debug("Success get");
        return Optional.ofNullable(booking);
    }

    @Override
    public List<Booking> getAll() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void create(Booking bookings) {
        Connection connection = getConnection();
        try {
            create(bookings, connection);
            logger.error("Success create");
        } catch (SQLException e) {
            logger.error("Failed create", e);
            throw new RuntimeException("Failed create", e);
        } finally {
            close(connection);
        }
    }

    public void create(Booking bookings, Connection connection) throws SQLException {
        PreparedStatement statement = null;
        ResultSet generatedKeys = null;
        try {
            statement = connection.prepareStatement(INSERT_BOOKING, Statement.RETURN_GENERATED_KEYS);

            int k = 1;
            statement.setDate(k++, Date.valueOf(bookings.getCheckin()));
            statement.setDate(k++, Date.valueOf(bookings.getCheckout()));
            statement.setLong(k++, bookings.getRoomId());
            statement.setString(k++, bookings.getStatus().name());
            statement.setTimestamp(k++, Timestamp.from(bookings.getStatusChanged()));
            statement.setLong(k++, bookings.getUserId());
            statement.setDouble(k, bookings.getPrice());

            int affectedRows = statement.executeUpdate();

            if (affectedRows == 0) {
                logger.error("Creating booking failed, no rows affected");
                throw new SQLException("Creating booking failed, no rows affected.");
            }

            generatedKeys = statement.getGeneratedKeys();
            if (generatedKeys.next()) {
                bookings.setId(generatedKeys.getLong(1));
            } else {
                logger.error("Creating booking failed, no ID obtained");
                throw new SQLException("Creating booking failed, no ID obtained");
            }
        } finally {
            close(generatedKeys);
            close(statement);
        }
    }


    @Override
    public void update(Booking bookings) {
        Connection connection = getConnection();
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(UPDATE_BOOKING);
            int k = 1;
            stmt.setDate(k++, Date.valueOf(bookings.getCheckin()));
            stmt.setDate(k++, Date.valueOf(bookings.getCheckout()));
            stmt.setLong(k++, bookings.getRoomId());
            stmt.setString(k++, bookings.getStatus().name());
            stmt.setTimestamp(k++, Timestamp.from(bookings.getStatusChanged()));
            stmt.setLong(k++, bookings.getUserId());
            stmt.setDouble(k++, bookings.getPrice());
            stmt.setLong(k, bookings.getId());
            stmt.executeUpdate();
            logger.debug("Success update");
        } catch (SQLException e) {
            logger.error("Failed update", e);
            throw new RuntimeException("Failed update", e);
        } finally {
            close(stmt);
            close(connection);
        }
    }

    private void rollback(Connection connection) {
        try {
            connection.rollback();
        } catch (SQLException e) {
            logger.error("Failed rollback in bookRoom", e);
            throw new RuntimeException(e);
        }
    }

    private void setAutoCommitToTrueAndCloseConnection(Connection connection) {
        try {
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            logger.error("Failed setAutoCommit in bookRoom", e);
        } finally {
            close(connection);
        }
    }

    @Override
    public void delete(Booking bookings) {
        throw new UnsupportedOperationException();
    }
}

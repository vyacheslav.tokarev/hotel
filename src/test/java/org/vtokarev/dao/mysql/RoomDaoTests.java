package org.vtokarev.dao.mysql;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.vtokarev.dao.RoomDao;
import org.vtokarev.domain.Room;
import org.vtokarev.dto.RoomSearchDto;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class RoomDaoTests {
    @RegisterExtension
    public static SqlMemoryDatabase sqlMemoryDatabase = new SqlMemoryDatabase();

    @BeforeEach
    void init() throws SQLException {
        String initSql = """
                INSERT INTO users VALUES (default,'Vyacheslav','Tokarev','vyacheslav.tokarev@gmail.com','bijEcgZ4O401p6LWe0U6ea9R9sjVExAUTW1rxTYUFHQ','GUEST','2022-09-11 18:34:15');
                INSERT INTO room_classes VALUES (default, 'Standard'),(default, 'Superior'),(default, 'Studio'),(default, 'Suite');
                 INSERT INTO rooms VALUES
                 (default,'Standard double room','201',2,1,'Standard double room',40.00,1),
                 (default,'Standard triple room','202',3,1,'Standard triple room',55.00,1),
                 (default,'Superior double room','203',2,2,'Superior double room',50.00,1);
                 INSERT INTO bookings VALUES (default,'2023-10-05','2023-10-20',1,'RESERVED','2022-10-10 21:01:02',1,40.00)""";

        Connection conn = sqlMemoryDatabase.dataSource().getConnection();
        Statement stmt = conn.createStatement();
        stmt.executeUpdate(initSql);
        stmt.close();
        conn.close();
    }

    @Test
    void create_test(){
        Room room1 = new Room(0, "roomName", "num", 2,
                1, "description", 50.00, true);

        RoomDao roomDao = new RoomDaoImpl(sqlMemoryDatabase.dataSource(), "/img/rooms");
        roomDao.create(room1);
        assertTrue(room1.getId() > 0);
        Optional<Room> room2 = roomDao.get(room1.getId());
        assertTrue(room2.isPresent());
        assertEquals(room1, room2.get());
    }
    @Test
    void update_test(){
        Room room1 = new Room(0, "roomName", "num", 2,
                1, "description", 50.00, true);

        RoomDao roomDao = new RoomDaoImpl(sqlMemoryDatabase.dataSource(), "/img/rooms");
        roomDao.create(room1);

        room1.setName("roomNameUpd");
        room1.setNumber("numUpd");
        room1.setCapacity(3);
        room1.setRoomClassId(2);
        room1.setDescription("description upd");
        room1.setPrice(60.00);
        room1.setAvailable(false);

        roomDao.update(room1);

        Optional<Room> room2 = roomDao.get(room1.getId());
        assertTrue(room2.isPresent());
        assertEquals(room1, room2.get());
    }
    @Test
    void delete_test(){
        Room room1 = new Room(0, "roomName", "num", 2,
                1, "description", 50.00, true);

        RoomDao roomDao = new RoomDaoImpl(sqlMemoryDatabase.dataSource(), "/img/rooms");
        roomDao.create(room1);

        Optional<Room> room2 = roomDao.get(room1.getId());
        assertTrue(room2.isPresent());

        roomDao.delete(room1);

        room2 = roomDao.get(room1.getId());
        assertFalse(room2.isPresent());
    }





    @ParameterizedTest
    @MethodSource("get_test_cases")
    void get_test(int id, String name, String number, int capacity, int room_class_id, String description, double price, boolean available) {

        RoomDao roomDao = new RoomDaoImpl(sqlMemoryDatabase.dataSource(), "/img/rooms");

        Optional<Room> room = roomDao.get(id);
        assertTrue(room.isPresent());

        assertEquals(name, room.get().getName());
        assertEquals(number, room.get().getNumber());
        assertEquals(capacity, room.get().getCapacity());
        assertEquals(room_class_id, room.get().getRoomClassId());
        assertEquals(description, room.get().getDescription());
        assertEquals(price, room.get().getPrice());
        assertEquals(available, room.get().isAvailable());
        System.out.println(roomDao.getImageRealPathDirectory(room.get()));
    }

    @Test
    void getAll_test() {
        RoomDao roomDao = new RoomDaoImpl(sqlMemoryDatabase.dataSource(), "/img/rooms");
        int capacity = roomDao.getMaxCapacity();
        assertEquals(3, capacity);
    }

    @Test
    void getBookingRooms_test() {
        LocalDate checkin = LocalDate.parse("2023-10-15");
        LocalDate checkout = LocalDate.parse("2023-10-25");
        boolean showOnlyAvailable = true;
        Long roomClassId = 1L;
        Integer personsNumber = 2;
        String orderBy = "price";

        RoomDao roomDao = new RoomDaoImpl(sqlMemoryDatabase.dataSource(), "/img/rooms");

        List<RoomSearchDto> list = roomDao.getBookingRooms(checkin, checkout, showOnlyAvailable, roomClassId,
                personsNumber, orderBy, null, null);

        System.out.println(list);

        assertEquals(2, list.get(0).getRoom().getId());

    }

    @Test
    void getBookingRoomsCount_test() {
        LocalDate checkin = LocalDate.parse("2023-10-15");
        LocalDate checkout = LocalDate.parse("2023-10-25");
        boolean showOnlyAvailable = true;
        Long roomClassId = 1L;
        Integer personsNumber = 2;
        RoomDao roomDao = new RoomDaoImpl(sqlMemoryDatabase.dataSource(), "/img/rooms");
        int count = roomDao.getBookingRoomsCount(checkin, checkout, showOnlyAvailable, roomClassId, personsNumber);
        assertEquals(1, count);
    }

    @ParameterizedTest
    @MethodSource("getByNumber_test_cases")
    void getByNumber_test(int id, String name, String number, int capacity, int room_class_id, String description, double price, boolean available){
        RoomDao roomDao = new RoomDaoImpl(sqlMemoryDatabase.dataSource(), "/img/rooms");
        Optional<Room> room = roomDao.getByNumber(number);
        assertTrue(room.isPresent());
        assertEquals(id, room.get().getId());
        assertEquals(name, room.get().getName());
        assertEquals(capacity, room.get().getCapacity());
        assertEquals(room_class_id, room.get().getRoomClassId());
        assertEquals(description, room.get().getDescription());
        assertEquals(price, room.get().getPrice());
        assertEquals(available, room.get().isAvailable());
    }

    static Stream<Arguments> getByNumber_test_cases() {
        return Stream.of(
                arguments(1, "Standard double room", "201", 2, 1, "Standard double room", 40.00, true),
                arguments(2, "Standard triple room", "202", 3, 1, "Standard triple room", 55.00, true),
                arguments(3, "Superior double room", "203", 2, 2, "Superior double room", 50.00, true));
    }




    static Stream<Arguments> get_test_cases() {
        return Stream.of(
                arguments(1, "Standard double room", "201", 2, 1, "Standard double room", 40.00, true),
                arguments(2, "Standard triple room", "202", 3, 1, "Standard triple room", 55.00, true),
                arguments(3, "Superior double room", "203", 2, 2, "Superior double room", 50.00, true));
    }
}
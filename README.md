The Hotel web application uses Tomcat as a servlet container and MySQL as a backend DB.
It was tested on MySQL version 8.0.3 and Tomcat version 9.0.65.

Default DB parameters are:
    DB's name is 'hotel'
    username is 'root'
    password is 'root'
these parameters can be changed in src/main/webapp/META-INF/context.xml

Folder structure:
db -  contains DB-related materials: DB dump and DB diagram.
javadoc - contains javadoc

The following app users are created if the DB is restored from the provided dump:

Admin
login: vyacheslav.tokarev@hotel.com
password: Qwerty12

Manager:
login: vyacheslav.tokarev@hotmail.com
password: Qwerty12

Guest:
login: vyacheslav.tokarev@gmail.com
password: Qwerty12
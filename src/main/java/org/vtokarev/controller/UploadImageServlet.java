package org.vtokarev.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * The {@code UploadImageServlet} class implements {@code doPost} method for uploading room images functionality.
 *
 * @author Vyacheslav Tokarev
 */

@WebServlet(urlPatterns = "/uploadImage", name = "uploadImageServlet")
@MultipartConfig(fileSizeThreshold = 1024 * 1024,
        maxFileSize = 1024 * 1024 * 5,
        maxRequestSize = 1024 * 1024 * 5)
public class UploadImageServlet extends HttpServlet {

    private final static Logger LOGGER = LogManager.getLogger(MethodHandles.lookup().lookupClass());

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String errorMessage = "";
        String back = req.getParameter("back");
        long roomId = Long.parseLong(req.getParameter("roomId"));

        String requestStr = back + "?back=roomList&roomId=" + roomId;

        String realPathDirectory = req.getParameter("realPathDirectory");
        Part filePart = req.getPart("file");
        String fileName = filePart.getSubmittedFileName();

        String uploadingFullPathFileName = realPathDirectory + File.separator + fileName;

        try {
            if (req.getPart("file").getSize() > 1024 * 1024 * 5) {
                errorMessage = "file.exceed.max.size.message";
                requestStr += ("&errorMessage=" + errorMessage);
                resp.sendRedirect(requestStr);
                return;
            }

            Path pathDirectory = Paths.get(realPathDirectory);
            if (!Files.exists(pathDirectory)) {
                Files.createDirectories(pathDirectory);
            }

            for (Part part : req.getParts()) {
                LOGGER.trace(realPathDirectory + File.separator + fileName);
                part.write(uploadingFullPathFileName);
            }
        } catch (Exception e) {
            LOGGER.error("Failed to upload file {} for roomId={}, exception : {} ", uploadingFullPathFileName, roomId, e);
            errorMessage = "unable.upload.file.message";
        }
        if (!errorMessage.isBlank()) {
            requestStr += ("&errorMessage=" + errorMessage);
        }
        resp.sendRedirect(requestStr);
    }
}

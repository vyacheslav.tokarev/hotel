package org.vtokarev.dto;

import org.vtokarev.domain.Booking;

import java.util.Objects;

/**
 * The {@code BookingDto} class is a data transfer object, that encapsulates {@link Booking},
 * some fields of {@link org.vtokarev.domain.Room}, {@link org.vtokarev.domain.RoomClass}
 * and calculated field {@code timeLeft}.
 * The DTO is used for transferring data between layers.
 *
 * Persistence -> DAO -> service -> controller({@link org.vtokarev.controller.BookingServlet}, {@link org.vtokarev.controller.BookingListServlet}) -> view({@code booking.jsp}, {@code bookingList.jsp}).
 *
 * @author Vyacheslav Tokarev
 */


public class BookingDto {

    private Booking booking;
    private String roomName;

    private String roomNumber;
    private int capacity;
    private String roomClassName;

    private String description;

    private String timeLeft;

    public BookingDto() {
    }

    public BookingDto(Booking booking, String roomName, String roomNumber, int capacity,
                      String roomClassName, String description, String timeLeft) {
        this.booking = booking;
        this.roomName = roomName;
        this.roomNumber = roomNumber;
        this.capacity = capacity;
        this.roomClassName = roomClassName;
        this.description = description;
        this.timeLeft = timeLeft;
    }

    public BookingDto(BookingDto bookingDto) {
        if (bookingDto != null) {
            this.booking = bookingDto.getBooking();
            this.roomName = bookingDto.getRoomName();
            this.roomNumber = bookingDto.getRoomNumber();
            this.capacity = bookingDto.getCapacity();
            this.roomClassName = bookingDto.getRoomClassName();
            this.description = bookingDto.getDescription();
            this.timeLeft = bookingDto.getTimeLeft();
        }
    }

    public Booking getBooking() {
        return booking;
    }

    public void setBooking(Booking booking) {
        this.booking = booking;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public String getRoomClassName() {
        return roomClassName;
    }

    public void setRoomClassName(String roomClassName) {
        this.roomClassName = roomClassName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public String getTimeLeft() {
        return timeLeft;
    }

    public void setTimeLeft(String timeLeft) {
        this.timeLeft = timeLeft;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BookingDto that = (BookingDto) o;
        return getCapacity() == that.getCapacity() && Objects.equals(getBooking(), that.getBooking()) &&
                Objects.equals(getRoomName(), that.getRoomName()) && Objects.equals(getRoomNumber(), that.getRoomNumber()) &&
                Objects.equals(getRoomClassName(), that.getRoomClassName()) &&
                Objects.equals(getDescription(), that.getDescription()) && Objects.equals(getTimeLeft(), that.getTimeLeft());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getBooking(), getRoomName(), getRoomNumber(), getCapacity(), getRoomClassName(), getDescription(), getTimeLeft());
    }

    @Override
    public String toString() {
        return "BookingDto{" +
                "booking=" + booking +
                ", roomName='" + roomName + '\'' +
                ", roomNumber='" + roomNumber + '\'' +
                ", capacity=" + capacity +
                ", roomClassName='" + roomClassName + '\'' +
                ", description='" + description + '\'' +
                ", timeLeft='" + timeLeft + '\'' +
                '}';
    }
}
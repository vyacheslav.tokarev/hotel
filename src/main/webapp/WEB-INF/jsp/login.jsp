<%@ include file="header.jsp" %>
<%--@elvariable id="passwordPattern" type="java.lang.String"--%>
<%--@elvariable id="user" type="org.vtokarev.domain.User"--%>
<%--@elvariable id="invalidCredentials" type="boolean"--%>
<br><br><br><br>
<div class="row">
    <div class="col s4 offset-s4 z-depth-1">
        <div class="container">
            <form action="login" method="post">
                <div class="input-field">
                    <input id="email" name="email" type="email" required="required" class="validate">
                    <label for="email"><fmt:message key="email.label"/></label>
                    <span class="helper-text" data-error="<fmt:message key="error.label" />"
                          data-success="<fmt:message key="success.label" />"></span>
                </div>
                <div class="input-field">
                    <input id="password" name="password" type="password" required="required" class="validate">
                    <label for="password"><fmt:message key="password.label"/></label>
                    <span class="helper-text"
                          data-error="<fmt:message key="dataError.password.login" />"
                          data-success="<fmt:message key="success.label" />"><fmt:message
                            key="dataError.password.login"/></span>
                </div>
                <button class="btn waves-effect waves-light cyan darken-3" type="submit" name="action"><fmt:message
                        key="submit.action"/>
                    <i class="material-icons right">done</i>
                </button>
                <a class="waves-effect waves-light btn red" href="home"><i
                        class="material-icons right">close</i><fmt:message key="close.action"/></a>
            </form>
            <c:if test="${not empty param.invalidCredentials || invalidCredentials != null}">
                <blockquote class="red-text">
                    <fmt:message key="invalidCredentials.login"/>
                </blockquote>
            </c:if>
            <br>
            <div class="center-align">
                <span class="helper-text"><fmt:message key="noAccount.login"/><a
                        href="modifyUser?action=create&back=home"> <fmt:message key="createAccount.login"/></a></span>
            </div>
        </div>
    </div>
</div>
<%@ include file="footer.jsp" %>
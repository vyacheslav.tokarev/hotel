<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ct" uri="http://vtokarev.org/hotel/tags" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tf" %>
<%--@elvariable id="user" type="org.vtokarev.domain.User"--%>
<%--@elvariable id="UserRole" type="org.vtokarev.domain.UserRole"--%>
<%--@elvariable id="invalidCredentials" type="boolean"--%>
<fmt:setLocale value="${cookie['lang'].value}"/>
<fmt:setBundle basename="text"/>
<!DOCTYPE html>
<html>
<head>
    <title>Hotel</title>
    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css"/>
    <link rel="stylesheet" href="css/style.css"/>
</head>
<body>
<nav>
    <div class="nav-wrapper cyan darken-4">
        <a href="." class="brand-logo left"><i class="material-icons">home</i>Hotel</a>
        <ul class="right hide-on-small-and-down ">
            <c:if test="${user != null && user.userRole.name() == 'GUEST'}">
                <li><a href="requestList"><i class="material-icons left"></i><fmt:message key="requestRoom.label" /></a></li>
                <li><a href="bookingList"><i class="material-icons left"></i><fmt:message key="bookRoom.label" /></a></li>
            </c:if>
            <c:if test="${user != null && user.userRole.name() == 'MANAGER'}">
                <li><a href="requestList"><i class="material-icons left"></i><fmt:message key="proceed.header" /></a></li>
            </c:if>
            <c:if test="${user != null && user.userRole.name() == 'ADMIN'}">
                <li><a href="userList"><i class="material-icons left"></i><fmt:message key="users.label" /></a></li>
                <li><a href="roomList"><i class="material-icons left"></i><fmt:message key="rooms.label" /></a></li>
            </c:if>
            <c:if test="${user == null}">
                <li><a href="login"><i class="material-icons left"></i><fmt:message key="signIn.header" /></a></li>
            </c:if>
            <c:if test="${user != null }">
                <li><a href="logout"><i class="material-icons right">logout</i>${user.email}</a></li>
            </c:if>
            <li><tf:lang value="${cookie['lang'].value}"/></li>
        </ul>
    </div>
</nav>
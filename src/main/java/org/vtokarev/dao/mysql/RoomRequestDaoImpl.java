package org.vtokarev.dao.mysql;

import org.vtokarev.dao.RoomRequestDao;
import org.vtokarev.domain.RoomRequest;
import org.vtokarev.domain.RoomRequestStatus;
import org.vtokarev.dto.RoomRequestDto;

import javax.sql.DataSource;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * {@code RoomRequestDaoImpl} class is an implementation of {@link RoomRequestDao} interface for MySQL persistence layer backend.
 *
 * @author Vyacheslav Tokarev
 */

public class RoomRequestDaoImpl extends BaseDaoImpl implements RoomRequestDao {

    private static final String UPDATE_BREACHED_NEW_STATUS = """
            UPDATE room_requests
            SET room_requests.status = 'CANCELLED'
            WHERE room_requests.status = 'NEW' AND room_requests.checkin < ?""";


    private static final String GET_BY_ID = """
            SELECT * FROM room_requests WHERE id = ?""";

    private static final String UPDATE_ROOM_REQUEST = """
            UPDATE room_requests
            SET
            checkin = ?, checkout = ?, persons_number = ?, room_class_id = ?, user_id = ?,
            booking_id = ?, reg_date = ?, status = ?, guest_notes = ?, manager_notes = ?
            WHERE id = ?""";

    final private static String INSERT_ROOM_REQUEST = """
            INSERT INTO room_requests
            (checkin, checkout, persons_number, guest_notes, manager_notes,room_class_id, user_id, booking_id, reg_date, status)
            VALUES
            (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)""";

    private final static String GET_ROOM_REQUESTS_DTO = """
            SELECT rr.*, u.first_name as userFirstName, u.email as userEmail, rc.name as roomClassName, b.status bookingStatus
            FROM room_requests rr
                INNER JOIN users u
                ON rr.user_id = u.id
                INNER JOIN room_classes rc
                ON rr.room_class_id = rc.id
                LEFT OUTER JOIN bookings b
                ON rr.booking_id = b.id""";

    public RoomRequestDaoImpl(DataSource dataSource) {
        super(dataSource);
    }

    public Optional<RoomRequest> get(long id, Connection connection) {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        RoomRequest roomRequest = null;
        try {
            stmt = connection.prepareStatement(GET_BY_ID);
            stmt.setLong(1, id);
            rs = stmt.executeQuery();
            if(rs.next()){
                roomRequest = new RoomRequest();
                roomRequest.setCheckin(rs.getDate("checkin").toLocalDate());
                roomRequest.setCheckout(rs.getDate("checkout").toLocalDate());
                roomRequest.setPersonsNumber(rs.getInt("persons_number"));
                roomRequest.setGuestNotes(rs.getString("guest_notes"));
                roomRequest.setManagerNotes(rs.getString("manager_notes"));
                roomRequest.setRoomClassId(rs.getLong("room_class_id"));
                roomRequest.setUserId(rs.getLong("user_id"));
                roomRequest.setBookingId(rs.getLong("booking_id"));
                roomRequest.setRegDate(rs.getTimestamp("reg_date").toInstant());
                roomRequest.setStatus(RoomRequestStatus.valueOf(rs.getString("status")));
                roomRequest.setId(rs.getLong("id"));
            }
        } catch (SQLException e) {
            logger.error("Failed get", e);
            throw new RuntimeException("Failed get", e);
        } finally {
            close(rs);
            close(stmt);
        }
        logger.debug("Success get");
        return Optional.ofNullable(roomRequest);
    }

    @Override
    public Optional<RoomRequest> get(long id) {
        Connection connection = getConnection();
        Optional<RoomRequest> roomRequest = get(id, connection);
        close(connection);
        return roomRequest;
    }

    @Override
    public List<RoomRequest> getAll() {
        throw new UnsupportedOperationException("not implemented yet");
    }

    @Override
    public void create(RoomRequest roomRequest) {
        Connection connection = getConnection();
        PreparedStatement statement = null;
        ResultSet generatedKeys = null;

        try {
            statement = connection.prepareStatement(INSERT_ROOM_REQUEST, Statement.RETURN_GENERATED_KEYS);
            int k = 1;
            statement.setDate(k++, Date.valueOf(roomRequest.getCheckin()));
            statement.setDate(k++, Date.valueOf(roomRequest.getCheckout()));
            statement.setInt(k++, roomRequest.getPersonsNumber());
            statement.setString(k++, roomRequest.getGuestNotes());
            statement.setString(k++, roomRequest.getManagerNotes());
            statement.setLong(k++, roomRequest.getRoomClassId());
            statement.setLong(k++, roomRequest.getUserId());

            if (roomRequest.getBookingId() == 0) {
                statement.setNull(k++, Types.INTEGER);
            } else {
                statement.setLong(k++, roomRequest.getBookingId());
            }

            statement.setTimestamp(k++, Timestamp.from(roomRequest.getRegDate()));
            statement.setString(k, roomRequest.getStatus().name());


            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("Creating user failed, no rows affected.");
            }

            generatedKeys = statement.getGeneratedKeys();
            if (generatedKeys.next()) {
                roomRequest.setId(generatedKeys.getLong(1));
            } else {
                throw new SQLException("Creating roomRequest failed, no ID obtained.");
            }
            logger.debug("Success create");
        } catch (SQLException e) {
            logger.error("Failed create", e);
            throw new RuntimeException("Failed create", e);
        } finally {
            close(generatedKeys);
            close(statement);
            close(connection);
        }
    }

    public void update(RoomRequest roomRequest, Connection connection) {
        PreparedStatement stmt = null;
        try {
            stmt = connection.prepareStatement(UPDATE_ROOM_REQUEST);
            int k = 1;
            stmt.setDate(k++, Date.valueOf(roomRequest.getCheckin()));
            stmt.setDate(k++, Date.valueOf(roomRequest.getCheckout()));
            stmt.setInt(k++, roomRequest.getPersonsNumber());
            stmt.setLong(k++, roomRequest.getRoomClassId());
            stmt.setLong(k++, roomRequest.getUserId());
            if(roomRequest.getBookingId() == 0)
                stmt.setNull(k++, Types.INTEGER);
            else
                stmt.setLong(k++, roomRequest.getBookingId());
            stmt.setTimestamp(k++, Timestamp.from(roomRequest.getRegDate()));
            stmt.setString(k++, roomRequest.getStatus().name());
            stmt.setString(k++, roomRequest.getGuestNotes());
            stmt.setString(k++, roomRequest.getManagerNotes());
            stmt.setLong(k, roomRequest.getId());

            stmt.executeUpdate();
            logger.debug("Success update");
        } catch (SQLException e) {
            logger.error("Failed update", e);
            throw new RuntimeException("Failed update", e);
        } finally {
            close(stmt);
        }
    }

    @Override
    public void update(RoomRequest roomRequest) {
        Connection connection = getConnection();
        update(roomRequest, connection);
        close(connection);
    }

    @Override
    public void delete(RoomRequest roomRequest) {
        throw new UnsupportedOperationException("not implemented yet");
    }

    @Override
    public List<RoomRequestDto> getRequestsByUserId(Long userId) {
        Connection connection = getConnection();
        PreparedStatement statement = null;
        ResultSet rs = null;
        List<RoomRequestDto> roomRequestDtoList;
        try {
            String sqlStr = GET_ROOM_REQUESTS_DTO;

            if (userId != null)
                sqlStr += " WHERE rr.user_id = ? ";

            sqlStr += " ORDER BY rr.reg_date DESC";

            statement = connection.prepareStatement(sqlStr);

            if (userId != null)
                statement.setLong(1, userId);
            rs = statement.executeQuery();
            roomRequestDtoList = fillRoomRequestDtoList(rs);
        } catch (SQLException e) {
            logger.error("Failed getAllRequests", e);
            throw new RuntimeException("Failed getAllRequests", e);
        } finally {
            close(rs);
            close(statement);
            close(connection);
        }
        logger.debug("Success getAllRequests");
        return roomRequestDtoList;
    }

    private List<RoomRequestDto> fillRoomRequestDtoList(ResultSet rs) throws SQLException {
        List<RoomRequestDto> roomRequestDtoList = new ArrayList<>();
        while (rs.next()) {
            RoomRequestDto roomRequestDto = new RoomRequestDto();
            RoomRequest roomRequest = new RoomRequest();
            roomRequest.setId(rs.getLong("id"));
            roomRequest.setCheckin(rs.getDate("checkin").toLocalDate());
            roomRequest.setCheckout(rs.getDate("checkout").toLocalDate());
            roomRequest.setPersonsNumber(rs.getInt("persons_number"));
            roomRequest.setGuestNotes(rs.getString("guest_notes"));
            roomRequest.setManagerNotes(rs.getString("manager_notes"));
            roomRequest.setRoomClassId(rs.getLong("room_class_id"));
            roomRequest.setUserId(rs.getLong("user_id"));
            roomRequest.setRegDate(rs.getTimestamp("reg_date").toInstant());
            roomRequest.setBookingId(rs.getLong("booking_id"));
            roomRequest.setStatus(RoomRequestStatus.valueOf(rs.getString("status")));
            roomRequestDto.setRoomRequest(roomRequest);
            roomRequestDto.setRoomClassName(rs.getString("roomClassName"));
            roomRequestDto.setUserFirstName(rs.getString("userFirstName"));
            roomRequestDto.setUserEmail(rs.getString("userEmail"));
            roomRequestDto.setBookingStatus(rs.getString("bookingStatus"));
            roomRequestDtoList.add(roomRequestDto);
        }
        return roomRequestDtoList;
    }

    @Override
    public int updateBreachedNewRoomRequestStatus(LocalDate checkin) {
        Connection connection = getConnection();
        PreparedStatement statement = null;
        int rowAffected;
        try {
            statement = connection.prepareStatement(UPDATE_BREACHED_NEW_STATUS);
            statement.setDate(1, Date.valueOf(checkin));
            rowAffected = statement.executeUpdate();
        } catch (SQLException e) {
            logger.error("Failed updateBreachedNewRoomRequestStatus", e);
            throw new RuntimeException(e);
        } finally {
            close(statement);
            close(connection);
        }
        logger.debug("Success updateBreachedNewRoomRequestStatus");
        return rowAffected;
    }
}

<%@ include file="header.jsp" %>
<%--@elvariable id="roomClasses" type="java.util.List<org.vtokarev.domain.RoomClass>"--%>
<%--@elvariable id="capacities" type="int[]"--%>
<%--@elvariable id="errorMessage" type="java.lang.String"--%>
<%--@elvariable id="checkin" type="java.lang.String"--%>
<%--@elvariable id="checkout" type="java.lang.String"--%>
<c:choose>
    <c:when test="${param.prg == 'yes'}">
        <c:set var="roomClassIdValue" value="${param.roomClassId}"/>
        <c:set var="personsNumberValue" value="${param.personsNumber}"/>
    </c:when>
    <c:otherwise>
        <c:set var="roomClassIdValue" value="1"/>
        <c:if test="${fn:length(capacities) <= 1}">
            <c:set var="personsNumberValue" value="1"/>
        </c:if>
        <c:if test="${fn:length(capacities) > 1}">
            <c:set var="personsNumberValue" value="2"/>
        </c:if>
    </c:otherwise>
</c:choose>
<br><br><br><br>
<div class="row">
    <div class="col s4 offset-s4 z-depth-1">
        <div class="container">
            <br>
            <form action="createRequest" method="post">
                <div class="input-field">
                    <input type="text" class="datepicker" id="checkin" name="checkin" required>
                    <label for="checkin"><fmt:message key="checkIn.label"/></label>
                </div>
                <div class="input-field">
                    <input type="text" class="datepicker" id="checkout" name="checkout" required>
                    <label for="checkout"><fmt:message key="checkOut.label"/></label>
                </div>
                <div class="input-field">
                    <select name="roomClass">
                        <c:forEach items="${roomClasses}" var="item">
                            <option value="${item.id}"
                                    <c:if test="${item.id == roomClassIdValue}">selected</c:if>>${item.name}</option>
                        </c:forEach>
                    </select>
                    <label><fmt:message key="roomClass.label"/></label>
                </div>
                <div class="input-field">
                    <select name="personsNumber" required>
                        <option value="" disabled selected><fmt:message key="guests.label"/></option>
                        <c:forEach items="${capacities}" var="item">
                            <option value="${item}"
                                    <c:if test="${item == personsNumberValue}">selected</c:if>>${item}</option>
                        </c:forEach>
                    </select>
                    <label><fmt:message key="guests.label"/></label>
                </div>
                <div class="input-field">
                    <textarea id="guestNotes" name="guestNotes"
                              class="materialize-textarea">${param.guestNotes}</textarea>
                    <label for="guestNotes"><fmt:message key="additionalInfo.label"/></label>
                </div>
                <div class="input-field">
                    <button class="btn waves-effect waves-light" type="submit" name="action"><fmt:message
                            key="confirm.action"/>
                        <i class="material-icons right">done</i>
                    </button>
                    <a class="waves-effect waves-light btn red" href="requestList"><i
                            class="material-icons right">close</i><fmt:message key="close.action"/></a>
                </div>
            </form>
            <c:if test="${not empty param.errorMessage}">
                <blockquote class="red-text">
                    <fmt:message key="${param.errorMessage}"/>
                </blockquote>
            </c:if>
            <br>
        </div>
    </div>
</div>
<script>
    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('select');
        var instances = M.FormSelect.init(elems);
    });
    <c:choose>
        <c:when test="${param.prg == 'yes'}">
            var checkin_date = new Date('${param.checkin}');
            var checkout_date = new Date('${param.checkout}');
        </c:when>
        <c:otherwise>
            var checkin_date = new Date();
            var checkout_date = new Date();
            checkout_date.setDate(checkout_date.getDate() + 1);
        </c:otherwise>
    </c:choose>
    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('[name="checkin"]');
        var instances = M.Datepicker.init(elems, {
            autoClose: true,
            firstDay: 1,
            format: 'yyyy-mm-dd',
            minDate: checkin_date,
            defaultDate: checkin_date,
            setDefaultDate: true
        });
    });
    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('[name="checkout"]');
        var instances = M.Datepicker.init(elems, {
            autoClose: true,
            firstDay: 1,
            format: 'yyyy-mm-dd',
            minDate: checkout_date,
            defaultDate: checkout_date,
            setDefaultDate: true
        });
    });
</script>
<%@ include file="footer.jsp" %>
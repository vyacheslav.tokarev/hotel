package org.vtokarev.dto;

import org.vtokarev.domain.Room;
import org.vtokarev.domain.RoomClass;

import java.util.List;
import java.util.Objects;

/**
 * The {@code RoomSearchDto} class is a data transfer object, that encapsulates {@link Room},
 * some fields of {@link RoomClass}, and calculated field {@code status} .
 * The DTO is used for transferring data between layers.
 *
 * Persistence -> DAO -> service -> controller({@link org.vtokarev.controller.ProceedRequestServlet}, {@link org.vtokarev.controller.SearchRoomServlet}) -> view({@code  searchRoomGuest.jsp}, {@code searchRoomManager.jsp}).
 *
 * @author Vyacheslav Tokarev
 */


public class RoomSearchDto {
    private Room room;
    private String status;
    private String roomClassName;

    private List<String> images;

    public RoomSearchDto() {
    }

    public RoomSearchDto(Room room, String status, String roomClassName, List<String> images) {
        this.room = room;
        this.status = status;
        this.roomClassName = roomClassName;
        this.images = images;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public String getRoomClassName() {
        return roomClassName;
    }

    public void setRoomClassName(String roomClassName) {
        this.roomClassName = roomClassName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoomSearchDto that = (RoomSearchDto) o;
        return Objects.equals(getRoom(), that.getRoom()) && Objects.equals(getStatus(), that.getStatus()) &&
                Objects.equals(getRoomClassName(), that.getRoomClassName()) &&
                Objects.equals(getImages(), that.getImages());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getRoom(), getStatus(), getRoomClassName(), getImages());
    }

    @Override
    public String toString() {
        return "RoomSearchDto{" +
                "room=" + room +
                ", status='" + status + '\'' +
                ", roomClassName='" + roomClassName + '\'' +
                ", images=" + images +
                '}';
    }
}

package org.vtokarev.service;

import org.junit.jupiter.api.Test;
import org.vtokarev.dao.RoomRequestDao;

import java.time.LocalDate;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class RoomRequestServiceTests {

    @Test
    void getRegistrationErrorMessage_test() {
        RoomRequestDao roomRequestDao = mock(RoomRequestDao.class);

        RoomService roomService = mock(RoomService.class);
        when(roomService.getMaxCapacity()).thenReturn(4);

        RoomClassService roomClassService = mock(RoomClassService.class);
        when(roomClassService.get(1)).thenReturn(Optional.empty());

        RoomRequestService rrs = new RoomRequestService(roomRequestDao, roomService, roomClassService);

        LocalDate checkin = LocalDate.parse("2022-10-05");
        LocalDate checkout = LocalDate.parse("2022-10-05");
        assertEquals("checkin.must.be.less.than.checkout.message",
                rrs.getRegistrationErrorMessage(checkin, checkout, 0, 0));

        checkin = LocalDate.parse("2022-10-10");
        checkout = LocalDate.parse("2022-10-05");
        assertEquals("checkin.must.be.less.than.checkout.message",
                rrs.getRegistrationErrorMessage(checkin, checkout, 0, 0));

        checkin = LocalDate.parse("2022-10-05");
        checkout = LocalDate.parse("2022-10-10");
        assertEquals("checkin.cannot.be.in.the.past.message",
                rrs.getRegistrationErrorMessage(checkin, checkout, 0, 0));

        checkin = LocalDate.parse("2023-10-05");
        checkout = LocalDate.parse("2023-10-10");
        assertEquals("no.rooms.with.the.entered.capacity.message",
                rrs.getRegistrationErrorMessage(checkin, checkout, 0, 5));

        assertEquals("no.rooms.with.the.entered.room.class.message",
                rrs.getRegistrationErrorMessage(checkin, checkout, 1, 4));
    }

}

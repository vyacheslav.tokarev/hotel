package org.vtokarev.dto;

import org.vtokarev.domain.Room;
import org.vtokarev.domain.RoomClass;
import org.vtokarev.domain.RoomRequest;

import java.util.Objects;

/**
 * The {@code RoomRequestDto} class is a data transfer object, that encapsulates {@link RoomRequest},
 * some fields of {@link Room},{@link org.vtokarev.domain.User}, {@link RoomClass} and {@link org.vtokarev.domain.BookingStatus}.
 * The DTO is used for transferring data between layers.
 * Persistence -> DAO -> service -> controller{@link org.vtokarev.controller.RequestListServlet} -> view({@code requestList.jsp}).
 *
 * @author Vyacheslav Tokarev
 */

public class RoomRequestDto {
    private RoomRequest roomRequest;
    private String roomClassName;
    private String userFirstName;
    private String userEmail;
    private String bookingStatus;

    public RoomRequestDto() {
    }

    public RoomRequestDto(RoomRequest roomRequest, String roomClassName, String userFirstName, String userEmail, String bookingStatus) {
        this.roomRequest = roomRequest;
        this.roomClassName = roomClassName;
        this.userFirstName = userFirstName;
        this.userEmail = userEmail;
        this.bookingStatus = bookingStatus;
    }

    public RoomRequest getRoomRequest() {
        return roomRequest;
    }

    public void setRoomRequest(RoomRequest roomRequest) {
        this.roomRequest = roomRequest;
    }

    public String getRoomClassName() {
        return roomClassName;
    }

    public void setRoomClassName(String roomClassName) {
        this.roomClassName = roomClassName;
    }

    public String getUserFirstName() {
        return userFirstName;
    }

    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getBookingStatus() {
        return bookingStatus;
    }

    public void setBookingStatus(String bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoomRequestDto that = (RoomRequestDto) o;
        return Objects.equals(getRoomRequest(), that.getRoomRequest()) &&
                Objects.equals(getRoomClassName(), that.getRoomClassName()) &&
                Objects.equals(getUserFirstName(), that.getUserFirstName()) &&
                Objects.equals(getUserEmail(), that.getUserEmail()) &&
                Objects.equals(getBookingStatus(), that.getBookingStatus());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getRoomRequest(), getRoomClassName(), getUserFirstName(), getUserEmail(), getBookingStatus());
    }
    @Override
    public String toString() {
        return "RoomRequestDto{" +
                "roomRequest=" + roomRequest +
                ", roomClassName='" + roomClassName + '\'' +
                ", userFirstName='" + userFirstName + '\'' +
                ", userEmail='" + userEmail + '\'' +
                ", bookingStatus='" + bookingStatus + '\'' +
                '}';
    }
}

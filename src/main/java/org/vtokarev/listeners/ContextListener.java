package org.vtokarev.listeners;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.vtokarev.dao.*;
import org.vtokarev.dao.mysql.*;
import org.vtokarev.service.*;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.Filter;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.lang.invoke.MethodHandles;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * The {@code ContextListener} is a class that implements {@link ServletContextListener} interface.
 * It's a place where DAO and service layer objects are created and initialized.
 *
 * @author Vyacheslav Tokarev
 */


@WebListener
public class ContextListener implements ServletContextListener {

    private final static Logger LOGGER = LogManager.getLogger(MethodHandles.lookup().lookupClass());

    private ScheduledExecutorService scheduler;
    private Properties configProp;

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        try {
            LOGGER.debug(sce.getServletContext().getRealPath("/img/rooms"));
            LOGGER.debug(sce.getServletContext().getContextPath() + "/img/rooms");

            loadConfigProperties();

            // set zoneOffset based on ZoneId in config prop file
            ZoneId zoneId = ZoneId.of(configProp.getProperty("zone.id"));
            LocalDateTime now = LocalDateTime.now();
            ZoneOffset zoneOffset = zoneId.getRules().getOffset(now);

            InitialContext ctx = new InitialContext();

            sce.getServletContext().setAttribute("app/searchRoomManagerRecordsPerPage", Integer.parseInt(configProp.getProperty("searchRoomManager.recordsPerPage")));
            sce.getServletContext().setAttribute("app/searchRoomGuestRecordsPerPage", Integer.parseInt(configProp.getProperty("searchRoomGuest.recordsPerPage")));

            DataSource ds = (DataSource) ctx.lookup("java:/comp/env/mysql/hotelDB");
            UserDao userDao = new UserDaoImpl(ds);
            RoomRequestDao roomRequestDao = new RoomRequestDaoImpl(ds);
            RoomDao roomDao = new RoomDaoImpl(ds, sce.getServletContext().getRealPath(configProp.getProperty("images.path")));
            RoomClassDao roomClassDao = new RoomClassDaoImpl(ds);
            BookingDao bookingDao = new BookingDaoImpl(ds, roomRequestDao);

            String passwordPattern = configProp.getProperty("password_pattern.regexp");
            String emailPattern = configProp.getProperty("email_pattern.regexp");

            UserService userService = new UserService(userDao, passwordPattern, emailPattern);
            RoomClassService roomClassService = new RoomClassService(roomClassDao);
            BookingService bookingService = new BookingService(bookingDao, zoneOffset);
            RoomService roomService = new RoomService(roomDao);
            RoomRequestService roomRequestService = new RoomRequestService(roomRequestDao, roomService, roomClassService);

            sce.getServletContext().setAttribute("app/UserService", userService);
            sce.getServletContext().setAttribute("app/RoomRequestService", roomRequestService);
            sce.getServletContext().setAttribute("app/RoomClassService", roomClassService);
            sce.getServletContext().setAttribute("app/RoomService", roomService);
            sce.getServletContext().setAttribute("app/BookingService", bookingService);

            sce.getServletContext().setAttribute("passwordPattern", passwordPattern);
            sce.getServletContext().setAttribute("emailPattern", emailPattern);
            sce.getServletContext().setAttribute("zoneOffset", zoneOffset);


            scheduler = Executors.newSingleThreadScheduledExecutor();
            scheduler.scheduleAtFixedRate(
                    new ScheduledUpdateRecordsService(bookingDao, roomRequestDao, zoneId), 0, 1, TimeUnit.MINUTES);
        } catch (NamingException e) {
            LOGGER.error("Failed contextInitialized", e);
            throw new RuntimeException("Failed contextInitialized", e);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        /* This method is called when the controller Context is undeployed or Application Server shuts down. */
        scheduler.shutdownNow();
    }

    private void loadConfigProperties(){
        try (InputStream input = Thread.currentThread().getContextClassLoader().getResourceAsStream("config.properties")) {
            configProp = new Properties();
            configProp.load(input);
        } catch (IOException e) {
            LOGGER.error("Failed to load configProperties");
            throw new RuntimeException("Failed to load configProperties", e);
        }
    }

}

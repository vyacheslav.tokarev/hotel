package org.vtokarev.service;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.vtokarev.dao.UserDao;
import org.vtokarev.domain.User;

import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;
import java.util.Properties;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.params.provider.Arguments.arguments;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UserServiceTests {

    static String passwordPattern;
    static String emailPattern;

    @BeforeAll
    static void init() throws IOException {
        InputStream input = Thread.currentThread().getContextClassLoader().getResourceAsStream("test.config.properties");
        Properties properties = new Properties();
        properties.load(input);
        passwordPattern = properties.getProperty("password_pattern.regexp");
        emailPattern = properties.getProperty("email_pattern.regexp");
    }


    @Test
    void getRegistrationErrorMessage_errors_test() {
        UserDao userDao = mock(UserDao.class);
        UserService userService = new UserService(userDao, passwordPattern, emailPattern);

        String errorMessage;

        errorMessage = userService.getCreateErrorMessage("", "lastName", "address@domain.com", "Qwerty12");
        assertEquals("enter.first.name.message", errorMessage);

        errorMessage = userService.getCreateErrorMessage("firstName", "", "address@domain.com", "Qwerty12");
        assertEquals("enter.last.name.message", errorMessage);

        errorMessage = userService.getCreateErrorMessage("firstName", "lastName", "address_domain.com", "Qwerty12");
        assertEquals("enter.valid.email.message", errorMessage);
    }

    @Test
    void getRegistrationErrorMessage_email_is_in_use_test() {
        UserDao userDao = mock(UserDao.class);
        when(userDao.getByEmail(anyString())).thenReturn(Optional.of(new User()));

        UserService userService = new UserService(userDao, passwordPattern, emailPattern);

        String errorMessage = userService.getCreateErrorMessage("firstName", "lastName", "address@domain.com", "Qwerty12");
        assertEquals("email.is.in.use.message", errorMessage);
    }

    @ParameterizedTest
    @MethodSource("getEditErrorMessage_test_cases")
    void getEditErrorMessage_test(String error, String firstName, String lastname, String email, String password){
        UserDao userDao = mock(UserDao.class);
        UserService userService = new UserService(userDao, passwordPattern, emailPattern);
        assertEquals(error, userService.getEditErrorMessage(firstName, lastname, email, password));
    }

    static Stream<Arguments> getEditErrorMessage_test_cases(){
        return Stream.of(
                arguments("enter.first.name.message", "", "lastName", "address@domain.com", "Qwerty12"),
                arguments("enter.last.name.message",  "firstName", "", "address@domain.com", "Qwerty12"),
                arguments("enter.valid.email.message",  "firstName", "lastName", "address@@domain.com", "Qwerty12"),
                arguments("enter.valid.password.message",  "firstName", "lastName", "address@domain.com", "Qwerty"),
                arguments("",  "firstName", "lastName", "address@domain.com", "Qwerty12"));
    }

    @Test
    void getRegistrationErrorMessage_success_test() {
        UserDao userDao = mock(UserDao.class);
        when(userDao.getByEmail(anyString())).thenReturn(Optional.empty());

        UserService userService = new UserService(userDao, passwordPattern, emailPattern);

        String errorMessage = userService.getCreateErrorMessage("firstName", "lastName", "address@domain.com", "Qwerty12");
        assertEquals("", errorMessage);
    }

    @ParameterizedTest
    @MethodSource("getRoomsByParams_errors_result_test_cases")
    void sha3_256Hex_test(String expectedHash, String password){
        UserDao userDao = mock(UserDao.class);
        UserService userService = new UserService(userDao, passwordPattern, emailPattern);
        assertEquals(expectedHash, userService.sha3_256Hex(password));

    }

    static Stream<Arguments> getRoomsByParams_errors_result_test_cases(){
        return Stream.of(
                arguments("bijEcgZ4O401p6LWe0U6ea9R9sjVExAUTW1rxTYUFHQ", "Qwerty12"),
                arguments("BmZ4thHcHQ6fEc1uLxMRrm/afjPNfuTCpy20KERL1EI", "Poiuy@341"),
                arguments("nojuDiijLTv3hT1IFvjdiH3Z+t8udQNOrwMw/AX19Tg", "Password529"),
                arguments("cLsCVEy+f5xAf0Bx2W6gAqqL+b5Bkr7CfYIfQpZWnxc", "IWvaMnefWn"));
    }
}

<%@ include file="header.jsp" %>
<%--@elvariable id="roomClasses" type="java.util.List<org.vtokarev.domain.RoomClass>"--%>
<%--@elvariable id="capacities" type="int[]"--%>
<%--@elvariable id="checkin" type="java.lang.String"--%>
<%--@elvariable id="checkout" type="java.lang.String"--%>
<%--@elvariable id="search" type="java.lang.String"--%>
<%--@elvariable id="roomClassId" type="int"--%>
<%--@elvariable id="personsNumber" type="int"--%>
<%--@elvariable id="showOnlyAvailable" type="java.lang.String"--%>
<%--@elvariable id="rooms" type="java.util.List<org.vtokarev.dto.RoomSearchDto>"--%>
<%--@elvariable id="orderBy" type="java.lang.String"--%>
<%--@elvariable id="errorMessage" type="java.lang.String"--%>
<%--@elvariable id="pages" type="java.util.List<Integer>"--%>
<%--@elvariable id="page" type="int"--%>
<c:choose>
    <c:when test="${search == 'yes'}">
        <c:set var="roomClassIdValue" value="${roomClassId}"/>
        <c:set var="personsNumberValue" value="${personsNumber}"/>
    </c:when>
    <c:otherwise>
        <c:set var="roomClassIdValue" value=""/>
        <c:if test="${fn:length(capacities) <= 1}">
            <c:set var="personsNumberValue" value="1"/>
        </c:if>
        <c:if test="${fn:length(capacities) > 1}">
            <c:set var="personsNumberValue" value="2"/>
        </c:if>
        <c:set var="showOnlyAvailable" value="on"/>
    </c:otherwise>
</c:choose>
<div class="row">
    <div class="col s3">
        <br>
        <div class="z-depth-1-half">
            <div class="container">
                <br>
                <form action="searchRoom" name="searchRoom" id="searchRoom" method="get">
                    <input type="hidden" name="search" value="yes">
                    <input type="hidden" name="page" value="1">
                    <div class="input-field">
                        <input type="text" class="datepicker" id="checkin" name="checkin" required>
                        <label for="checkin"><fmt:message key="checkIn.label"/></label>
                    </div>
                    <div class="input-field">
                        <input type="text" class="datepicker" id="checkout" name="checkout" required>
                        <label for="checkout"><fmt:message key="checkOut.label"/></label>
                    </div>
                    <div class="input-field">
                        <select name="roomClassId">
                            <option selected><fmt:message key="any.label"/></option>
                            <c:forEach items="${roomClasses}" var="item">
                                <option value="${item.id}"
                                        <c:if test="${item.id == roomClassIdValue}">selected</c:if>>${item.name}</option>
                            </c:forEach>
                        </select>
                        <label><fmt:message key="roomClass.label"/></label>
                    </div>
                    <div class="input-field">
                        <select name="personsNumber" required>
                            <option value="" disabled selected><fmt:message key="guests.label"/></option>
                            <c:forEach items="${capacities}" var="item">
                                <option value="${item}"
                                        <c:if test="${item == personsNumberValue}">selected</c:if>>${item}</option>
                            </c:forEach>
                        </select>
                        <label><fmt:message key="guests.label"/></label>
                    </div>
                    <div>
                        <div class="input-field">
                            <select name="orderBy" required>
                                <option value="Name" <c:if test="${orderBy == 'Name'}">selected</c:if>><fmt:message
                                        key="name.label"/></option>
                                <c:choose>
                                    <c:when test="${orderBy == null || orderBy == ''}">
                                        <option value="Price" selected><fmt:message key="price.label"/></option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="Price" <c:if test="${orderBy == 'Price'}">selected</c:if>>
                                            <fmt:message key="price.label"/>
                                        </option>
                                    </c:otherwise>
                                </c:choose>
                                <option value="Class" <c:if test="${orderBy == 'Class'}">selected</c:if>><fmt:message
                                        key="class.label"/></option>
                                <option value="Capacity" <c:if test="${orderBy == 'Capacity'}">selected</c:if>>
                                    <fmt:message key="maxPersons.label"/>
                                </option>
                                <option value="Status" <c:if test="${orderBy == 'Status'}">selected</c:if>><fmt:message
                                        key="status.label"/>
                                </option>
                            </select>
                            <label><fmt:message key="orderBy.label"/></label>
                        </div>
                    </div>
                    <label>
                        <input type="checkbox" name="showOnlyAvailable"
                               <c:if test="${showOnlyAvailable == 'on'}">checked="checked"</c:if>/>
                        <span><fmt:message key="showOnlyAvailableRooms.label"/></span>
                    </label>
                    <div class="input-field">
                        <button class="btn waves-effect waves-light cyan darken-3" type="submit"><fmt:message
                                key="search.action"/>
                            <i class="material-icons right">search</i>
                        </button>
                    </div>
                    <br>
                </form>
            </div>
        </div>
    </div>
    <div class="col s9">
        <br>
        <div class="col s12">
            <c:if test="${not empty param.errorMessage}">
                <br><br><br><br><br><br>
                <div class="col s8 offset-s2">
                    <blockquote class="red-text">
                        <h6><fmt:message key="${param.errorMessage}"/></h6>
                    </blockquote>
                </div>
            </c:if>
            <c:if test="${rooms.size() > 0}">
                <div class="col s12">
                    <div class="right">
                        <ul class="pagination">
                            <c:if test="${param.page == 1}">
                                <li class="disabled"><a
                                        href="searchRoom?search=yes&page=${param.page}&checkin=${param.checkin}&checkout=${param.checkout}&roomClassId=${param.roomClassId}&personsNumber=${param.personsNumber}&orderBy=${param.orderBy}&showOnlyAvailable=${param.showOnlyAvailable}">
                                    <i class="material-icons">chevron_left</i></a></li>
                            </c:if>
                            <c:if test="${param.page > 1}">
                                <li class="waves-effect"><a
                                        href="searchRoom?search=yes&page=${param.page - 1}&checkin=${param.checkin}&checkout=${param.checkout}&roomClassId=${param.roomClassId}&personsNumber=${param.personsNumber}&orderBy=${param.orderBy}&showOnlyAvailable=${param.showOnlyAvailable}">
                                    <i class="material-icons">chevron_left</i></a></li>
                            </c:if>
                            <c:forEach items="${pages}" var="p">
                                <c:if test="${p == page}">
                                    <li class="active cyan darken-3"><a
                                            href="searchRoom?search=yes&page=${p}&checkin=${param.checkin}&checkout=${param.checkout}&roomClassId=${param.roomClassId}&personsNumber=${param.personsNumber}&orderBy=${param.orderBy}&showOnlyAvailable=${param.showOnlyAvailable}">${p}</a>
                                    </li>
                                </c:if>
                                <c:if test="${p != page}">
                                    <li class="waves-effect"><a
                                            href="searchRoom?search=yes&page=${p}&checkin=${param.checkin}&checkout=${param.checkout}&roomClassId=${param.roomClassId}&personsNumber=${param.personsNumber}&orderBy=${param.orderBy}&showOnlyAvailable=${param.showOnlyAvailable}">${p}</a>
                                    </li>
                                </c:if>
                            </c:forEach>

                            <c:if test="${param.page == pages.size()}">
                                <li class="disabled"><a
                                        href="searchRoom?search=yes&page=${param.page}&checkin=${param.checkin}&checkout=${param.checkout}&roomClassId=${param.roomClassId}&personsNumber=${param.personsNumber}&orderBy=${param.orderBy}&showOnlyAvailable=${param.showOnlyAvailable}"><i
                                        class="material-icons">chevron_right</i></a></li>
                            </c:if>
                            <c:if test="${param.page < pages.size()}">
                                <li class="waves-effect"><a
                                        href="searchRoom?search=yes&page=${param.page + 1}&checkin=${param.checkin}&checkout=${param.checkout}&roomClassId=${param.roomClassId}&personsNumber=${param.personsNumber}&orderBy=${param.orderBy}&showOnlyAvailable=${param.showOnlyAvailable}"><i
                                        class="material-icons">chevron_right</i></a></li>
                            </c:if>
                        </ul>
                    </div>
                </div>
            </c:if>
            <c:forEach items="${rooms}" var="roomDto">
                <div class="col s3 z-depth-1-half my-card">
                    <div class="s12">
                        <h6>${roomDto.room.name}</h6><br>
                        <span><fmt:message key="number.booking"/>: <b>${roomDto.room.number}</b></span><br>
                        <span><fmt:message key="roomClass.label"/>: ${roomDto.roomClassName}</span><br>
                        <span><fmt:message key="maxPersons.label"/>: ${roomDto.room.capacity}</span><br>
                        <span><fmt:message key="description.label"/>: ${roomDto.room.description}</span><br>
                        <h6><fmt:message key="price.label"/>: ${roomDto.room.price}</h6><br>
                    </div>
                    <div class="row">
                        <div class="s12">
                            <c:forEach items="${roomDto.images}" var="img">
                                <div class="col s2 my-img">
                                    <img class="materialboxed" width="50" src="${img}" alt="">
                                </div>
                            </c:forEach>
                        </div>
                    </div>
                    <div class="row">
                        <div class="s12">
                            <c:choose>
                                <c:when test="${roomDto.status == 'available.label'}">
                                    <div class="col s4 left">
                                        <h6 class="green-text"><b><fmt:message key="${roomDto.status}"/></b></h6>
                                    </div>
                                    <div class="col s8 right">
                                        <a class="waves-effect waves-light btn-small right cyan darken-3"
                                           href="bookRoomGuest?roomId=${roomDto.room.id}&checkin=${param.checkin}&checkout=${param.checkout}"><fmt:message
                                                key="book.action"/></a>
                                    </div>
                                </c:when>
                                <c:otherwise>
                                    <div class="col s4 left">
                                        <h6 class="red-text"><b><fmt:message key="${roomDto.status}"/></b></h6>
                                    </div>
                                    <div class="col s8 right">
                                        <a class="waves-effect waves-light btn-small right cyan darken-3 disabled"><fmt:message
                                                key="book.action"/></a>
                                    </div>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>
            </c:forEach>
        </div>
    </div>
</div>
<br>
<script>
    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('.materialboxed');
        var instances = M.Materialbox.init(elems);
    });
    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('select');
        var instances = M.FormSelect.init(elems);
    });
    <c:choose>
        <c:when test="${param.search == 'yes'}">
            var checkin_date = new Date('${param.checkin}');
            var checkout_date = new Date('${param.checkout}');
        </c:when>
        <c:otherwise>
            var checkin_date = new Date();
            var checkout_date = new Date();
            checkout_date.setDate(checkout_date.getDate() + 1);
        </c:otherwise>
    </c:choose>
    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('[name="checkin"]');
        var instances = M.Datepicker.init(elems, {
            autoClose: true,
            firstDay: 1,
            format: 'yyyy-mm-dd',
            minDate: new Date(),
            defaultDate: checkin_date,
            setDefaultDate: true
        });
    });
    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('[name="checkout"]');
        var instances = M.Datepicker.init(elems, {
            autoClose: true,
            firstDay: 1,
            format: 'yyyy-mm-dd',
            minDate: new Date(),
            defaultDate: checkout_date,
            setDefaultDate: true
        });
    });
</script>
<%@ include file="footer.jsp" %>
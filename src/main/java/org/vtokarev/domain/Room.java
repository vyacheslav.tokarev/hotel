package org.vtokarev.domain;

import java.io.Serializable;
import java.util.Objects;

/**
 * The {@code Room} class represents the entity of room.
 *
 * @author Vyacheslav Tokarev
 */

public class Room implements Serializable {
    private long id;
    private String name;
    private String number;
    private int capacity;
    private long roomClassId;
    private String description;
    private double price;
    private boolean available;

    public Room() {
    }

    public Room(long id, String name, String number, int capacity, long roomClassId,
                String description, double price, boolean available) {
        this.id = id;
        this.name = name;
        this.number = number;
        this.capacity = capacity;
        this.roomClassId = roomClassId;
        this.description = description;
        this.price = price;
        this.available = available;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public long getRoomClassId() {
        return roomClassId;
    }

    public void setRoomClassId(long roomClassId) {
        this.roomClassId = roomClassId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Room room = (Room) o;
        return getId() == room.getId() && getCapacity() == room.getCapacity() &&
                getRoomClassId() == room.getRoomClassId() && Double.compare(room.getPrice(), getPrice()) == 0 &&
                isAvailable() == room.isAvailable() && Objects.equals(getName(), room.getName()) &&
                Objects.equals(getNumber(), room.getNumber()) && Objects.equals(getDescription(), room.getDescription());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getNumber(), getCapacity(), getRoomClassId(),
                getDescription(), getPrice(), isAvailable());
    }

    @Override
    public String toString() {
        return "Room{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", number='" + number + '\'' +
                ", capacity=" + capacity +
                ", roomClassId=" + roomClassId +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", available=" + available +
                '}';
    }
}


package org.vtokarev.domain;

import java.io.Serializable;

/**
 * The {@code BookingStatus} class represents the entity of status of the {@link Booking}. {@code RESERVED} means that the booking was made but not paid yet.
 * {@code BOOKED} means that the booking is paid. {@code CANCELLED} - the booking was canceled by the guest or automatically if the payment hasn't been made in time.
 *
 * @author Vyacheslav Tokarev
 */

public enum BookingStatus implements Serializable {

    RESERVED,
    BOOKED,
    CANCELLED;

    @Override
    public String toString() {
        return this.name();
    }
}
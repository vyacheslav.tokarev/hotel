package org.vtokarev.service;

import org.vtokarev.dao.BookingDao;
import org.vtokarev.domain.BookingStatus;
import org.vtokarev.dto.BookingDto;

import java.time.*;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Stream;

/**
 * {@code BookingService} is a service layer class that implements methods for booking room operations.
 *
 * @author Vyacheslav Tokarev
 */


public class BookingService {

    final private BookingDao bookingDao;
    final private ZoneOffset zoneOffset;

    public BookingService(BookingDao bookingDao, ZoneOffset zoneOffset) {
        this.bookingDao = bookingDao;
        this.zoneOffset = zoneOffset;
    }

    public void updateBookingStatusById(long bookingId, BookingStatus bookingStatus) {
        bookingDao.get(bookingId).ifPresent(b -> {
            b.setStatus(bookingStatus);
            bookingDao.update(b);
        });
    }

    public String bookRoom(long userId, long roomId, double price, LocalDate checkin, LocalDate checkout, Long requestId, String managerNotes) {

        if (!bookingDao.bookRoom(checkin, checkout, userId, roomId, price, requestId, managerNotes)) {
            return "selected.room.is.not.available.message";
        }
        return null;
    }

    public List<BookingDto> getBookingsByUserId(long userId, boolean isActive) {

        Stream<BookingDto> bookingDtoStream = bookingDao.getBookingsByUserId(userId).stream();
        if (isActive)
            bookingDtoStream = bookingDtoStream.filter(this::isBookingsActive);

        return bookingDtoStream.map(this::calcBookingDtoTimeLeft).toList();
    }

    BookingDto calcBookingDtoTimeLeft(BookingDto bookingDto) {
        BookingDto b = new BookingDto(bookingDto);

        if (bookingDto == null || bookingDto.getBooking() == null)
            return b;

        if (bookingDto.getBooking().getStatus() == BookingStatus.RESERVED) {
            long seconds = Duration.between(Instant.now(), bookingDto.getBooking().getStatusChanged().plus(2, ChronoUnit.DAYS)).getSeconds();
            long HH = seconds / 3600;
            long MM = (seconds % 3600) / 60;
            long SS = seconds % 60;
            b.setTimeLeft(String.format("%02d:%02d:%02d", HH, MM, SS));
        }
        return b;
    }
    private boolean isBookingsActive(BookingDto bookingDto) {
        if (bookingDto == null ||
                bookingDto.getBooking() == null ||
                bookingDto.getBooking().getStatus() == null ||
                bookingDto.getBooking().getCheckin() == null)
            return false;

        if (bookingDto.getBooking().getStatus() == BookingStatus.CANCELLED)
            return false;
        Instant now = Instant.now();
        LocalDate nowLd = LocalDateTime.ofInstant(now, zoneOffset).toLocalDate();
        return !nowLd.isAfter(bookingDto.getBooking().getCheckin());
    }

    public BookingDto getBookingDtoById(long bookingId) {
        BookingDto bookingDto = bookingDao.getBookingById(bookingId);
        return calcBookingDtoTimeLeft(bookingDto);
    }
}
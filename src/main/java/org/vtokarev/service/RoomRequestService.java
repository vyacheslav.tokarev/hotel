package org.vtokarev.service;

import org.vtokarev.dao.RoomRequestDao;
import org.vtokarev.domain.RoomRequest;
import org.vtokarev.dto.RoomRequestDto;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * {@code RoomRequestService} is a service layer class that implements methods for operations with room requests.
 *
 * @author Vyacheslav Tokarev
 */

public class RoomRequestService {

    private final RoomRequestDao roomRequestDao;
    private final RoomService roomService;
    private final RoomClassService roomClassService;

    public RoomRequestService(RoomRequestDao roomRequestDao, RoomService roomService, RoomClassService roomClassService) {
        this.roomRequestDao = roomRequestDao;
        this.roomService = roomService;
        this.roomClassService = roomClassService;
    }

    public List<RoomRequestDto> getRequestsByUserId(Long userId) {
        return roomRequestDao.getRequestsByUserId(userId);
    }
    public String getRegistrationErrorMessage(LocalDate checkin, LocalDate checkout, long roomClassId, int personsNumber) {

        if (checkin.isEqual(checkout) || checkin.isAfter(checkout)) {
            return "checkin.must.be.less.than.checkout.message";
        }

        if (checkin.isBefore(LocalDate.now())) {
            return "checkin.cannot.be.in.the.past.message";
        }

        int maxCapacity = roomService.getMaxCapacity();

        if (personsNumber <= 0 || personsNumber > maxCapacity) {
            return "no.rooms.with.the.entered.capacity.message";
        }

        if(roomClassService.get(roomClassId).isEmpty()){
            return "no.rooms.with.the.entered.room.class.message";
        }

        return "";
    }

    public void create(RoomRequest roomRequest){
        roomRequestDao.create(roomRequest);
    }

    public Optional<RoomRequest> get(long id){
        return roomRequestDao.get(id);
    }

    public void update(RoomRequest roomRequest) {
        roomRequestDao.update(roomRequest);
    }
}

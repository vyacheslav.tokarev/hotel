package org.vtokarev.dao;

import org.vtokarev.domain.Booking;
import org.vtokarev.dto.BookingDto;

import java.time.Instant;
import java.time.LocalDate;
import java.util.List;

/**
 * {@code BookingDao} interface extends {@code GenericDao} interface.
 * It defines the methods for booking room operations with a persistence layer.
 *
 * @author Vyacheslav Tokarev
 */


public interface BookingDao extends GenericDao<Booking> {
    List<BookingDto> getBookingsByUserId(long userId);

    BookingDto getBookingById(long bookingId);

    boolean bookRoom(LocalDate checkin, LocalDate checkout, long userId, long roomId, double price, Long requestId, String managerNotes);

    int updateBreachedReservedBookingStatus(Instant statusChanged, LocalDate checkin);
}

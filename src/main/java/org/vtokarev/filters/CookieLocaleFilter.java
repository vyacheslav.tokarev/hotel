package org.vtokarev.filters;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The {@code AuthenticationFilter} is a class that implements {@link Filter} interface.
 * It set {@code "UTF-8"} encoding for HTTP requests and responses.
 * Considering that text data are stored using {@code "UTF-8"} encoding at the backend, it provides support for non-Latin languages, specifically Cyrillic.
 * Also, it instructs browser not to use caching. That avoids issues with dynamic content.
 *
 * @author Vyacheslav Tokarev
 */


public class CookieLocaleFilter implements Filter {
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        if (req.getParameter("cookieLocale") != null) {
            Cookie cookie = new Cookie("lang", req.getParameter("cookieLocale"));
            res.addCookie(cookie);

            String queryString = req.getQueryString();

            queryString = queryString
                    .replaceAll("cookieLocale=ru&", "?")
                    .replaceAll("cookieLocale=en&", "?")
                    .replaceAll("cookieLocale=ru", "")
                    .replaceAll("cookieLocale=en", "");

            String redirectString = req.getRequestURI() + queryString;

            res.sendRedirect(redirectString);
            return;
        }
        chain.doFilter(request, response);
    }
}

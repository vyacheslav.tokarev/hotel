<%@ include file="header.jsp" %>
<%@ page import="org.vtokarev.domain.UserRole" %>
<%@ page import="org.vtokarev.domain.RoomRequestStatus" %>
<%--@elvariable id="roomRequests" type="java.util.List<org.vtokarev.dto.RoomRequestDto>"--%>
<%--@elvariable id="user" type="org.vtokarev.domain.User"--%>
<%--@elvariable id="zoneOffset" type="java.lang.String"--%>
<br>
<div class="row">
    <c:if test="${user.userRole == UserRole.GUEST}">
        <div class="col s2">
            <a href="createRequest" class="btn waves-effect waves-light cyan darken-3"><fmt:message
                    key="applyNewRequest.action"/></a>
        </div>
    </c:if>
    <div class="col s12">
        <div class="container-fluid">
            <div class="row">
                <table class="highlight centered">
                    <thead>
                    <tr>
                        <th><fmt:message key="checkIn.label"/></th>
                        <th><fmt:message key="checkOut.label"/></th>
                        <th><fmt:message key="persons.label"/></th>
                        <th><fmt:message key="roomClass.label"/></th>
                        <c:if test="${user.userRole == UserRole.MANAGER}">
                            <th><fmt:message key="guests.label"/></th>
                        </c:if>
                        <th><fmt:message key="guestNotes.label"/></th>
                        <th><fmt:message key="hotelNotes.label"/></th>
                        <th><fmt:message key="status.label"/></th>
                        <th><fmt:message key="statusUpdated.label"/></th>
                        <th><fmt:message key="booking.label"/></th>
                        <th><fmt:message key="actions.label"/></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="item" items="${roomRequests}">
                        <tr>
                            <td>${item.roomRequest.checkin}</td>
                            <td>${item.roomRequest.checkout}</td>
                            <td>${item.roomRequest.personsNumber}</td>
                            <td>${item.roomClassName}</td>
                            <c:if test="${user.userRole == UserRole.MANAGER}">
                                <td>${item.userEmail}</td>
                            </c:if>
                            <td>${item.roomRequest.guestNotes}</td>
                            <td>${item.roomRequest.managerNotes}</td>
                            <td><fmt:message key="${item.roomRequest.status}"/></td>
                            <td><ct:formatLocalDateTime value="${item.roomRequest.regDate}" pattern="yyyy-MM-dd HH:mm" zoneOffset="${zoneOffset}"/></td>
                            <td>
                                <c:if test="${item.roomRequest.bookingId != 0}">
                                    <a href="booking?id=${item.roomRequest.bookingId}&back=requestList">BK-${item.roomRequest.bookingId}</a>
                                </c:if>
                            </td>
                            <td>
                                <c:if test="${user.userRole == UserRole.GUEST && item.roomRequest.status == RoomRequestStatus.NEW}">
                                    <a class="waves-effect waves-light btn-small modal-trigger red"
                                       href="#modal${item.roomRequest.id}"><i
                                            class="material-icons right">delete</i><fmt:message key="cancelRequest.label" /></a>
                                    <div id="modal${item.roomRequest.id}" class="modal">
                                        <form action="proceedRequest" method="post">
                                            <input type="hidden" name="requestId" value="${item.roomRequest.id}">
                                            <div class="modal-content">
                                                <h5><fmt:message key="cancelRequest.label"/> ?</h5>
                                                <div class="container">
                                                    <p class="left-align">
                                                        <br>
                                                        <fmt:message key="checkIn.label"/>:
                                                        <b>${item.roomRequest.checkin}</b> <br>
                                                        <fmt:message key="checkOut.label"/>:
                                                        <b>${item.roomRequest.checkout}</b><br>
                                                        <fmt:message key="persons.label"/>:
                                                        <b>${item.roomRequest.personsNumber}</b><br>
                                                        <fmt:message key="roomClass.label"/>:
                                                        <b>${item.roomClassName}</b><br>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn-small waves-effect waves-light red"
                                                        type="submit" name="cancel" value="true"><fmt:message key="yes.action"/><i
                                                        class="material-icons right">delete</i>
                                                </button>
                                                <a href="#"
                                                   class="modal-close waves-effect waves-light btn-small light-blue darken-4"><fmt:message
                                                        key="close.action"/><i
                                                        class="material-icons right">close</i></a>
                                            </div>
                                        </form>
                                    </div>
                                </c:if>
                                <c:if test="${user.userRole == UserRole.MANAGER && item.roomRequest.status == RoomRequestStatus.NEW}">
                                    <a href="proceedRequest?page=1&requestId=${item.roomRequest.id}&userId=${item.roomRequest.userId}"
                                       class="btn-small waves-effect waves-light cyan darken-3">
                                        <i class="material-icons right">search</i><fmt:message key="searchRoom.label"/></a>
                                </c:if>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('.modal');
        var instances = M.Modal.init(elems);
    });
</script>
<%@ include file="footer.jsp" %>
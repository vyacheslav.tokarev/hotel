package org.vtokarev.domain;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Objects;

/**
 * The {@code Booking} class represents the entity of booking. Booking is created for users with GUEST role, the instance of {@link UserRole}.
 *
 * @author Vyacheslav Tokarev
 */

public class Booking implements Serializable {

    private long id;
    private LocalDate checkin;
    private LocalDate checkout;
    private long roomId;
    private long userId;
    private BookingStatus status;
    private double price;
    private Instant statusChanged;

    public Booking() {
    }

    public Booking(long id, LocalDate checkin, LocalDate checkout, long roomId, long userId, BookingStatus status,
                   double price, Instant statusChanged) {
        this.id = id;
        this.checkin = checkin;
        this.checkout = checkout;
        this.roomId = roomId;
        this.userId = userId;
        this.status = status;
        this.price = price;
        this.statusChanged = statusChanged.truncatedTo(ChronoUnit.MICROS);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDate getCheckin() {
        return checkin;
    }

    public void setCheckin(LocalDate checkin) {
        this.checkin = checkin;
    }

    public LocalDate getCheckout() {
        return checkout;
    }

    public void setCheckout(LocalDate checkout) {
        this.checkout = checkout;
    }

    public long getRoomId() {
        return roomId;
    }

    public void setRoomId(long roomId) {
        this.roomId = roomId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public BookingStatus getStatus() {
        return status;
    }

    public void setStatus(BookingStatus status) {
        this.status = status;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Instant getStatusChanged() {
        return statusChanged;
    }

    public void setStatusChanged(Instant statusChanged) {
        this.statusChanged = statusChanged.truncatedTo(ChronoUnit.MICROS);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Booking booking = (Booking) o;
        return getId() == booking.getId() && getRoomId() == booking.getRoomId() &&
                getUserId() == booking.getUserId() && Double.compare(booking.getPrice(), getPrice()) == 0 &&
                Objects.equals(getCheckin(), booking.getCheckin()) && Objects.equals(getCheckout(), booking.getCheckout()) &&
                getStatus() == booking.getStatus() && Objects.equals(getStatusChanged(), booking.getStatusChanged());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getCheckin(), getCheckout(), getRoomId(),
                getUserId(), getStatus(), getPrice(), getStatusChanged());
    }
}

<%@page isErrorPage="true" %>
<%@ include file="header.jsp" %>
<div class="row">
    <br><br><br>
    <div class="col s6 offset-s3">
        <div class="card cyan darken-4">
            <div class="card-content white-text">
                <span class="card-title"><fmt:message key="somethingWentWrong.label"/></span>
                <p><fmt:message key="cannotProceedRequest.label"/></p>
            </div>
            <div class="card-action center">
                <a href="${pageContext.request.contextPath}/home"><fmt:message key="homePage.label"/></a>
            </div>
        </div>
    </div>
</div>
<%@ include file="footer.jsp" %>
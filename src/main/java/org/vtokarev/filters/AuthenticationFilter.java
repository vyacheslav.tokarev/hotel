package org.vtokarev.filters;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.vtokarev.domain.User;
import org.vtokarev.domain.UserRole;
import org.vtokarev.tuples.Triplet;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.Set;
import java.util.stream.Stream;

/**
 * The {@code AuthenticationFilter} is a class that implements {@link Filter} interface.
 * It checks if the request is created by an authenticated user.
 * If the user is not authenticated and the target resources are not in the exception list, the filter sends a redirect to the login page, otherwise the request continues further processing.
 *
 * @author Vyacheslav Tokarev
 */


public class AuthenticationFilter implements Filter {

    private final static Logger LOGGER = LogManager.getLogger(MethodHandles.lookup().lookupClass());

    Stream<Triplet<String, String, Set<UserRole>>> accessList;

/*
    String[] pathsToBeIgnored;
*/


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
/*
        String ignoredPathStr = filterConfig.getInitParameter("pathToBeIgnored");
        pathsToBeIgnored = ignoredPathStr.split(";");
*/
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;

        String path = request.getRequestURI();
        String contextPath = request.getContextPath();
        String method = request.getMethod();

        LOGGER.debug("path = {}, contextPath={}, method={}", path, contextPath, method);

        initSecurityAccessList();

        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");

        UserRole userRole = user == null ? UserRole.UNKNOWN : user.getUserRole();

        final String finalPath = path;

        HttpServletResponse response = (HttpServletResponse) servletResponse;
        if (path.contains("//")) {
            response.sendRedirect(contextPath);
            return;
        }

        boolean isNotAuthorized = accessList.noneMatch(e ->
                (finalPath.equals(contextPath + e.getA()) || finalPath.startsWith(contextPath + e.getA() + "/"))
                        && method.equals(e.getB())
                        && e.getC().contains(userRole));

        LOGGER.debug("isNotAuthorized = {}", isNotAuthorized);

        if (isNotAuthorized) {
            response.sendRedirect(contextPath + "/login");
            return;
        }

        filterChain.doFilter(servletRequest, servletResponse);


/*
        if (path.replaceAll("/", "").equals(contextPath.replaceAll("/", "")) ||
                Arrays.stream(pathsToBeIgnored).anyMatch(path::contains)) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }

        if (session.getAttribute("user") == null) {
            HttpServletResponse response = (HttpServletResponse) servletResponse;
            return;
        }
        filterChain.doFilter(servletRequest, servletResponse);
*/
    }


    private void initSecurityAccessList() {

        accessList = Stream.of(
                new Triplet<>("/bookingList", "GET", Set.of(UserRole.GUEST)),

                new Triplet<>("/booking", "GET", Set.of(UserRole.GUEST, UserRole.MANAGER)),

                new Triplet<>("/bookRoomGuest", "GET", Set.of(UserRole.GUEST)),
                new Triplet<>("/bookRoomGuest", "POST", Set.of(UserRole.GUEST)),

                new Triplet<>("/bookRoomManager", "POST", Set.of(UserRole.MANAGER)),

                new Triplet<>("/createRequest", "GET", Set.of(UserRole.GUEST)),
                new Triplet<>("/createRequest", "POST", Set.of(UserRole.GUEST)),

                new Triplet<>("/home", "GET", Set.of(UserRole.GUEST, UserRole.MANAGER, UserRole.ADMIN, UserRole.UNKNOWN)),

                new Triplet<>("/imageList", "GET", Set.of(UserRole.ADMIN)),
                new Triplet<>("/imageList", "POST", Set.of(UserRole.ADMIN)),


                new Triplet<>("/login", "GET", Set.of(UserRole.GUEST, UserRole.MANAGER, UserRole.ADMIN, UserRole.UNKNOWN)),
                new Triplet<>("/login", "POST", Set.of(UserRole.GUEST, UserRole.MANAGER, UserRole.ADMIN, UserRole.UNKNOWN)),

                new Triplet<>("/logout", "GET", Set.of(UserRole.GUEST, UserRole.MANAGER, UserRole.ADMIN)),

                new Triplet<>("/modifyRoom", "GET", Set.of(UserRole.ADMIN)),
                new Triplet<>("/modifyRoom", "POST", Set.of(UserRole.ADMIN)),

                new Triplet<>("/modifyUser", "GET", Set.of(UserRole.ADMIN, UserRole.UNKNOWN)),
                new Triplet<>("/modifyUser", "POST", Set.of(UserRole.ADMIN, UserRole.UNKNOWN)),

                new Triplet<>("/proceedBooking", "POST", Set.of(UserRole.GUEST)),

                new Triplet<>("/proceedRequest", "GET", Set.of(UserRole.MANAGER)),
                new Triplet<>("/proceedRequest", "POST", Set.of(UserRole.GUEST)),

                new Triplet<>("/requestList", "GET", Set.of(UserRole.GUEST, UserRole.MANAGER)),

                new Triplet<>("/roomList", "GET", Set.of(UserRole.ADMIN)),

                new Triplet<>("/searchRoom", "GET", Set.of(UserRole.GUEST)),

                new Triplet<>("/uploadImage", "POST", Set.of(UserRole.ADMIN)),

                new Triplet<>("/userList", "GET", Set.of(UserRole.ADMIN)),

                new Triplet<>("/css", "GET", Set.of(UserRole.GUEST, UserRole.MANAGER, UserRole.ADMIN, UserRole.UNKNOWN)),
                new Triplet<>("/img", "GET", Set.of(UserRole.GUEST, UserRole.MANAGER, UserRole.ADMIN, UserRole.UNKNOWN)),

                new Triplet<>("/", "GET", Set.of(UserRole.GUEST, UserRole.MANAGER, UserRole.ADMIN, UserRole.UNKNOWN))


        );


    }


}

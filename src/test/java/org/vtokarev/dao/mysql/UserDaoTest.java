package org.vtokarev.dao.mysql;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.vtokarev.dao.UserDao;
import org.vtokarev.domain.User;
import org.vtokarev.domain.UserRole;

import javax.sql.DataSource;
import java.lang.invoke.MethodHandles;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

public class UserDaoTest {

    private final static Logger LOGGER = LogManager.getLogger(MethodHandles.lookup().lookupClass());

    @RegisterExtension
    public static SqlMemoryDatabase sqlMemoryDatabase = new SqlMemoryDatabase();

    @Test
    void create_get_test() {
        DataSource ds = sqlMemoryDatabase.dataSource();
        UserDao userDao = new UserDaoImpl(ds);
        User user1 = new User(0,
                "firstName",
                "lastName",
                "firstName.lastName@domain.com",
                "Qwerty12",
                Instant.now(),
                UserRole.GUEST);
        userDao.create(user1);
        Optional<User> user2 = userDao.get(user1.getId());
        assertTrue(user2.isPresent());
        assertTrue(user1.getId() > 0);
        assertEquals(user1, user2.get());
    }


    @Test
    void getByEmail_test() {
        DataSource ds = sqlMemoryDatabase.dataSource();
        UserDao userDao = new UserDaoImpl(ds);
        User user1 = new User(0,
                "firstName",
                "lastname",
                "email@domain.com",
                "Qwerty12",
                Instant.now(),
                UserRole.GUEST);
        userDao.create(user1);
        Optional<User> user2 = userDao.getByEmail(user1.getEmail());
        assertTrue(user2.isPresent());
        assertTrue(user1.getId() > 0);

        LOGGER.debug(user1);
        LOGGER.debug(user2.get());

        assertEquals(user1, user2.get());
    }

    @Test
    void getAll_test(){
        DataSource ds = sqlMemoryDatabase.dataSource();
        UserDao userDao = new UserDaoImpl(ds);

        User user1 = new User(0,
                "firstName1",
                "lastname1",
                "email1@domain.com",
                "Qwerty12",
                Instant.now(),
                UserRole.GUEST);
        userDao.create(user1);

        User user2 = new User(0,
                "firstName2",
                "lastname2",
                "email2@domain.com",
                "Qwerty12",
                Instant.now(),
                UserRole.MANAGER);
        userDao.create(user2);

        List<User> userList = userDao.getAll();
        assertEquals(2, userList.size());

        List<User> user1List = userList.stream().filter(u -> u.getId() == user1.getId()).toList();
        assertEquals(1, user1List.size());
        assertEquals(user1, user1List.get(0));

        List<User> user2List = userList.stream().filter(u -> u.getId() == user2.getId()).toList();
        assertEquals(1, user2List.size());
        assertEquals(user2, user2List.get(0));
    }

    @Test
    void update_test(){
        DataSource ds = sqlMemoryDatabase.dataSource();
        UserDao userDao = new UserDaoImpl(ds);

        User user1 = new User(0,
                "firstName1",
                "lastname1",
                "email1@domain.com",
                "Qwerty12",
                Instant.now(),
                UserRole.GUEST);
        userDao.create(user1);

        user1.setFirstName("firstName2");
        user1.setLastName("firstName2");
        user1.setEmail("email2@domain.com");

        userDao.update(user1);

        Optional<User> user2 = userDao.get(user1.getId());
        assertTrue(user2.isPresent());
        assertEquals(user1, user2.get());
    }


    @Test
    void delete_test(){
    DataSource ds = sqlMemoryDatabase.dataSource();
        UserDao userDao = new UserDaoImpl(ds);

        User user1 = new User(0,
                "firstName1",
                "lastname1",
                "email1@domain.com",
                "Qwerty12",
                Instant.now(),
                UserRole.GUEST);
        userDao.create(user1);
        Optional<User> user2 = userDao.get(user1.getId());
        assertTrue(user2.isPresent());
        assertEquals(user1, user2.get());
        userDao.delete(user1);
        user2 = userDao.get(user1.getId());
        assertFalse(user2.isPresent());
    }
}
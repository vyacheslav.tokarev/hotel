package org.vtokarev.dao;

import org.vtokarev.domain.RoomClass;

/**
 * {@code RoomClassDao} interface extends {@code GenericDao} interface.
 * It defines the methods for operations with room classes on a persistence layer.
 * Actually it contains only methods inherited from {@code GenericDao} superinterface and no new methods.
 *
 * @author Vyacheslav Tokarev
 */


public interface RoomClassDao extends GenericDao<RoomClass> {
}
package org.vtokarev.controller;

import org.vtokarev.dto.RoomEditDto;
import org.vtokarev.service.RoomService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * The {@code RoomListServlet} class processes GET {@code roomList} requests generated by users with ADMIN role.
 * Gets {@link List<RoomEditDto>} set it as an attribute and forward the control to {@code roomList.jsp} view for further processing.
 *
 * @author Vyacheslav Tokarev
 */


@WebServlet(urlPatterns = "/roomList", name = "roomListServlet")
public class RoomListServlet extends HttpServlet {

    private RoomService roomService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        roomService = (RoomService) config.getServletContext().getAttribute("app/RoomService");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<RoomEditDto> rooms = roomService.getAllRoomsEditDto();
        req.setAttribute("rooms", rooms);
        req.getRequestDispatcher("/WEB-INF/jsp/roomList.jsp").forward(req, resp);
    }
}

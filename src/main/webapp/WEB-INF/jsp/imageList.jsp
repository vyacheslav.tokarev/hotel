<%--@elvariable id="realPathDirectory" type="java.lang.String"--%>
<%--@elvariable id="images" type="java.util.List<Pair<String, String>>"--%>
<%--@elvariable id="room" type="org.vtokarev.domain.Room"--%>
<%--@elvariable id="errorMessage" type="java.lang.String"--%>
<%@ include file="header.jsp" %>
<br>
<div class="row">
    <div class="col s4 offset-s4 z-depth-1-half">
        <div class="container-fluid" style="margin: 10px">
            <div class="row">
                <div class="col s12"><fmt:message key="number.booking"/>: <b>${room.number}</b></div>
                <div class="col s12"><fmt:message key="room.booking"/>: ${room.name}</div>
                <c:if test="${not empty images}">
                    <table class="highlight centered">
                        <thead>
                        <tr>
                            <th><fmt:message key="photos.label"/></th>
                            <th><fmt:message key="delete.action"/></th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="img" items="${images}" varStatus="status">
                            <tr>
                                <td>
                                    <div class="col s2 offset-s5 my-img">
                                        <img class="materialboxed" width="50" src="${img.a}" style="margin: -20px"
                                             alt="">
                                    </div>
                                </td>
                                <td>
                                    <a class="waves-effect waves-light btn modal-trigger red"
                                       href="#modal${status.index}"><i
                                            class="material-icons center">delete</i></a>
                                    <div id="modal${status.index}" class="modal">
                                        <form action="imageList" method="post">
                                            <input type="hidden" name="action" value="delete">
                                            <input type="hidden" name="realPathFileName" value="${img.b}">
                                            <input type="hidden" name="back" value="imageList">
                                            <input type="hidden" name="roomId" value="${room.id}">
                                            <div class="modal-content">
                                                <h5><fmt:message key="delete.action"/> ?</h5>
                                                <div class="row">
                                                    <div class="col s4 offset-s3 my-img">
                                                        <img class="materialboxed" width="200" src="${img.a}" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn-small waves-effect waves-light red" type="submit"
                                                        name="edit" value="true"><fmt:message key="delete.action"/><i
                                                        class="material-icons right">delete</i>
                                                </button>
                                                <a href="#!"
                                                   class="modal-close waves-effect waves-light btn-small light-blue darken-4"><fmt:message
                                                        key="cancel.action"/><i
                                                        class="material-icons right">close</i></a>
                                            </div>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </c:if>
            </div>
            <div class="row">
                <form action="uploadImage" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="back" value="imageList">
                    <input type="hidden" name="roomId" value="${room.id}">
                    <input type="hidden" name="realPathDirectory" value="${realPathDirectory}">
                    <div class="file-field input-field">
                        <div class="btn-small cyan darken-3">
                            <span><fmt:message key="upload.image.label"/></span>
                            <input type="file" name="file" onchange="this.form.submit()" accept=".png, .jpg, .jpeg">
                        </div>
                        <div class="file-path-wrapper">
                            <input class="file-path validate" type="text">
                        </div>
                    </div>
                </form>
                <div class="col s6 right">
                    <a class="btn-small waves-effect waves-light light-blue darken-4 right"
                       href="<c:out value="${param.back}">home</c:out>"><i
                            class="material-icons right">logout</i><fmt:message key="close.action"/></a>
                </div>
            </div>
            <blockquote class="red-text">
                <c:if test="${not empty errorMessage}">
                    <fmt:message key="${errorMessage}"/>
                </c:if>
            </blockquote>
        </div>
    </div>
</div>
<script>
    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('.materialboxed');
        var instances = M.Materialbox.init(elems);
    });
    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('.modal');
        var instances = M.Modal.init(elems);
    });
</script>
<%@ include file="footer.jsp" %>
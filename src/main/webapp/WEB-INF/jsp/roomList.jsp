<%@ include file="header.jsp" %>
<%--@elvariable id="rooms" type="java.util.List<org.vtokarev.dto.RoomEditDto>"--%>
<br>
<div class="row">
    <div class="col s2">
        <a href="modifyRoom?action=create&back=roomList" class="btn waves-effect waves-light cyan darken-3"><fmt:message
                key="create.action"/></a>
    </div>
    <div class="col s12">
        <div class="container-fluid">
            <div class="row">
                <table class="highlight centered">
                    <thead>
                    <tr>
                        <th><fmt:message key="number.booking"/></th>
                        <th><fmt:message key="room.booking"/></th>
                        <th><fmt:message key="maxPersons.label"/></th>
                        <th><fmt:message key="roomClass.label"/></th>
                        <th><fmt:message key="price.label"/></th>
                        <th><fmt:message key="description.label"/></th>
                        <th><fmt:message key="available.label"/></th>
                        <th><fmt:message key="edit.action"/></th>
                        <th><fmt:message key="delete.action"/></th>
                        <th><fmt:message key="photos.label"/></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="item" items="${rooms}">
                        <tr>
                            <td>${item.room.number}</td>
                            <td>${item.room.name}</td>
                            <td>${item.room.capacity}</td>
                            <td>${item.roomClass.name}</td>
                            <td>${item.room.price}</td>
                            <td>${item.room.description}</td>
                            <td>
                                <c:choose>
                                    <c:when test="${item.room.available}">
                                        <fmt:message key="yes.action"/>
                                    </c:when>
                                    <c:otherwise>
                                        <fmt:message key="no.action"/>
                                    </c:otherwise>
                                </c:choose>
                            </td>
                            <td>
                                <a href="modifyRoom?roomId=${item.room.id}&action=edit&back=roomList"
                                   class="waves-effect waves-light btn cyan darken-3"><i class="material-icons center">edit</i></a>
                            </td>
                            <td>
                                <a class="waves-effect waves-light btn modal-trigger red"
                                   href="#modal${item.room.id}"><i
                                        class="material-icons center">delete</i></a>
                                <div id="modal${item.room.id}" class="modal">
                                    <form action="modifyRoom" method="post">
                                        <input type="hidden" name="roomId" value="${item.room.id}">
                                        <input type="hidden" name="action" value="delete">
                                        <input type="hidden" name="back" value="roomList">
                                        <div class="modal-content">
                                            <h5><fmt:message key="delete.action"/> ?</h5>
                                            <div class="container">
                                                <p class="left-align">
                                                    <br>
                                                    <fmt:message key="number.booking"/>: <b>${item.room.number}</b> <br>
                                                    <fmt:message key="room.booking"/>: <b>${item.room.name}</b><br>
                                                    <fmt:message key="maxPersons.label"/>:
                                                    <b>${item.room.capacity}</b><br>
                                                    <fmt:message key="roomClass.label"/>:
                                                    <b>${item.roomClass.name}</b><br>
                                                    <fmt:message key="price.label"/>: <b>${item.room.price}</b><br>
                                                    <fmt:message key="description.label"/>:
                                                    <b>${item.room.description}</b><br>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn-small waves-effect waves-light red" type="submit"
                                                    name="edit" value="true"><fmt:message key="delete.action"/><i
                                                    class="material-icons right">delete</i>
                                            </button>
                                            <a href="#"
                                               class="modal-close waves-effect waves-light btn-small light-blue darken-4"><fmt:message
                                                    key="cancel.action"/><i class="material-icons right">close</i></a>
                                        </div>
                                    </form>
                                </div>
                            </td>
                            <td>
                                <a href="imageList?roomId=${item.room.id}&back=roomList"
                                   class="waves-effect waves-light btn cyan darken-3"><i class="material-icons center">photo_camera</i></a>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('.modal');
        var instances = M.Modal.init(elems);
    });
</script>
<%@ include file="footer.jsp" %>
package org.vtokarev.dao;

import org.vtokarev.domain.User;

import java.util.Optional;

/**
 * {@code UserDao} interface extends {@code GenericDao} interface.
 * It defines the methods for operations with users on a persistence layer.
 *
 * @author Vyacheslav Tokarev
 */


public interface UserDao extends GenericDao<User> {
    Optional<User> getByEmail(String email);
    void delete(long id);
}

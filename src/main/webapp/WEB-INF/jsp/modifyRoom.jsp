<%@ include file="header.jsp" %>
<%--@elvariable id="description" type="java.lang.String"--%>
<%--@elvariable id="price" type="double"--%>
<%--@elvariable id="available" type="boolean"--%>
<%--@elvariable id="roomClassId" type="int"--%>
<%--@elvariable id="capacity" type="int"--%>
<%--@elvariable id="roomClasses" type="java.util.List<org.vtokarev.domain.RoomClass>"--%>
<%--@elvariable id="roomNumber" type="java.lang.String"--%>
<%--@elvariable id="roomName" type="java.lang.String"--%>
<%--@elvariable id="roomId" type="int"--%>
<%--@elvariable id="back" type="java.lang.String"--%>
<%--@elvariable id="action" type="java.lang.String"--%>
<%--@elvariable id="errorMessage" type="java.lang.String"--%>
<br><br><br><br>
<div class="row">
    <div class="col s6 offset-s3 z-depth-1">
        <div class="container">
            <form action="modifyRoom" method="post">
                <input type="hidden" name="action" value="${action}">
                <input type="hidden" name="roomId" value="${roomId}">
                <div class="input-field">
                    <input id="roomNumber" value="${roomNumber}" name="roomNumber" type="text"
                           required class="validate" maxlength="6">
                    <label for="roomNumber"><fmt:message key="number.booking"/></label>
                </div>
                <div class="input-field">
                    <input id="roomName" value="${roomName}" name="roomName" type="text"
                           required class="validate" maxlength="50">
                    <label for="roomName"><fmt:message key="room.booking"/></label>
                </div>
                <div class="input-field">
                    <select id="capacity" name="capacity" required>
                        <option value="1" <c:if test="${capacity == 1}">selected</c:if>>1</option>
                        <option value="2" <c:if test="${capacity == 2 || empty capacity}">selected</c:if>>2</option>
                        <option value="3" <c:if test="${capacity == 3}">selected</c:if>>3</option>
                        <option value="4" <c:if test="${capacity == 4}">selected</c:if>>4</option>
                        <option value="5" <c:if test="${capacity == 5}">selected</c:if>>5</option>
                        <option value="6" <c:if test="${capacity == 6}">selected</c:if>>6</option>
                        <option value="7" <c:if test="${capacity == 7}">selected</c:if>>7</option>
                        <option value="8" <c:if test="${capacity == 8}">selected</c:if>>8</option>
                    </select>
                    <label for="capacity"><fmt:message key="maxPersons.label"/></label>
                </div>
                <div class="input-field s12">
                    <select id="roomClassId" name="roomClassId" required>
                        <c:forEach items="${roomClasses}" var="item">
                            <option value="${item.id}"
                                    <c:if test="${item.id == roomClassId}">selected</c:if>>${item.name}</option>
                        </c:forEach>
                    </select>
                    <label for="roomClassId"><fmt:message key="roomClass.label"/></label>
                </div>
                <div class="input-field s12">
                    <input id="price" value="${price}" name="price" type="number"
                           required class="validate" step=".01" min="0" max="10000">
                    <label for="price"><fmt:message key="price.label"/></label>
                </div>
                <div class="input-field s12">
                    <textarea name="description" maxlength="250" id="description"
                              class="materialize-textarea">${description}</textarea>
                    <label for="description"><fmt:message key="description.label"/></label>
                </div>
                <div class="input-field">
                    <div class="switch">
                        <label>
                            <fmt:message key="notAvailable.label"/>
                            <input id="available" name="available" type="checkbox"
                                   <c:if test="${available || empty available}">checked </c:if> />
                            <span class="lever"></span>
                            <fmt:message key="available.label"/>
                        </label>
                    </div>
                </div>
                <br>
                <c:if test="${action == 'edit'}"><c:set var="actionLable" value="save.action"/></c:if>
                <c:if test="${action == 'create'}"><c:set var="actionLable" value="create.action"/></c:if>
                <button class="btn waves-effect waves-light cyan darken-3" type="submit" name="action"><fmt:message
                        key="${actionLable}"/>
                    <i class="material-icons right">done</i>
                </button>
                <a class="waves-effect waves-light btn red" href="${back}"><i
                        class="material-icons right">close</i><fmt:message key="close.action"/></a>
            </form>
            <blockquote class="red-text">
                <c:if test="${not empty errorMessage}">
                    <fmt:message key="${errorMessage}"/>
                </c:if>
            </blockquote>
        </div>
    </div>
</div>
<script>
    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('select');
        var instances = M.FormSelect.init(elems);
    });
</script>
<%@ include file="footer.jsp" %>
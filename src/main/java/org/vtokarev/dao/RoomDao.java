package org.vtokarev.dao;

import org.vtokarev.domain.Room;
import org.vtokarev.dto.RoomEditDto;
import org.vtokarev.dto.RoomSearchDto;
import org.vtokarev.tuples.Pair;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * {@code RoomDao} interface extends {@code GenericDao} interface.
 * It defines the methods for operations with room on a persistence layer.
 *
 * @author Vyacheslav Tokarev
 */


public interface RoomDao extends GenericDao<Room> {
    int getMaxCapacity();
    List<RoomSearchDto> getBookingRooms(LocalDate checkin, LocalDate checkout, boolean showOnlyAvailable, Long roomClassId, Integer personsNumber, String orderBy, Integer limit, Integer offset );
    int getBookingRoomsCount(LocalDate checkin, LocalDate checkout, boolean showOnlyAvailable, Long roomClassId, Integer personsNumber);

    List<RoomEditDto> getAllRoomsEditDto();

    List<Pair<String, String>> getImagePaths(Room room);

    Optional<Room> getByNumber(String number);

    String getImageRealPathDirectory(Room room);

    void deleteImageDirectory(Room room);


    }

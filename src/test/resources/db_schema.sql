ALTER TABLE IF EXISTS bookings DROP CONSTRAINT IF EXISTS fk_bookings_rooms;
ALTER TABLE IF EXISTS bookings DROP CONSTRAINT IF EXISTS fk_bookings_users;

ALTER TABLE IF EXISTS room_requests DROP CONSTRAINT IF EXISTS fk_room_requests_bookings;
ALTER TABLE IF EXISTS room_requests DROP CONSTRAINT IF EXISTS fk_room_requests_room_classes;
ALTER TABLE IF EXISTS room_requests DROP CONSTRAINT IF EXISTS fk_room_requests_users;

ALTER TABLE IF EXISTS rooms DROP CONSTRAINT IF EXISTS fk_rooms_room_classes;

DROP TABLE IF EXISTS users;
CREATE TABLE users
(
    id         int         NOT NULL AUTO_INCREMENT,
    first_name varchar(20) NOT NULL,
    last_name  varchar(20) NOT NULL,
    email      varchar(80) NOT NULL,
    password   varchar(64)          DEFAULT NULL,
    user_role  varchar(10) NOT NULL,
    reg_date   timestamp   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    CONSTRAINT users_chk_1 CHECK ((user_role in ('GUEST', 'MANAGER')))
);

DROP TABLE IF EXISTS rooms;
CREATE TABLE rooms
(
    id            int           NOT NULL AUTO_INCREMENT,
    name          varchar(20)   NOT NULL,
    number        varchar(6)    NOT NULL,
    capacity      int           NOT NULL,
    room_class_id int           NOT NULL,
    description   varchar(255)           DEFAULT NULL,
    price         decimal(5, 2) NOT NULL DEFAULT '0.00',
    available     boolean       NOT NULL DEFAULT '1',
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS bookings;
CREATE TABLE bookings
(
    id             int           NOT NULL AUTO_INCREMENT,
    checkin        date          NOT NULL,
    checkout       date          NOT NULL,
    room_id        int           NOT NULL,
    status         varchar(20)   NOT NULL,
    status_changed timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    user_id        int           NOT NULL,
    price          decimal(5, 2) NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT bookings_chk_1 CHECK ((status in ('RESERVED', 'BOOKED', 'CANCELLED')))

);

DROP TABLE IF EXISTS room_classes;
CREATE TABLE room_classes
(
    id   int         NOT NULL AUTO_INCREMENT,
    name varchar(20) NOT NULL,
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS room_requests;
CREATE TABLE room_requests
(
    id             int         NOT NULL AUTO_INCREMENT,
    checkin        date        NOT NULL,
    checkout       date        NOT NULL,
    persons_number int         NOT NULL,
    room_class_id  int         NOT NULL,
    user_id        int         NOT NULL,
    booking_id     int                  DEFAULT NULL,
    reg_date       timestamp   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    status         varchar(20) NOT NULL,
    guest_notes    varchar(1000)        DEFAULT NULL,
    manager_notes  varchar(1000)        DEFAULT NULL,
    PRIMARY KEY (id),
    CONSTRAINT room_requests_chk_1 CHECK ((status in ('NEW', 'PROVIDED', 'CANCELLED')))

);
ALTER TABLE bookings ADD CONSTRAINT fk_bookings_rooms FOREIGN KEY (room_id) REFERENCES rooms(id) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE bookings ADD CONSTRAINT fk_bookings_users FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE room_requests ADD CONSTRAINT fk_room_requests_bookings FOREIGN KEY (booking_id) REFERENCES bookings(id) ON DELETE CASCADE ON UPDATE RESTRICT;
ALTER TABLE room_requests ADD CONSTRAINT fk_room_requests_room_classes FOREIGN KEY (room_class_id) REFERENCES room_classes(id) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE room_requests ADD CONSTRAINT fk_room_requests_users FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE rooms ADD CONSTRAINT fk_rooms_room_classes FOREIGN KEY (room_class_id) REFERENCES room_classes(id) ON DELETE RESTRICT ON UPDATE RESTRICT;

package org.vtokarev.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.vtokarev.dao.UserDao;
import org.vtokarev.domain.User;

import java.lang.invoke.MethodHandles;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

/**
 * {@code UserService} is a service layer class that implements methods for operations with users.
 *
 * @author Vyacheslav Tokarev
 */

public class UserService {

    public final String passwordPattern;
    public final String emailPattern;
    private final static Logger LOGGER = LogManager.getLogger(MethodHandles.lookup().lookupClass());
    private final UserDao userDao;

    public UserService(UserDao userDao, String passwordPattern, String emailPattern) {
        this.passwordPattern = passwordPattern;
        this.emailPattern = emailPattern;
        this.userDao = userDao;
    }

    public Optional<User> getUserByEmail(String email) {
        return userDao.getByEmail(email);
    }

    public boolean isValid(User user, String password) {
        if (user == null || user.getPassword() == null)
            return false;
        return user.getPassword().equals(sha3_256Hex(password));
    }

    private boolean passwordDoesntMeetRequirements(String password) {
        return !Pattern.matches(passwordPattern, password);
    }

    private boolean emailDoesntMeetRequirements(String email) {
        return !Pattern.matches(emailPattern, email);
    }

    public String getCreateErrorMessage(String firstName, String lastname, String email, String password) {

        if (firstName.isBlank()) {
            return "enter.first.name.message";
        }
        if (lastname.isBlank()) {
            return "enter.last.name.message";
        }
        if (emailDoesntMeetRequirements(email)) {
            return "enter.valid.email.message";
        }
        if (passwordDoesntMeetRequirements(password)) {
            return "enter.valid.password.message";
        }

        Optional<User> user = getUserByEmail(email);
        if (user.isPresent()) {
            return "email.is.in.use.message";
        }
        return "";
    }

    public String getEditErrorMessage(String firstName, String lastname, String email, String password) {
        if (firstName.isBlank()) {
            return "enter.first.name.message";
        }
        if (lastname.isBlank()) {
            return "enter.last.name.message";
        }
        if (emailDoesntMeetRequirements(email)) {
            return "enter.valid.email.message";
        }
        if (!password.isBlank() && passwordDoesntMeetRequirements(password)) {
            return "enter.valid.password.message";
        }
        return "";
    }

    public void create(User user) {
        userDao.create(user);
    }

    public String sha3_256Hex(String str) {
        final MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("SHA3-256");
        } catch (NoSuchAlgorithmException e) {
            LOGGER.error(e.getMessage());
            throw new RuntimeException(e);
        }
        final byte[] hashBytes = digest.digest(str.getBytes(StandardCharsets.UTF_8));
        LOGGER.debug(Base64.getEncoder().withoutPadding().encodeToString(hashBytes));
        return Base64.getEncoder().withoutPadding().encodeToString(hashBytes);
    }

    public List<User> getAll() {
        return userDao.getAll();
    }

    public Optional<User> get(long id) {
        return userDao.get(id);
    }

    public void delete(long id) {
        userDao.delete(id);
    }

    public void update(User user){
        userDao.update(user);
    }

}

package org.vtokarev.service;

import org.vtokarev.dao.RoomClassDao;
import org.vtokarev.domain.RoomClass;

import java.util.List;
import java.util.Optional;

/**
 * {@code RoomClassService} is a service layer class that implements methods for operations with room classes.
 *
 * @author Vyacheslav Tokarev
 */

public class RoomClassService {
    RoomClassDao roomClassDao;

    public RoomClassService(RoomClassDao roomClassDao) {
        this.roomClassDao = roomClassDao;
    }

    public List<RoomClass> getAll(){
        return roomClassDao.getAll();
    }
    public Optional<RoomClass> get(long id){
        return roomClassDao.get(id);
    }
}

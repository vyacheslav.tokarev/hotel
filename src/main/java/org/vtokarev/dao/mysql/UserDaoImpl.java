package org.vtokarev.dao.mysql;

import org.vtokarev.dao.UserDao;
import org.vtokarev.domain.User;
import org.vtokarev.domain.UserRole;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * {@code UserDaoImpl} class is an implementation of {@link UserDao} interface for MySQL persistence layer backend.
 *
 * @author Vyacheslav Tokarev
 */


public class UserDaoImpl extends BaseDaoImpl implements UserDao {

    final private static String UPDATE = "UPDATE users SET first_name = ?, last_name = ?, email = ?, password = ? WHERE id = ?";

    final private static String UPDATE_WITHOUT_PASSWORD = "UPDATE users SET first_name = ?, last_name = ?, email = ? WHERE id = ?";

    final private static String DELETE_BY_ID = "DELETE FROM users WHERE id = ?";

    final private static String GET_BY_ID = "SELECT * FROM users WHERE id = ?";
    final private static String GET_BY_EMAIL = "SELECT * FROM users WHERE email = ?";

    final private static String GET_ALL = "SELECT * FROM users";

    final private static String INSERT = "INSERT INTO users (first_name, last_name, email, password, user_role, reg_date) VALUES (?, ?, ?, ?, ?, ?)";

    public UserDaoImpl(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    public Optional<User> get(long id) {
        Connection connection = getConnection();
        ResultSet rs;
        try (PreparedStatement statement = connection.prepareStatement(GET_BY_ID)) {
            statement.setLong(1, id);
            rs = statement.executeQuery();
            if (rs.next()) {
                logger.debug("Success get");
                return getUser(statement);
            }
        } catch (SQLException e) {
            logger.error("Failed get", e);
            throw new RuntimeException("Failed get", e);
        } finally {
            close(connection);
        }
        logger.debug("Success get with empty result");
        return Optional.empty();
    }

    @Override
    public List<User> getAll() {
        Connection connection = getConnection();
        ResultSet rs = null;
        Statement stmt = null;
        List<User> users = new ArrayList<>();
        try {
            stmt = connection.createStatement();
            rs = stmt.executeQuery(GET_ALL);
            while (rs.next()) {
                User user = getUser(rs);
                users.add(user);
            }
        } catch (SQLException e) {
            logger.error("Failed getAll", e);
            throw new RuntimeException("Failed getAll", e);
        } finally {
            close(rs);
            close(stmt);
            close(connection);
        }
        logger.debug("Success getAll");
        return users;
    }

    @Override
    public void create(User user) {
        Connection connection = getConnection();
        try (PreparedStatement statement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, user.getFirstName());
            statement.setString(2, user.getLastName());
            statement.setString(3, user.getEmail());
            statement.setString(4, user.getPassword());
            statement.setString(5, user.getUserRole().name());
            statement.setTimestamp(6, Timestamp.from(user.getRegDate()));

            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("Creating user failed, no rows affected.");
            }

            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    user.setId(generatedKeys.getLong(1));
                } else {
                    throw new SQLException("Creating user failed, no ID obtained.");
                }
            }
            logger.debug("Success create");
        } catch (SQLException e) {
            logger.error("Failed create", e);
            throw new RuntimeException("Failed create", e);
        } finally {
            close(connection);
        }
    }

    @Override
    public void update(User user) {
        Connection connection = getConnection();
        PreparedStatement stmt = null;
        String sqlUpdate = user.getPassword().isBlank() ? UPDATE_WITHOUT_PASSWORD : UPDATE;
        try {
            stmt = connection.prepareStatement(sqlUpdate);
            int k = 1;
            stmt.setString(k++, user.getFirstName());
            stmt.setString(k++, user.getLastName());
            stmt.setString(k++, user.getEmail());
            if (!user.getPassword().isBlank())
                stmt.setString(k++, user.getPassword());
            stmt.setLong(k, user.getId());
            stmt.executeUpdate();
            logger.debug("Success update");
        } catch (SQLException e) {
            logger.error("Failed update", e);
            throw new RuntimeException("Failed update", e);
        } finally {
            close(stmt);
            close(connection);
        }
    }

    @Override
    public void delete(User user) {
        delete(user.getId());
    }

    public void delete(long id) {
        Connection connection = getConnection();
        PreparedStatement stmt = null;
        try {
            stmt = connection.prepareStatement(DELETE_BY_ID);
            stmt.setLong(1, id);
            stmt.executeUpdate();
            logger.debug("Success delete");
        } catch (SQLException e) {
            logger.error("Failed delete", e);
            throw new RuntimeException("Failed delete", e);
        } finally {
            close(stmt);
            close(connection);
        }
    }

    @Override
    public Optional<User> getByEmail(String email) {
        Connection connection = getConnection();
        try (PreparedStatement statement = connection.prepareStatement(GET_BY_EMAIL)) {
            statement.setString(1, email);
            Optional<User> user = getUser(statement);
            logger.debug("Success getByEmail");
            return user;
        } catch (SQLException e) {
            logger.error("Failed getByEmail", e);
            throw new RuntimeException("Failed getByEmail", e);
        } finally {
            close(connection);
        }
    }

    private Optional<User> getUser(PreparedStatement statement) {
        try (ResultSet rs = statement.executeQuery()) {
            if (rs.next()) {
                User user = getUser(rs);
                logger.debug("Success getUser ");
                return Optional.of(user);
            }
        } catch (SQLException e) {
            logger.error("Failed getUser", e);
            throw new RuntimeException("Failed getUser", e);
        }
        return Optional.empty();
    }

    private User getUser(ResultSet rs) throws SQLException {
        User user = new User();
        user.setId(rs.getLong("id"));
        user.setFirstName(rs.getString("first_name"));
        user.setLastName(rs.getString("last_name"));
        user.setPassword(rs.getString("password"));
        user.setEmail(rs.getString("email"));
        user.setUserRole(UserRole.valueOf(rs.getString("user_role")));
        user.setRegDate(rs.getTimestamp("reg_date").toInstant());
        logger.debug("Success getUser ");
        return user;
    }
}
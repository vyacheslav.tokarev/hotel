<%@ include file="header.jsp" %>
<%--@elvariable id="users" type="java.util.List<org.vtokarev.domain.User>"--%>
<%--@elvariable id="zoneOffset" type="java.lang.String"--%>
<br>
<div class="row">
    <div class="col s2">
        <a href="modifyUser?action=create&back=userList" class="btn waves-effect waves-light cyan darken-3"><fmt:message
                key="create.action"/></a>
    </div>
    <div class="col s12">
        <div class="container-fluid">
            <div class="row">
                <table class="highlight centered">
                    <thead>
                    <tr>
                        <th><fmt:message key="firstName.label"/></th>
                        <th><fmt:message key="lastName.label"/></th>
                        <th><fmt:message key="email.label"/></th>
                        <th><fmt:message key="role.label"/></th>
                        <th><fmt:message key="regDate.label"/></th>
                        <th><fmt:message key="edit.action"/></th>
                        <th><fmt:message key="delete.action"/></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="item" items="${users}">
                        <tr>
                            <td>${item.firstName}</td>
                            <td>${item.lastName}</td>
                            <td>${item.email}</td>
                            <td><fmt:message key="${item.userRole}"/></td>
                            <td><ct:formatLocalDateTime value="${item.regDate}" pattern="yyyy-MM-dd HH:mm"
                                                        zoneOffset="${zoneOffset}"/></td>
                            <td>
                                <a href="modifyUser?userId=${item.id}&action=edit&back=userList"
                                   class="waves-effect waves-light btn cyan darken-3"><i class="material-icons center">edit</i></a>
                            </td>
                            <td>
                                <a class="waves-effect waves-light btn modal-trigger red" href="#modal${item.id}"><i
                                        class="material-icons center">delete</i></a>
                                <div id="modal${item.id}" class="modal">
                                    <form action="modifyUser" method="post">
                                        <input type="hidden" name="userId" value="${item.id}">
                                        <input type="hidden" name="action" value="delete">
                                        <input type="hidden" name="back" value="userList">
                                        <div class="modal-content">
                                            <h5><fmt:message key="delete.action"/> ?</h5>
                                            <div class="container">
                                                <p class="left-align">
                                                    <br>
                                                    <fmt:message key="firstName.label"/>: <b>${item.firstName}</b> <br>
                                                    <fmt:message key="lastName.label"/>: <b>${item.lastName}</b><br>
                                                    <fmt:message key="email.label"/>: <b>${item.email}</b><br>
                                                    <fmt:message key="role.label"/>: <b><fmt:message
                                                        key="${item.userRole}"/></b><br>
                                                    <fmt:message key="email.label"/>: <b>${item.email}</b><br>
                                                    <fmt:message key="regDate.label"/>: <b><ct:formatLocalDateTime
                                                        value="${item.regDate}" pattern="yyyy-MM-dd HH:mm"
                                                        zoneOffset="${zoneOffset}"/></b><br>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn-small waves-effect waves-light red" type="submit"
                                                    name="edit" value="true"><fmt:message key="delete.action"/><i
                                                    class="material-icons right">delete</i>
                                            </button>
                                            <a href="#"
                                               class="modal-close waves-effect waves-light btn-small light-blue darken-4"><fmt:message
                                                    key="cancel.action"/><i class="material-icons right">close</i></a>
                                        </div>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('.modal');
        var instances = M.Modal.init(elems);
    });
</script>
<%@ include file="footer.jsp" %>
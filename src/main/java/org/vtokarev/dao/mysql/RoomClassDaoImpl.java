package org.vtokarev.dao.mysql;

import org.vtokarev.dao.BookingDao;
import org.vtokarev.dao.RoomClassDao;
import org.vtokarev.domain.RoomClass;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * {@code RoomClassDaoImpl} class is an implementation of {@link RoomClassDao} interface for MySQL persistence layer backend.
 *
 * @author Vyacheslav Tokarev
 */

public class RoomClassDaoImpl extends BaseDaoImpl implements RoomClassDao {

    final private static String GET_ROOMCLASS_BY_ID = "SELECT * FROM room_classes WHERE id = ?";
    final private static String GET_ALL_ROOMCLASSES = "SELECT * FROM room_classes";

    public RoomClassDaoImpl(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    public Optional<RoomClass> get(long id) {
        Connection connection = getConnection();
        PreparedStatement statement = null;
        ResultSet rs = null;
        RoomClass roomClass = null;
        try {
            statement = connection.prepareStatement(GET_ROOMCLASS_BY_ID);
            statement.setLong(1, id);
            rs = statement.executeQuery();
            if (rs.next()) {
                roomClass = new RoomClass();
                roomClass.setId(rs.getLong("id"));
                roomClass.setName(rs.getString("name"));
                logger.debug("Success get");
            }
        } catch (SQLException e) {
            logger.error("Failed get", e);
            throw new RuntimeException("Failed get", e);
        } finally {
            close(rs);
            close(statement);
            close(connection);
        }
        return Optional.ofNullable(roomClass);
    }

    @Override
    public List<RoomClass> getAll() {
        Connection connection = getConnection();
        Statement statement = null;
        ResultSet rs = null;
        List<RoomClass> roomClasses = new ArrayList<>();
        try {
            statement = connection.createStatement();
            rs = statement.executeQuery(GET_ALL_ROOMCLASSES);
            while (rs.next()) {
                RoomClass roomClass = new RoomClass();
                roomClass.setId(rs.getLong("id"));
                roomClass.setName(rs.getString("name"));
                roomClasses.add(roomClass);
            }
        } catch (SQLException e) {
            logger.error("Failed getAll", e);
            throw new RuntimeException("Failed getAll", e);
        } finally {
            close(rs);
            close(statement);
            close(connection);
        }
        logger.debug("Success getAll");
        return roomClasses;
    }

    @Override
    public void create(RoomClass roomClass) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void update(RoomClass roomClass) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(RoomClass roomClass) {
        throw new UnsupportedOperationException();
    }
}

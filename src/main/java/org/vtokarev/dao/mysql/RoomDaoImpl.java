package org.vtokarev.dao.mysql;

import org.vtokarev.dao.RoomDao;
import org.vtokarev.domain.Room;
import org.vtokarev.domain.RoomClass;
import org.vtokarev.dto.RoomEditDto;
import org.vtokarev.dto.RoomSearchDto;
import org.vtokarev.tuples.Pair;

import javax.sql.DataSource;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * {@code RoomDaoImpl} class is an implementation of {@link RoomDao} interface for MySQL persistence layer backend.
 *
 * @author Vyacheslav Tokarev
 */

public class RoomDaoImpl extends BaseDaoImpl implements RoomDao {

    public static final String INSERT = """
            INSERT INTO rooms
            (name, number, capacity, room_class_id, description, price, available)
            VALUES
            (?, ?, ?, ?, ?, ?, ?)""";

    public static final String UPDATE_BY_ID = """
            UPDATE rooms SET
            name = ?, number = ?, capacity = ?, room_class_id = ?, description = ?, price = ?, available = ?
            WHERE id = ?""";

    public static final String GET_ALL_ROOMS_EDIT_DTO = """
            SELECT r.*, rc.name as roomClassName
            FROM rooms r
            	INNER JOIN room_classes rc
            	ON rc.id = r.room_class_id
            ORDER BY r.number""";

    public static final String DELETE_BY_ID = "DELETE FROM rooms WHERE id = ?";

    private static final String GET_ROOM_BY_ID = """
            SELECT *
            FROM rooms
            WHERE id = ?""";

    private static final String GET_ROOM_BY_NUMBER = """
            SELECT *
            FROM rooms
            WHERE number = ?""";

    private final static String GET_MAX_CAPACITY = """
            SELECT MAX(capacity) as capacity
            FROM rooms
            WHERE rooms.available = true""";

    private static final String GET_BOOKING_ROOMS = """
            SELECT r.*, rc.name as room_class_name, b.room_id as booked
            FROM rooms r

            	LEFT OUTER JOIN
            		(select DISTINCT bookings.room_id
            		FROM bookings
            		WHERE (bookings.status != 'CANCELLED') AND
            			(? > bookings.checkin AND ? < bookings.checkout OR
            			? > bookings.checkin AND ? < bookings.checkout OR
            			? <= bookings.checkin AND ? >= bookings.checkout)) as b
            	ON r.id = b.room_id
               
                INNER JOIN room_classes rc
                ON (r.room_class_id = rc.id)
               
            WHERE r.available""";

    private static final String GET_BOOKING_ROOMS_COUNT = """
            SELECT COUNT(r.id) as count
            FROM rooms r

            	LEFT OUTER JOIN
            		(select DISTINCT bookings.room_id
            		FROM bookings
            		WHERE (bookings.status != 'CANCELLED') AND
            			(? > bookings.checkin AND ? < bookings.checkout OR
            			? > bookings.checkin AND ? < bookings.checkout OR
            			? <= bookings.checkin AND ? >= bookings.checkout)) as b
            	ON r.id = b.room_id
               
                INNER JOIN room_classes rc
                ON (r.room_class_id = rc.id)
               
            WHERE r.available""";


    private final String imagesRealPath;

    public RoomDaoImpl(DataSource dataSource, String imagesRealPath) {
        super(dataSource);
        this.imagesRealPath = imagesRealPath;
    }

    @Override
    public Optional<Room> get(long id) {
        Connection connection = getConnection();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.prepareStatement(GET_ROOM_BY_ID);
            statement.setLong(1, id);
            resultSet = statement.executeQuery();

            if (resultSet.next()) {
                Room room = getRoom(resultSet);
                return Optional.of(room);
            }

        } catch (SQLException e) {
            logger.error("Failed get", e);
            throw new RuntimeException("Failed get", e);

        } finally {
            close(resultSet);
            close(statement);
            close(connection);
        }
        logger.debug("Success get");
        return Optional.empty();
    }

    public Optional<Room> getByNumber(String number) {
        Connection connection = getConnection();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.prepareStatement(GET_ROOM_BY_NUMBER);
            statement.setString(1, number);
            resultSet = statement.executeQuery();

            if (resultSet.next()) {
                Room room = getRoom(resultSet);
                return Optional.of(room);
            }
        } catch (SQLException e) {
            logger.error("Failed getByNumber", e);
            throw new RuntimeException("Failed getByNumber", e);
        } finally {
            close(resultSet);
            close(statement);
            close(connection);
        }
        logger.debug("Success getByNumber");
        return Optional.empty();
    }

    @Override
    public List<Room> getAll() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void create(Room room) {
        Connection connection = getConnection();
        PreparedStatement stmt = null;
        ResultSet generatedKeys = null;

        try {
            stmt = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
            fillStatementParameters(room, stmt);
            stmt.executeUpdate();

            generatedKeys = stmt.getGeneratedKeys();
            if (generatedKeys.next()) {
                room.setId(generatedKeys.getLong(1));
            } else {
                logger.error("Creating room failed, no ID obtained");
                throw new SQLException("Creating room failed, no ID obtained");
            }
            logger.debug("Success create");
        } catch (SQLException e) {
            logger.error("Failed create", e);
            throw new RuntimeException("Failed create", e);
        } finally {
            close(generatedKeys);
            close(stmt);
            close(connection);
        }
    }

    private void fillStatementParameters(Room room, PreparedStatement stmt) throws SQLException {
        int k = 1;
        stmt.setString(k++, room.getName());
        stmt.setString(k++, room.getNumber());
        stmt.setInt(k++, room.getCapacity());
        stmt.setLong(k++, room.getRoomClassId());
        stmt.setString(k++, room.getDescription());
        stmt.setDouble(k++, room.getPrice());
        stmt.setBoolean(k, room.isAvailable());
    }

    @Override
    public void update(Room room) {
        Connection connection = getConnection();
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(UPDATE_BY_ID);
            fillStatementParameters(room, stmt);
            stmt.setLong(8, room.getId());
            stmt.executeUpdate();
            logger.debug("Success update");
        } catch (SQLException e) {
            logger.error("Failed update", e);
            throw new RuntimeException("Failed update", e);
        } finally {
            close(stmt);
            close(connection);
        }
    }

    @Override
    public void delete(Room room) {
        Connection connection = getConnection();
        PreparedStatement stmt = null;
        try {
            stmt = connection.prepareStatement(DELETE_BY_ID);
            stmt.setLong(1, room.getId());
            stmt.executeUpdate();
        } catch (SQLException e) {
            logger.error("Failed delete", e);
            throw new RuntimeException("Failed delete", e);
        } finally {
            close(stmt);
            close(connection);
        }

        deleteImageDirectory(room);
        logger.debug("Success delete");
    }

    @Override
    public int getMaxCapacity() {
        Connection connection = getConnection();
        Statement statement = null;
        ResultSet rs = null;
        int maxCapacity = 0;
        try {
            statement = connection.createStatement();
            rs = statement.executeQuery(GET_MAX_CAPACITY);
            if (rs.next()) {
                maxCapacity = rs.getInt("capacity");
            }
        } catch (SQLException e) {
            logger.error("Failed getMaxCapacity", e);
            throw new RuntimeException("Failed getMaxCapacity", e);
        } finally {
            close(rs);
            close(statement);
            close(connection);
        }
        logger.debug("Success getMaxCapacity");
        return maxCapacity;
    }

    public List<RoomSearchDto> getBookingRooms(LocalDate checkin,
                                               LocalDate checkout,
                                               boolean showOnlyAvailable,
                                               Long roomClassId,
                                               Integer personsNumber,
                                               String orderBy,
                                               Integer limit,
                                               Integer offset) {
        String sqlStr = GET_BOOKING_ROOMS;

        if (showOnlyAvailable) {
            sqlStr += " AND (b.room_id is NULL) ";
        }

        if (roomClassId != null) {
            sqlStr += " AND (r.room_class_id = ?) ";
        }

        if (personsNumber != null) {
            sqlStr += " AND (r.capacity >= ?) ";
        }

        sqlStr += " ORDER BY " + orderBy;

        if (limit != null) {
            sqlStr += " LIMIT ? ";
        }

        if (offset != null) {
            sqlStr += " OFFSET ? ";
        }


        List<RoomSearchDto> rooms;
        Connection connection = getConnection();
        PreparedStatement statement = null;
        ResultSet rs = null;
        try {
            int k = 1;
            statement = connection.prepareStatement(sqlStr);
            statement.setDate(k++, Date.valueOf(checkin));
            statement.setDate(k++, Date.valueOf(checkin));
            statement.setDate(k++, Date.valueOf(checkout));
            statement.setDate(k++, Date.valueOf(checkout));
            statement.setDate(k++, Date.valueOf(checkin));
            statement.setDate(k++, Date.valueOf(checkout));

            if (roomClassId != null) {
                statement.setLong(k++, roomClassId);
            }

            if (personsNumber != null) {
                statement.setInt(k++, personsNumber);
            }

            if (limit != null) {
                statement.setInt(k++, limit);
            }

            if (offset != null) {
                statement.setInt(k, offset);
            }

            rs = statement.executeQuery();

            rooms = new ArrayList<>();
            while (rs.next()) {
                RoomSearchDto roomSearchDto = new RoomSearchDto();
                Room room = getRoom(rs);
                roomSearchDto.setRoom(room);
                roomSearchDto.setRoomClassName(rs.getString("room_class_name"));
                rs.getLong("booked");
                if (rs.wasNull()) {
                    roomSearchDto.setStatus("available.label");
                } else {
                    roomSearchDto.setStatus("notAvailable.label");
                }
                roomSearchDto.setImages(getImagePaths(room).stream().map(Pair::getA).collect(Collectors.toList()));
                rooms.add(roomSearchDto);
            }
        } catch (SQLException e) {
            logger.error("Failed getBookingRooms", e);
            throw new RuntimeException("Failed getBookingRooms", e);
        } finally {
            close(rs);
            close(statement);
            close(connection);
        }
        logger.debug("Success getBookingRooms");
        return rooms;
    }

    @Override
    public int getBookingRoomsCount(LocalDate checkin, LocalDate checkout, boolean showOnlyAvailable, Long roomClassId, Integer personsNumber) {

        String sqlStr = GET_BOOKING_ROOMS_COUNT;

        if (showOnlyAvailable) {
            sqlStr += " AND (b.room_id is NULL) ";
        }

        if (roomClassId != null) {
            sqlStr += " AND (r.room_class_id = ?) ";
        }

        if (personsNumber != null) {
            sqlStr += " AND (r.capacity >= ?) ";
        }

        Connection connection = getConnection();
        PreparedStatement statement = null;
        ResultSet rs = null;
        int count = 0;

        try {
            int k = 1;
            statement = connection.prepareStatement(sqlStr);
            statement.setDate(k++, Date.valueOf(checkin));
            statement.setDate(k++, Date.valueOf(checkin));
            statement.setDate(k++, Date.valueOf(checkout));
            statement.setDate(k++, Date.valueOf(checkout));
            statement.setDate(k++, Date.valueOf(checkin));
            statement.setDate(k++, Date.valueOf(checkout));

            if (roomClassId != null) {
                statement.setLong(k++, roomClassId);
            }

            if (personsNumber != null) {
                statement.setInt(k, personsNumber);
            }

            rs = statement.executeQuery();

            if (rs.next()) {
                count = rs.getInt("count");
            }
        } catch (SQLException e) {
            logger.error("Failed getBookingRoomsCount", e);
            throw new RuntimeException("Failed getBookingRoomsCount", e);
        } finally {
            close(rs);
            close(statement);
            close(connection);
        }
        logger.debug("Success getBookingRoomsCount");
        return count;
    }

    @Override
    public List<RoomEditDto> getAllRoomsEditDto() {
        Connection connection = getConnection();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        List<RoomEditDto> list = new ArrayList<>();
        try {
            stmt = connection.prepareStatement(GET_ALL_ROOMS_EDIT_DTO);
            rs = stmt.executeQuery();
            while (rs.next()) {
                Room room = getRoom(rs);
                RoomClass roomClass = new RoomClass();
                roomClass.setId(rs.getLong("room_class_id"));
                roomClass.setName(rs.getString("roomClassName"));
                RoomEditDto roomEditDto = new RoomEditDto(room, roomClass, getImagePaths(room).stream().map(Pair::getA).collect(Collectors.toList()));
                list.add(roomEditDto);
            }
        } catch (SQLException e) {
            logger.error("Failed getAllRoomsEditDto", e);
            throw new RuntimeException("Failed getAllRoomsEditDto", e);
        } finally {
            close(rs);
            close(stmt);
            close(connection);
        }
        logger.debug("Success getAllRoomsEditDto");
        return list;
    }

    private Room getRoom(ResultSet rs) throws SQLException {
        Room room = new Room();
        room.setId(rs.getLong("id"));
        room.setName(rs.getString("name"));
        room.setNumber(rs.getString("number"));
        room.setCapacity(rs.getInt("capacity"));
        room.setRoomClassId(rs.getLong("room_class_id"));
        room.setDescription(rs.getString("description"));
        room.setPrice(rs.getDouble("price"));
        room.setAvailable(rs.getBoolean("available"));
        return room;
    }

    public List<Pair<String, String>> getImagePaths(Room room) {

        List<Pair<String, String>> images = new ArrayList<>();
        Path path = Path.of(imagesRealPath, String.valueOf(room.getId()));

        if (Files.exists(path)) {
            try (Stream<Path> walk = Files.walk(path)) {
                images = walk
                        .filter(Files::isRegularFile)
                        .map(file ->
                                new Pair<>("img/rooms/" + room.getId() + File.separator + file.getFileName().toString(),
                                        file.toAbsolutePath().toString()))
                        .collect(Collectors.toList());
            } catch (IOException e) {
                logger.error("Failed getImages", e);
            }
        }
        logger.debug(images);
        return images;
    }


    public String getImageRealPathDirectory(Room room) {
        return Path.of(imagesRealPath, String.valueOf(room.getId())).toString();
    }

    @Override
    public void deleteImageDirectory(Room room) {
        Path path = Path.of(imagesRealPath, String.valueOf(room.getId()));

        if(!exist(path))
            return;

        try (Stream<Path> walk = Files.walk(path)) {
            walk
                    .sorted(Comparator.reverseOrder())
                    .forEach(this::deleteDirectoryFile);
            logger.debug("Success deleteImageDirectory");
        } catch (IOException e) {
            logger.error("Unable to delete the path: {}", path, e);
        }
    }

    private void deleteDirectoryFile(Path path) {
        try {
            Files.delete(path);
        } catch (IOException e) {
            logger.error("Unable to delete the path: {}", path, e);
        }
    }

    private boolean exist(Path path){
        try {
            return Files.exists(path);
        } catch (SecurityException e){
            logger.error("Unable to delete the path: {}", path, e);
            return false;
        }
    }


}
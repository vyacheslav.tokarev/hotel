<%@ include file="header.jsp" %>
<%--@elvariable id="bookings" type="java.util.List<org.vtokarev.dto.BookingDto>"--%>
<%--@elvariable id="zoneOffset" type="java.lang.String"--%>
<br>
<div class="row">
    <div class="col s2">
        <a href="searchRoom" class="btn waves-effect waves-light cyan darken-3"><fmt:message key="book.action"/></a>
    </div>
    <div class="col s12">
        <div class="container-fluid">
            <div class="row">
                <table class="highlight centered">
                    <thead>
                    <tr>
                        <th><fmt:message key="checkIn.label"/></th>
                        <th><fmt:message key="checkOut.label"/></th>
                        <th><fmt:message key="room.booking"/></th>
                        <th><fmt:message key="number.booking"/></th>
                        <th><fmt:message key="maxPersons.label"/></th>
                        <th><fmt:message key="roomClass.label"/></th>
                        <th><fmt:message key="description.label"/></th>
                        <th><fmt:message key="price.label"/></th>
                        <th><fmt:message key="status.label"/></th>
                        <th><fmt:message key="statusUpdated.label"/></th>
                        <th><fmt:message key="timeLeftToPay.booking"/><br>(HH:MM:SS)</th>
                        <th><fmt:message key="bookingNumber.label"/></th>
                        <th><fmt:message key="action.bookingList"/></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="item" items="${bookings}">
                        <tr>
                            <td>${item.booking.checkin}</td>
                            <td>${item.booking.checkout}</td>
                            <td>${item.roomName}</td>
                            <td>${item.roomNumber}</td>
                            <td>${item.capacity}</td>
                            <td>${item.roomClassName}</td>
                            <td>${item.description}</td>
                            <td>${item.booking.price}</td>
                            <td><fmt:message key="${item.booking.status}"/></td>
                            <td><ct:formatLocalDateTime value="${item.booking.statusChanged}" pattern="yyyy-MM-dd HH:mm"
                                                        zoneOffset="${zoneOffset}"/></td>
                            <td>${item.timeLeft}</td>
                            <td><a href="booking?id=${item.booking.id}&back=bookingList">BK-${item.booking.id}</a></td>
                            <td>
                                <c:if test="${not empty item.timeLeft}">

                                    <a class="waves-effect waves-light btn-small modal-trigger cyan darken-3"
                                       href="#modalPay${item.booking.id}"><i
                                            class="material-icons right">done</i><fmt:message key="pay.action"/></a>
                                    <div id="modalPay${item.booking.id}" class="modal">
                                        <form action="proceedBooking" method="post">
                                            <input type="hidden" name="bookingId" value="${item.booking.id}">
                                            <input type="hidden" name="pay" value="true">
                                            <div class="modal-content">
                                                <h5><fmt:message key="payBooking.message"/> ?</h5>
                                                <div class="container">
                                                    <p class="left-align">
                                                        <br>
                                                        <fmt:message key="bookingNumber.label"/>:
                                                        <b>BK-${item.booking.id}</b> <br>
                                                        <fmt:message key="checkIn.label"/>:
                                                        <b>${item.booking.checkin}</b> <br>
                                                        <fmt:message key="checkOut.label"/>:
                                                        <b>${item.booking.checkout}</b><br>
                                                        <fmt:message key="room.booking"/>: <b>${item.roomName}</b><br>
                                                        <fmt:message key="number.booking"/>:
                                                        <b>${item.roomNumber}</b><br>
                                                        <fmt:message key="maxPersons.label"/>:
                                                        <b>${item.capacity}</b><br>
                                                        <fmt:message key="roomClass.label"/>:
                                                        <b>${item.roomClassName}</b><br>
                                                        <fmt:message key="description.label"/>:
                                                        <b>${item.description}</b><br>
                                                        <fmt:message key="price.label"/>:
                                                        <b>${item.booking.price}</b><br>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn-small waves-effect waves-light cyan darken-3"
                                                        type="submit" name="cancel" value="true"><fmt:message
                                                        key="yes.action"/><i
                                                        class="material-icons right">done</i>
                                                </button>
                                                <a href="#"
                                                   class="modal-close waves-effect waves-light btn-small light-blue darken-4"><fmt:message
                                                        key="close.action"/><i
                                                        class="material-icons right">close</i></a>
                                            </div>
                                        </form>
                                    </div>
                                    <a class="waves-effect waves-light btn-small modal-trigger red"
                                       href="#modalCancel${item.booking.id}"><i
                                            class="material-icons right">close</i><fmt:message key="cancel.action"/></a>
                                    <div id="modalCancel${item.booking.id}" class="modal">
                                        <form action="proceedBooking" method="post">
                                            <input type="hidden" name="bookingId" value="${item.booking.id}">
                                            <input type="hidden" name="cancel" value="true">
                                            <div class="modal-content">
                                                <h5><fmt:message key="cancelBooking.message"/> ?</h5>
                                                <div class="container">
                                                    <p class="left-align">
                                                        <br>
                                                        <fmt:message key="bookingNumber.label"/>:
                                                        <b>BK-${item.booking.id}</b> <br>
                                                        <fmt:message key="checkIn.label"/>:
                                                        <b>${item.booking.checkin}</b> <br>
                                                        <fmt:message key="checkOut.label"/>:
                                                        <b>${item.booking.checkout}</b><br>
                                                        <fmt:message key="room.booking"/>: <b>${item.roomName}</b><br>
                                                        <fmt:message key="number.booking"/>:
                                                        <b>${item.roomNumber}</b><br>
                                                        <fmt:message key="maxPersons.label"/>:
                                                        <b>${item.capacity}</b><br>
                                                        <fmt:message key="roomClass.label"/>:
                                                        <b>${item.roomClassName}</b><br>
                                                        <fmt:message key="description.label"/>:
                                                        <b>${item.description}</b><br>
                                                        <fmt:message key="price.label"/>:
                                                        <b>${item.booking.price}</b><br>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn-small waves-effect waves-light red"
                                                        type="submit" name="cancel" value="true"><fmt:message
                                                        key="yes.action"/><i
                                                        class="material-icons right">done</i>
                                                </button>
                                                <a href="#"
                                                   class="modal-close waves-effect waves-light btn-small light-blue darken-4"><fmt:message
                                                        key="close.action"/><i
                                                        class="material-icons right">close</i></a>
                                            </div>
                                        </form>
                                    </div>
                                </c:if>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('.modal');
        var instances = M.Modal.init(elems);
    });
</script>
<%@ include file="footer.jsp" %>
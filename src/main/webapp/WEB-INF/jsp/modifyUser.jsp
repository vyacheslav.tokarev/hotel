<%@ include file="header.jsp" %>
<%@ page import="org.vtokarev.domain.UserRole" %>
<%--@elvariable id="id" type="int"--%>
<%--@elvariable id="user" type="org.vtokarev.domain.User"--%>
<%--@elvariable id="back" type="java.lang.String"--%>
<%--@elvariable id="action" type="java.lang.String"--%>
<%--@elvariable id="passwordPattern" type="java.lang.String"--%>
<%--@elvariable id="firstName" type="java.lang.String"--%>
<%--@elvariable id="lastName" type="java.lang.String"--%>
<%--@elvariable id="email" type="java.lang.String"--%>
<%--@elvariable id="errorMessage" type="java.lang.String"--%>
<%--@elvariable id="userRole" type="java.lang.String"--%>
<br><br><br><br>
<div class="row">
    <div class="col s6 offset-s3 z-depth-1">
        <div class="container">
            <form action="modifyUser" method="post">
                <input type="hidden" name="action" value="${action}">
                <input type="hidden" name="userId" value="${id}">
                <c:if test="${user.userRole == UserRole.ADMIN && action == 'create'}">
                    <div class="input-field">
                        <select id="userRole" name="userRole" required>
                            <option value="ADMIN"
                                    <c:if test="${userRole == 'ADMIN'}">selected</c:if>><fmt:message
                                    key="ADMIN"/></option>
                            <option value="MANAGER"
                                    <c:if test="${userRole == 'MANAGER'}">selected</c:if>><fmt:message
                                    key="MANAGER"/></option>
                            <option value="GUEST"
                                    <c:if test="${userRole == 'GUEST'}">selected</c:if>><fmt:message
                                    key="GUEST"/></option>
                        </select>
                        <label for="userRole"><fmt:message key="role.label"/></label>
                    </div>
                </c:if>
                <c:if test="${user.userRole == UserRole.ADMIN && action == 'edit'}">
                    <div class="input-field">
                        <fmt:message key="role.label"/>: <b><fmt:message key="${userRole}"/></b>
                    </div>
                    <input type="hidden" name="userRole" value="${userRole}">
                </c:if>
                <div class="input-field">
                    <input id="firstName" value="${firstName}" name="firstName" type="text"
                           required="required"
                           class="validate">
                    <label for="firstName"><fmt:message key="firstName.label"/></label>
                </div>
                <div class="input-field">
                    <input id="lastName" name="lastName" value="${lastName}" type="text" required="required"
                           class="validate">
                    <label for="lastName"><fmt:message key="lastName.label"/></label>
                </div>
                <div class="input-field">
                    <input id="email" name="email" value="${email}" type="email" required="required"
                           class="validate">
                    <label for="email"><fmt:message key="email.label"/></label>
                    <span class="helper-text" data-error="<fmt:message key="error.label" />"
                          data-success="<fmt:message key="success.label" />"></span>
                </div>
                <div class="input-field">
                    <input id="password" name="password" type="password" <c:if test="${action=='create'}">required</c:if>
                           pattern="${passwordPattern}" class="validate">
                    <label for="password"><fmt:message key="password.label"/></label>
                    <span class="helper-text"
                          data-error="<fmt:message key="dataError.password.login" />"
                          data-success="<fmt:message key="success.label" />"><fmt:message
                            key="dataError.password.login"/></span>
                </div>
                <c:if test="${user.userRole != UserRole.ADMIN}">
                    <input type="hidden" name="userRole" value="GUEST">
                </c:if>
                <c:if test="${action == 'edit'}"><c:set var="actionLable" value="save.action"/></c:if>
                <c:if test="${action == 'create'}"><c:set var="actionLable" value="create.action"/></c:if>
                <button class="btn waves-effect waves-light cyan darken-3" type="submit" name="action"><fmt:message
                        key="${actionLable}"/>
                    <i class="material-icons right">done</i>
                </button>
                <a class="waves-effect waves-light btn red" href="${back}"><i
                        class="material-icons right">close</i><fmt:message key="close.action"/></a>
            </form>
            <blockquote class="red-text">
                <c:if test="${not empty errorMessage}">
                    <fmt:message key="${errorMessage}"/>
                </c:if>
            </blockquote>
        </div>
    </div>
</div>
<script>
    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('select');
        var instances = M.FormSelect.init(elems);
    });
</script>
<%@ include file="footer.jsp" %>
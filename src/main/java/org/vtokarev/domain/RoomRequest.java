package org.vtokarev.domain;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Objects;

/**
 * The {@code RoomRequest} class represents the request made by guests to find the room by parameters.
 * Once the request is created, the manager proceeds with the request and tries to find a suitable room or cancel the request if no rooms are available.
 *
 * @author Vyacheslav Tokarev
 */

public class RoomRequest implements Serializable {
    private long id;
    private LocalDate checkin;
    private LocalDate checkout;
    private int personsNumber;

    private String guestNotes;
    private String managerNotes;

    private long roomClassId;
    private long userId;
    private long bookingId;

    private Instant regDate;

    private RoomRequestStatus status;

    public RoomRequest() {
    }

    public RoomRequest(long id, LocalDate checkin, LocalDate checkout, int personsNumber, String guestNotes, String managerNotes, long roomClassId, long userId, long bookingId, Instant regDate, RoomRequestStatus status) {
        this.id = id;
        this.checkin = checkin;
        this.checkout = checkout;
        this.personsNumber = personsNumber;
        this.guestNotes = guestNotes;
        this.managerNotes = managerNotes;
        this.roomClassId = roomClassId;
        this.userId = userId;
        this.bookingId = bookingId;
        this.regDate = regDate.truncatedTo(ChronoUnit.MICROS);
        this.status = status;
    }

    public RoomRequest(RoomRequest roomRequest) {
        this.id = roomRequest.id;
        this.checkin = roomRequest.checkin;
        this.checkout = roomRequest.checkout;
        this.personsNumber = roomRequest.personsNumber;
        this.guestNotes = roomRequest.guestNotes;
        this.managerNotes = roomRequest.managerNotes;
        this.roomClassId = roomRequest.roomClassId;
        this.userId = roomRequest.userId;
        this.bookingId = roomRequest.bookingId;
        this.regDate = roomRequest.regDate.truncatedTo(ChronoUnit.MICROS);
        this.status = roomRequest.status;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDate getCheckin() {
        return checkin;
    }

    public void setCheckin(LocalDate checkin) {
        this.checkin = checkin;
    }

    public LocalDate getCheckout() {
        return checkout;
    }

    public void setCheckout(LocalDate checkout) {
        this.checkout = checkout;
    }

    public int getPersonsNumber() {
        return personsNumber;
    }

    public void setPersonsNumber(int personsNumber) {
        this.personsNumber = personsNumber;
    }

    public long getRoomClassId() {
        return roomClassId;
    }

    public void setRoomClassId(long roomClassId) {
        this.roomClassId = roomClassId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getBookingId() {
        return bookingId;
    }

    public void setBookingId(long bookingId) {
        this.bookingId = bookingId;
    }

    public Instant getRegDate() {
        return regDate;
    }

    public void setRegDate(Instant regDate) {
        this.regDate = regDate.truncatedTo(ChronoUnit.MICROS);
    }

    public RoomRequestStatus getStatus() {
        return status;
    }

    public String getGuestNotes() {
        return guestNotes;
    }

    public void setGuestNotes(String guestNotes) {
        this.guestNotes = guestNotes;
    }

    public String getManagerNotes() {
        return managerNotes;
    }

    public void setManagerNotes(String managerNotes) {
        this.managerNotes = managerNotes;
    }

    public void setStatus(RoomRequestStatus status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoomRequest that = (RoomRequest) o;
        return getId() == that.getId() && getPersonsNumber() == that.getPersonsNumber() &&
                getRoomClassId() == that.getRoomClassId() && getUserId() == that.getUserId() &&
                getBookingId() == that.getBookingId() && Objects.equals(getCheckin(), that.getCheckin()) &&
                Objects.equals(getCheckout(), that.getCheckout()) && Objects.equals(getGuestNotes(), that.getGuestNotes()) &&
                Objects.equals(getManagerNotes(), that.getManagerNotes()) && Objects.equals(getRegDate(), that.getRegDate()) &&
                getStatus() == that.getStatus();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getCheckin(), getCheckout(), getPersonsNumber(), getGuestNotes(),
                getManagerNotes(), getRoomClassId(), getUserId(), getBookingId(), getRegDate(), getStatus());
    }

    @Override
    public String toString() {
        return "RoomRequest{" +
                "id=" + id +
                ", checkin=" + checkin +
                ", checkout=" + checkout +
                ", personsNumber=" + personsNumber +
                ", guestNotes='" + guestNotes + '\'' +
                ", managerNotes='" + managerNotes + '\'' +
                ", roomClassId=" + roomClassId +
                ", userId=" + userId +
                ", bookingId=" + bookingId +
                ", regDate=" + regDate +
                ", status=" + status +
                '}';
    }
}

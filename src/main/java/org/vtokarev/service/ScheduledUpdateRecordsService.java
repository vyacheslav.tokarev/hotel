package org.vtokarev.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.vtokarev.dao.BookingDao;
import org.vtokarev.dao.RoomRequestDao;

import java.lang.invoke.MethodHandles;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;

/**
 * {@code RoomClassService} class is a service layer class that implements scheduled running methods.
 *
 * @author Vyacheslav Tokarev
 */


public class ScheduledUpdateRecordsService implements Runnable {

    private final static Logger LOGGER = LogManager.getLogger(MethodHandles.lookup().lookupClass());

    BookingDao bookingDao;
    RoomRequestDao roomRequestDao;
    ZoneId zoneId;

    public ScheduledUpdateRecordsService(BookingDao bookingDao, RoomRequestDao roomRequestDao, ZoneId zoneId) {
        this.bookingDao = bookingDao;
        this.roomRequestDao = roomRequestDao;
        this.zoneId = zoneId;
    }

    @Override
    public void run() {
        Instant nowInstant = Instant.now();
        Instant updateDate = nowInstant.minus(2, ChronoUnit.DAYS);
        LocalDate nowLocalDate = LocalDate.ofInstant(nowInstant, zoneId);

        int rowAffectedBooking = bookingDao.updateBreachedReservedBookingStatus(updateDate, nowLocalDate);
        LOGGER.debug("Successfully updated {} booking statuses", rowAffectedBooking);

        int rowAffectedRoomRequest = roomRequestDao.updateBreachedNewRoomRequestStatus(nowLocalDate);
        LOGGER.debug("Successfully updated {} roomRequest statuses", rowAffectedRoomRequest);
    }
}

package org.vtokarev.dao;

import org.vtokarev.domain.User;

import java.util.List;
import java.util.Optional;

/**
 * {@code GenericDao} interface is the base interface for other specific DAO interfaces which are implemented by DAO classes.
 * It defines the base operation set with the persistence layer.
 *
 * @author Vyacheslav Tokarev
 */

public interface GenericDao<T> {
    Optional<T> get(long id);
    List<T> getAll();
    void create(T t);
    void update(T t);
    void delete(T t);
}

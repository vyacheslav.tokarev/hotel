<%@ include file="header.jsp" %>
<%--@elvariable id="zoneOffset" type="java.lang.String"--%>
<%--@elvariable id="booking" type="org.vtokarev.dto.BookingDto"--%>
<%--@elvariable id="images" type="java.util.List<java.lang.String>"--%>
<%--@elvariable id="user" type="org.vtokarev.domain.User"--%>
<br>
<div class="row">
    <div class="col s4 offset-s4 z-depth-1">
        <div class="container">
            <div class="col s12 center"><p><fmt:message key="booking.label"/>: <b>BK-${booking.booking.id}</b></p></div>
            <div class="col s12"><fmt:message key="checkIn.label"/>: ${booking.booking.checkin}</div>
            <div class="col s12"><fmt:message key="checkOut.label"/>: ${booking.booking.checkout}</div>
            <div class="col s12"><fmt:message key="room.booking"/>: ${booking.roomName}</div>
            <div class="col s12"><fmt:message key="number.booking"/>: <b>${booking.roomNumber}</b></div>
            <div class="col s12"><fmt:message key="maxPersons.label"/>: <b>${booking.capacity}</b></div>
            <div class="col s12"><fmt:message key="roomClass.label"/>: ${booking.roomClassName}</div>
            <div class="col s12"><fmt:message key="description.label"/>: ${booking.description}</div>
            <div class="col s12"><fmt:message key="price.label"/>: ${booking.booking.price}</div>
            <div class="col s12"><fmt:message key="status.label"/>: <fmt:message key="${booking.booking.status}"/></div>
            <div class="col s12"><fmt:message
                    key="statusUpdated.label"/>: <ct:formatLocalDateTime value="${booking.booking.statusChanged}"
                                                                         pattern="yyyy-MM-dd HH:mm"
                                                                         zoneOffset="${zoneOffset}"/></div>
            <br>
            <c:if test="${not empty booking.timeLeft}">
                <div class="col s12"><p><fmt:message key="timeLeftToPay.booking"/>(HH:MM:SS): <b>${booking.timeLeft}</b>
                </p></div>
            </c:if>
            <div class="col s12">
            </div>
            <div class="row">
                <div class="s12">
                    <c:forEach items="${images}" var="img">
                        <div class="col s3 my-img">
                            <img class="materialboxed" width="80" src="${img}" alt="">
                        </div>
                    </c:forEach>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col s12 left">
                    <c:if test="${not empty booking.timeLeft && user.userRole == 'GUEST'}">
                        <form action="proceedBooking" id="proceedBooking" method="post">
                            <input type="hidden" name="back" value="<c:out value="${param.back}">home</c:out>">
                            <button class="btn-small waves-effect waves-light cyan darken-3" type="submit"
                                    name="pay" value="true"><fmt:message key="pay.action"/><i
                                    class="material-icons right">done</i>
                            </button>
                            <input type="hidden" name="bookingId" value="${booking.booking.id}">
                        </form>
                    </c:if>
                </div>
                <div class="col s12">
                </div>
                <div class="col s6 left">
                    <c:if test="${not empty booking.timeLeft && user.userRole == 'GUEST'}">
                        <button class="btn-small waves-effect waves-light red" form="proceedBooking" type="submit"
                                name="cancel" value="true"><fmt:message key="cancel.action"/><i
                                class="material-icons right">close</i>
                        </button>
                    </c:if>
                </div>
                <div class="col s6 right">
                    <a class="btn-small waves-effect waves-light light-blue darken-4 right"
                       href="<c:out value="${param.back}">home</c:out>"><i
                            class="material-icons right">logout</i><fmt:message key="close.action"/></a>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('.materialboxed');
        var instances = M.Materialbox.init(elems);
    });
</script>
<%@ include file="footer.jsp" %>
package org.vtokarev.controller;

import org.vtokarev.domain.Room;
import org.vtokarev.service.RoomService;
import org.vtokarev.tuples.Pair;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

/**
 * The {@code ImageListServlet} uses for operation with images of the {@link Room}.
 * The class implements {@code doGet} method that processes GET {@code imageList} requests generated by users with ADMIN role.
 * In the {@code doGet} method, it collects the necessary data from the ADMIN request and forward control to {@code imageList.jsp} view for further processing.
 * In the {@code doPost} method, it uses the collected data and implements add or delete photo operations of the {@link Room}.
 *
 * @author Vyacheslav Tokarev
 */

@WebServlet(urlPatterns = "/imageList", name = "imageListServlet")
public class ImageListServlet extends HttpServlet {

    private RoomService roomService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        roomService = (RoomService) config.getServletContext().getAttribute("app/RoomService");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("errorMessage") != null) {
            req.setAttribute("errorMessage", req.getParameter("errorMessage"));
        }

        long roomId;
        try {
            roomId = Long.parseLong(req.getParameter("roomId"));
        } catch (NumberFormatException e) {
            resp.sendRedirect("home");
            return;
        }

        Optional<Room> room = roomService.getRoomById(roomId);

        if (room.isPresent()) {
            String realPathDirectory = roomService.getImageRealPathDirectory(room.get());
            List<Pair<String, String>> images = roomService.getImagePaths(room.get());

            req.setAttribute("room", room.get());
            req.setAttribute("images", images);
            req.setAttribute("realPathDirectory", realPathDirectory);
            req.getRequestDispatcher("/WEB-INF/jsp/imageList.jsp").forward(req, resp);
            return;
        }
        resp.sendRedirect("home");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String realPathFileName = req.getParameter("realPathFileName");
        String back = req.getParameter("back");
        long roomId = Long.parseLong(req.getParameter("roomId"));

        System.out.println(realPathFileName);

        String errorMessage = roomService.deleteImage(realPathFileName);

        String requestStr = back + "?back=roomList&roomId=" + roomId;

        if (!errorMessage.isBlank()) {
            requestStr += ("&errorMessage" + errorMessage);
        }

        resp.sendRedirect(requestStr);
    }
}

package org.vtokarev.dao.mysql;

import org.h2.jdbcx.JdbcDataSource;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Objects;
import java.util.Properties;

public class SqlMemoryDatabase implements BeforeEachCallback {

    private DataSource dataSource;

    private void loadDataSource() throws IOException {
        InputStream input = getClass().getResourceAsStream("/test.config.properties");

        Properties properties = new Properties();
        properties.load(input);

        JdbcDataSource jdbcDataSource = new JdbcDataSource();
        jdbcDataSource.setURL(properties.getProperty("datasource.url"));
        jdbcDataSource.setUser(properties.getProperty("datasource.username"));
        jdbcDataSource.setPassword(properties.getProperty("datasource.password"));

        dataSource = jdbcDataSource;
    }

    private void createSchema() throws URISyntaxException, IOException, SQLException {
        Path schemaPath = Paths.get(Objects.requireNonNull(getClass().getResource("/db_schema.sql")).toURI());
        String createSchemaSql = new String(Files.readAllBytes(schemaPath));

        Connection connection = dataSource.getConnection();
        Statement stmt = connection.createStatement();
        stmt.execute(createSchemaSql);
    }

    public DataSource dataSource() {
        return dataSource;
    }

    @Override
    public void beforeEach(ExtensionContext extensionContext) throws Exception {
        loadDataSource();
        createSchema();
    }
}

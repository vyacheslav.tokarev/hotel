package org.vtokarev.dao.mysql;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.vtokarev.dao.BookingDao;
import org.vtokarev.dao.RoomRequestDao;
import org.vtokarev.domain.Booking;
import org.vtokarev.domain.BookingStatus;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class BookingDaoTests {

    @RegisterExtension
    public static SqlMemoryDatabase sqlMemoryDatabase = new SqlMemoryDatabase();


    @BeforeEach
    void init() throws SQLException {
        String initSql = """
                INSERT INTO users VALUES (default,'Vyacheslav','Tokarev','vyacheslav.tokarev@gmail.com','bijEcgZ4O401p6LWe0U6ea9R9sjVExAUTW1rxTYUFHQ','GUEST','2022-09-11 18:34:15');
                INSERT INTO room_classes VALUES (default, 'Standard'),(default, 'Superior'),(default, 'Studio'),(default, 'Suite');
                 INSERT INTO rooms VALUES
                 (default,'Standard double room','201',2,1,'Standard double room',40.00,1),
                 (default,'Standard triple room','202',3,1,'Standard triple room',55.00,1),
                 (default,'Superior double room','203',2,2,'Superior double room',50.00,1);""";
        Connection conn = sqlMemoryDatabase.dataSource().getConnection();
        Statement stmt = conn.createStatement();
        stmt.executeUpdate(initSql);
        stmt.close();
        conn.close();
    }

    @Test
    void create_get_test() {
        LocalDate checkin = LocalDate.parse("2023-10-15");
        LocalDate checkout = LocalDate.parse("2023-10-25");
        long roomId = 1L;
        long userId = 1L;
        Booking booking1 = new Booking(0, checkin, checkout, roomId, userId, BookingStatus.BOOKED, 55.00, Instant.now());

        RoomRequestDao roomRequestDao = new RoomRequestDaoImpl(sqlMemoryDatabase.dataSource());
        BookingDao bookingDao = new BookingDaoImpl(sqlMemoryDatabase.dataSource(), roomRequestDao);

        bookingDao.create(booking1);
        Optional<Booking> booking2 = bookingDao.get(booking1.getId());
        assertTrue(booking2.isPresent());
        assertEquals(booking1, booking2.get());
    }

    @Test
    void update_test(){
        RoomRequestDao roomRequestDao = new RoomRequestDaoImpl(sqlMemoryDatabase.dataSource());
        BookingDao bookingDao = new BookingDaoImpl(sqlMemoryDatabase.dataSource(), roomRequestDao);

        LocalDate checkin = LocalDate.parse("2023-10-15");
        LocalDate checkout = LocalDate.parse("2023-10-25");
        long roomId = 1L;
        long userId = 1L;
        Booking booking1 = new Booking(0, checkin, checkout, roomId, userId, BookingStatus.BOOKED, 55.00, Instant.now());
        bookingDao.create(booking1);

        booking1.setCheckin(LocalDate.parse("2023-10-16"));
        booking1.setCheckout(LocalDate.parse("2023-10-26"));
        booking1.setStatus(BookingStatus.CANCELLED);
        booking1.setPrice(65.00);
        bookingDao.update(booking1);

        Optional<Booking> booking2 = bookingDao.get(booking1.getId());
        assertTrue(booking2.isPresent());
        assertEquals(booking1, booking2.get());

    }

}

package org.vtokarev.domain;

import java.io.Serializable;
import java.util.Objects;

/**
 * The {@code RoomClass} class represents the entity of the class of the room. One of the characteristics of the room.
 * It uses as a search parameter when a guest leaves the request to find the room.
 *
 * @author Vyacheslav Tokarev
 */

public class RoomClass implements Serializable {
    private long id;
    private String name;

    public RoomClass() {
    }

    public RoomClass(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoomClass roomClass = (RoomClass) o;
        return getId() == roomClass.getId() && Objects.equals(getName(), roomClass.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName());
    }

    @Override
    public String toString() {
        return "RoomClass{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}

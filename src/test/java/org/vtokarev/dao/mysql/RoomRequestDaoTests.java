package org.vtokarev.dao.mysql;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.vtokarev.dao.RoomRequestDao;
import org.vtokarev.dao.UserDao;
import org.vtokarev.domain.RoomRequest;
import org.vtokarev.domain.RoomRequestStatus;
import org.vtokarev.domain.User;
import org.vtokarev.domain.UserRole;
import org.vtokarev.dto.RoomRequestDto;

import javax.sql.DataSource;
import java.lang.invoke.MethodHandles;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.Instant;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

public class RoomRequestDaoTests {
    @RegisterExtension
    public static SqlMemoryDatabase sqlMemoryDatabase = new SqlMemoryDatabase();

    private final static Logger LOGGER = LogManager.getLogger(MethodHandles.lookup().lookupClass());
    private DataSource ds = null;
    private long roomClassId = 0;
    private long userId = 0;


    @BeforeEach
    void init() throws SQLException {
        ds = sqlMemoryDatabase.dataSource();

        Connection conn = ds.getConnection();
        Statement stmt = conn.createStatement();
        stmt.executeUpdate("insert into room_classes (name) values ('Room Class1')", Statement.RETURN_GENERATED_KEYS);
        ResultSet rs = stmt.getGeneratedKeys();
        if (rs.next())
            roomClassId = rs.getInt("id");
        rs.close();
        stmt.close();
        conn.close();
        LOGGER.debug("roomClasId={}", roomClassId);

        UserDao userDao = new UserDaoImpl(ds);
        User user = new User(0,
                "firstName",
                "lastname",
                "email@domain.com",
                "Qwerty12",
                Instant.now(),
                UserRole.GUEST);
        userDao.create(user);
        userId = user.getId();
    }

    @Test
    void create_get_test() {

        RoomRequestDao roomRequestDao = new RoomRequestDaoImpl(ds);
        RoomRequest roomRequest1 = getRoomRequest(1);

        roomRequestDao.create(roomRequest1);
        assertTrue(roomRequest1.getId() > 0);
        Optional<RoomRequest> roomRequest2 = roomRequestDao.get(1);
        assertTrue(roomRequest2.isPresent());
        assertEquals(roomRequest1, roomRequest2.get());
    }

    @Test
    void update_test() {
        LOGGER.debug("update_test()");

        RoomRequestDao roomRequestDao = new RoomRequestDaoImpl(ds);
        RoomRequest roomRequest1 = getRoomRequest(1);

        roomRequestDao.create(roomRequest1);

        roomRequest1.setCheckin(LocalDate.parse("2021-10-06"));
        roomRequest1.setCheckout(LocalDate.parse("2021-10-19"));
        roomRequest1.setManagerNotes("Another manager notes");
        roomRequest1.setGuestNotes("Another guest notes");
        roomRequest1.setRegDate(Instant.now().minus(2, ChronoUnit.HOURS));
        roomRequest1.setStatus(RoomRequestStatus.PROVIDED);

        roomRequestDao.update(roomRequest1);
        Optional<RoomRequest> roomRequest2 = roomRequestDao.get(roomRequest1.getId());
        assertTrue(roomRequest2.isPresent());
        assertEquals(roomRequest1, roomRequest2.get());

    }

    @Test
    void getRequestsByUserId_test() {
        RoomRequest roomRequest1 = getRoomRequest(1);
        RoomRequest roomRequest2 = getRoomRequest(2);

        RoomRequestDao roomRequestDao = new RoomRequestDaoImpl(ds);
        roomRequestDao.create(roomRequest1);
        roomRequestDao.create(roomRequest2);
        assertNotEquals(roomRequest1.getId(), roomRequest2.getId());

        List<RoomRequestDto> list = roomRequestDao.getRequestsByUserId(userId);

        assertEquals(2, list.size());
        long count1 =  list.stream()
                .filter(rr -> rr.getRoomRequest().equals(roomRequest1))
                .count();
        assertEquals(1, count1);

        long count2 =  list.stream()
                .filter(rr -> rr.getRoomRequest().equals(roomRequest2))
                .count();
        assertEquals(1, count2);
    }

    @Test
    void updateBreachedNewRoomRequestStatus_test(){

        RoomRequestDao roomRequestDao = new RoomRequestDaoImpl(ds);
        RoomRequest roomRequest1 = getRoomRequest(1);
        roomRequestDao.create(roomRequest1);
        roomRequestDao.updateBreachedNewRoomRequestStatus(LocalDate.parse("2021-10-06"));
        Optional<RoomRequest> roomRequest2 = roomRequestDao.get(roomRequest1.getId());
        assertTrue(roomRequest2.isPresent());
        assertEquals(RoomRequestStatus.CANCELLED, roomRequest2.get().getStatus());
    }

    private RoomRequest getRoomRequest(int instanceNumber) {
        return switch (instanceNumber) {
            case 1 -> new RoomRequest(
                    0,
                    LocalDate.parse("2021-10-05"),
                    LocalDate.parse("2021-10-20"),
                    2,
                    "some guest notes - 1",
                    "some manager notes - 1",
                    roomClassId,
                    userId,
                    0,
                    Instant.now(),
                    RoomRequestStatus.NEW);
            default -> new RoomRequest(
                    0,
                    LocalDate.parse("2021-10-10"),
                    LocalDate.parse("2021-10-25"),
                    3,
                    "some guest notes - 2",
                    "some manager notes - 2",
                    roomClassId,
                    userId,
                    0,
                    Instant.now().minus(2, ChronoUnit.HOURS),
                    RoomRequestStatus.CANCELLED);
        };
    }
}

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ attribute name="value" required="true" %>

<fmt:setBundle basename="text"/>

<%
    String lang = value.equalsIgnoreCase("en") ? "ru" : "en";

    String queryString = request.getQueryString() == null ? "" : "&" + request.getQueryString()
            .replaceAll("\\?cookieLocale=ru", "")
            .replaceAll("\\?cookieLocale=en", "")
            .replaceAll("&cookieLocale=en", "")
            .replaceAll("&cookieLocale=en", "");

    queryString = "?cookieLocale=" + lang + queryString;
    String fmtKey = "lang."+lang;
%>

<a href="<%=queryString%>"><fmt:message key="<%=fmtKey%>" /></a>
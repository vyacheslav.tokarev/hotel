package org.vtokarev.tuples;

/**
 * It's an implementation for the three-element tuple container.
 *
 * @author Vyacheslav Tokarev
 */


public class Triplet<A, B, C> {
    private A a;
    private B b;

    private C c;

    public Triplet() {
    }

    public Triplet(A a, B b, C c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public A getA() {
        return a;
    }

    public void setA(A a) {
        this.a = a;
    }

    public B getB() {
        return b;
    }

    public void setB(B b) {
        this.b = b;
    }

    public C getC() {
        return c;
    }

    public void setC(C c) {
        this.c = c;
    }

    public void set(A a, B b, C c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

}

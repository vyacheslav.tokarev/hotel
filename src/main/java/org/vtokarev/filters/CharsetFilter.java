package org.vtokarev.filters;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The {@code AuthenticationFilter} is a class that implements {@link Filter} interface.
 * It set {@code "UTF-8"} encoding for HTTP requests and responses.
 * Considering that text data are stored using {@code "UTF-8"} encoding at the backend, it provides support for non-Latin languages, specifically Cyrillic.
 * Also, it set appropriate HTTP headers, that instruct a browser not to use caching.
 *
 * @author Vyacheslav Tokarev
 */


public class CharsetFilter implements Filter {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest)servletRequest;
        HttpServletResponse response = (HttpServletResponse)servletResponse;

        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");

        response.setHeader("Cache-Control", "no-cache, no-store");
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 0);

        chain.doFilter(request, response);
    }
}

package org.vtokarev.domain;

import java.io.Serializable;

/**
 * The {@code UserRole} class represents the entity of {@link User} role.
 * {@code GUEST} represents the guest of the hotel. The users having the role can leave requests, book rooms, cancel, or pay for the booking.
 * {@code MANAGER} searches rooms for the users according to the parameters in {@link RoomRequest}
 * {@code ADMIN} - create, modify and delete users and rooms.
 *
 * @author Vyacheslav Tokarev
 */

public enum UserRole implements Serializable {
    /**
     * The role means the {@link User} is a guest of the hotel.
     */
    GUEST,
    /**
     * The role means the {@link User} is a manager of the hotel.
     */
    MANAGER,

    /**
     * The role means the {@link User} is a member of Admin security group.
     */
    ADMIN,

    /**
     * The role means that the {@link User} is not authenticated.
     */
    UNKNOWN
}
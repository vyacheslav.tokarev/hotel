<%@ include file="header.jsp" %>
<%--@elvariable id="checkin" type="java.time.LocalDate"--%>
<%--@elvariable id="checkout" type="java.time.LocalDate"--%>
<%--@elvariable id="roomClassName" type="java.lang.String"--%>
<%--@elvariable id="room" type="org.vtokarev.domain.Room"--%>
<%--@elvariable id="images" type="java.util.List<String>"--%>
<br>
<div class="row">
  <div class="col s4 offset-s4 z-depth-1">
    <div class="container">
      <form action="bookRoomGuest" method="post">
        <input type="hidden" name="checkin" value="${checkin}">
        <input type="hidden" name = "checkout" value="${checkout}">
        <input type="hidden" name="roomId" value="${room.id}">
        <input type="hidden" name="price" value="${room.price}">
        <h5>${room.name}</h5><br>
        <span><fmt:message key="checkIn.label" /> : <b>${checkin}</b></span><br>
        <span><fmt:message key="checkOut.label" />: <b>${checkout}</b></span><br><br>
        <span><fmt:message key="number.booking" />: <b>${room.number}</b></span><br>
        <span><fmt:message key="roomClass.label" />: ${roomClassName}</span><br>
        <span><fmt:message key="maxPersons.label" />: ${room.capacity}</span><br>
        <span><fmt:message key="description.label" />: ${room.description}</span><br>
        <h6><fmt:message key="price.label" />: <b>${room.price}</b></h6><br>
        <div class="row">
          <div class="s12">
            <c:forEach items="${images}" var="img">
              <div class="col s4 my-img">
                <img class="materialboxed" width="120" src="${img}" alt="">
              </div>
            </c:forEach>
          </div>
        </div>
        <button class="btn waves-effect waves-light cyan darken-3" type="submit" name="action"><fmt:message key="book.action" />
          <i class="material-icons right">done</i>
        </button>
        <a class="waves-effect waves-light btn red" href="searchRoom"><i
                class="material-icons right">close</i><fmt:message key="close.action" /></a>
      </form>
      <br>
    </div>
  </div>
</div>
<script>
  document.addEventListener('DOMContentLoaded', function () {
    var elems = document.querySelectorAll('.materialboxed');
    var instances = M.Materialbox.init(elems);
  });
</script>
<%@ include file="footer.jsp" %>
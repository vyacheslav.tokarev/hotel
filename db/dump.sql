CREATE DATABASE  IF NOT EXISTS `hotel` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `hotel`;
-- MySQL dump 10.13  Distrib 8.0.30, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: hotel
-- ------------------------------------------------------
-- Server version	8.0.30

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bookings`
--

DROP TABLE IF EXISTS `bookings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bookings` (
  `id` int NOT NULL AUTO_INCREMENT,
  `checkin` date NOT NULL,
  `checkout` date NOT NULL,
  `room_id` int NOT NULL,
  `status` varchar(20) NOT NULL,
  `status_changed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int NOT NULL,
  `price` decimal(5,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_booking_room1_idx` (`room_id`),
  KEY `fk_booking_user1_idx` (`user_id`),
  CONSTRAINT `fk_booking_room1` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_booking_user1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bookings`
--

LOCK TABLES `bookings` WRITE;
/*!40000 ALTER TABLE `bookings` DISABLE KEYS */;
/*!40000 ALTER TABLE `bookings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `room_classes`
--

DROP TABLE IF EXISTS `room_classes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `room_classes` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `room_classes`
--

LOCK TABLES `room_classes` WRITE;
/*!40000 ALTER TABLE `room_classes` DISABLE KEYS */;
INSERT INTO `room_classes` VALUES (1,'Standard'),(2,'Superior'),(3,'Studio'),(4,'Suite');
/*!40000 ALTER TABLE `room_classes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `room_requests`
--

DROP TABLE IF EXISTS `room_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `room_requests` (
  `id` int NOT NULL AUTO_INCREMENT,
  `checkin` date NOT NULL,
  `checkout` date NOT NULL,
  `persons_number` int NOT NULL,
  `room_class_id` int NOT NULL,
  `user_id` int NOT NULL,
  `booking_id` int DEFAULT NULL,
  `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(20) NOT NULL,
  `guest_notes` varchar(1000) DEFAULT NULL,
  `manager_notes` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_application_room_class1_idx` (`room_class_id`),
  KEY `fk_application_user1_idx` (`user_id`),
  KEY `fk_application_booking1_idx` (`booking_id`),
  CONSTRAINT `fk_application_booking1` FOREIGN KEY (`booking_id`) REFERENCES `bookings` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_application_room_class1` FOREIGN KEY (`room_class_id`) REFERENCES `room_classes` (`id`),
  CONSTRAINT `fk_application_user1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `room_requests`
--

LOCK TABLES `room_requests` WRITE;
/*!40000 ALTER TABLE `room_requests` DISABLE KEYS */;
/*!40000 ALTER TABLE `room_requests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rooms`
--

DROP TABLE IF EXISTS `rooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rooms` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `number` varchar(6) NOT NULL,
  `capacity` int NOT NULL,
  `room_class_id` int NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `price` decimal(5,2) NOT NULL DEFAULT '0.00',
  `available` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `number_UNIQUE` (`number`),
  KEY `fk_room_room_class1_idx` (`room_class_id`),
  CONSTRAINT `fk_room_room_class1` FOREIGN KEY (`room_class_id`) REFERENCES `room_classes` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rooms`
--

LOCK TABLES `rooms` WRITE;
/*!40000 ALTER TABLE `rooms` DISABLE KEYS */;
INSERT INTO `rooms` VALUES (1,'Standard double room','201',2,1,'Standard double room',40.00,1),(2,'Standard triple room','202',3,1,'Standard triple room',55.00,1),(3,'Superior double room','203',2,2,'Superior double room',50.00,1),(4,'Superior triple room','301',2,2,'Superior triple room',70.00,1),(5,'Standard double room','302',2,1,'Standard double room',40.00,1),(6,'Standard triple room','303',4,1,'Standard triple room',55.00,1),(7,'Superior double room','304',2,3,'Superior double room',50.00,1),(8,'Superior triple room','305',2,4,'Superior triple room',70.00,1),(9,'Standard double room','402',2,3,'Standard double room',40.00,1),(10,'Standard triple room','403',4,3,'Standard triple room',55.00,1),(11,'Superior double room','404',2,4,'Superior double room',50.00,1),(12,'Superior triple room','405',2,4,'Superior triple room',70.00,1),(13,'Standard double room','502',2,3,'Standard double room',40.00,1),(14,'Standard triple room','503',4,3,'Standard triple room',55.00,1),(15,'Superior double room','504',2,4,'Superior double room',50.00,1),(16,'Superior triple room','505',2,4,'Superior triple room',70.00,1);
/*!40000 ALTER TABLE `rooms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `email` varchar(80) NOT NULL,
  `password` varchar(64) DEFAULT NULL,
  `user_role` varchar(10) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  CONSTRAINT `users_chk_1` CHECK ((`user_role` in (_utf8mb4'GUEST',_utf8mb4'MANAGER',_utf8mb4'ADMIN')))
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (10,'Vyacheslav','Tokarev','vyacheslav.tokarev@hotmail.com','bijEcgZ4O401p6LWe0U6ea9R9sjVExAUTW1rxTYUFHQ','MANAGER','2022-09-25 13:07:07'),(11,'Vyacheslav','Tokarev','vyacheslav.tokarev@hotel.com','bijEcgZ4O401p6LWe0U6ea9R9sjVExAUTW1rxTYUFHQ','ADMIN','2022-10-12 17:55:50'),(22,'Vyacheslav','Tokarev','vyacheslav.tokarev@gmail.com','bijEcgZ4O401p6LWe0U6ea9R9sjVExAUTW1rxTYUFHQ','GUEST','2022-10-15 20:55:26');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-10-20  0:56:45

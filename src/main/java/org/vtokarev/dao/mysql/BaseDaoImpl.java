package org.vtokarev.dao.mysql;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * The abstract {@code BaseDaoImpl} class is the base class for other specific DAO classes.
 * It implements a base functionality used by its subclasses.
 *
 * @author Vyacheslav Tokarev
 */


public abstract class BaseDaoImpl {

    protected final Logger logger =  LogManager.getLogger(getClass());
    protected final DataSource dataSource;

    public BaseDaoImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    protected Connection getConnection(){
        try {
            Connection connection = dataSource.getConnection();
            logger.debug("Success getConnection");
            return connection;
        } catch (SQLException e) {
            logger.error("Failed getConnection", e);
            throw new RuntimeException(e);
        }
    }

    protected void close(AutoCloseable autoCloseable){
        try {
            if(autoCloseable != null){
                autoCloseable.close();
            }
        } catch (Exception e) {
            logger.error("Failed close connection", e);
            throw new RuntimeException(e);
        }
    }
}
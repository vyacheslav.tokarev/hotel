<%@ include file="header.jsp" %>
<br>
<div class="container">
    <div class="carousel carousel-slider z-depth-5">
        <img class="carousel-item" src="img/home/1.jpg"></a>
        <img class="carousel-item" src="img/home/2.jpg"></a>
        <img class="carousel-item" src="img/home/3.jpg"></a>
        <img class="carousel-item" src="img/home/4.jpg"></a>
    </div>
    <p>
    <h3>Hotel</h3>
    The client registers in the system and makes an Application, which indicates the number of
    guests, apartment class and length of stay. The customer can also select a room from the list
    of available rooms and book it.<br>
    An unregistered user cannot leave a request or book a room.<br>
    Implement sorting for the list of rooms:<br>
    - by price;<br>
    - by number of persons;<br>
    - by class;<br>
    - by status (free, booked, busy, unavailable).<br>
    The manager reviews the received applications, selects the most favorite of the available
    numbers and sends a request to the customer to confirm the reservation. The request is
    displayed in the user&#39;s personal account. After the room is booked, the system issues an
    invoice to the customer, which must be paid within two days. If the bill is not paid, the
    reservation is automatically withdrawn.
    </p>
</div>
<script>
    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('.carousel');
        var instances = M.Carousel.init(elems, {
            indicators: true,
        });
    });
</script>
<%@ include file="footer.jsp" %>
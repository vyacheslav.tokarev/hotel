package org.vtokarev.service;

import org.vtokarev.dao.RoomDao;
import org.vtokarev.domain.Room;
import org.vtokarev.dto.RoomEditDto;
import org.vtokarev.dto.RoomSearchDto;
import org.vtokarev.tuples.Pair;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * {@code RoomService} is a service layer class that implements methods for operations with rooms.
 *
 * @author Vyacheslav Tokarev
 */


public class RoomService {

    private final RoomDao roomDao;


    public RoomService(RoomDao roomDao) {
        this.roomDao = roomDao;
    }

    public Optional<Room> getRoomById(long id) {
        return roomDao.get(id);
    }

    public int getMaxCapacity() {
        return roomDao.getMaxCapacity();
    }

    public Pair<List<RoomSearchDto>, String> getRoomsByParams(LocalDate checkin,
                                                              LocalDate checkout,
                                                              boolean showOnlyAvailable,
                                                              Long roomClassId,
                                                              Integer personsNumber,
                                                              String orderBy,
                                                              Integer limit,
                                                              Integer offset) {
        orderBy = switch (orderBy) {
            case "Name" -> "name";
            case "Capacity" -> "capacity";
            case "Status" -> "booked";
            case "Class" -> "room_class_name";
            default -> "price";
        };

        String result;

        if (checkin == null || checkout == null) {
            result = "incorrect.checkin.checkout.message"; //Incorrect check-in or check-out dates
            return new Pair<>(new ArrayList<>(), result);
        }

        if (checkin.isEqual(checkout) || checkin.isAfter(checkout)) {
            result = "checkin.must.be.less.than.checkout.message"; //Check-in must be less than check-out date
            return new Pair<>(new ArrayList<>(), result);
        }

        if (checkin.isBefore(LocalDate.now())) {
            result = "checkin.cannot.be.in.the.past.message"; //The check-in date cannot be in the past. The earliest check-in date is today
            return new Pair<>(new ArrayList<>(), result);
        }

        Pair<List<RoomSearchDto>, String> returnObj = new Pair<>();
        List<RoomSearchDto> rooms = roomDao.getBookingRooms(checkin, checkout, showOnlyAvailable, roomClassId, personsNumber, orderBy, limit, offset);

        if (rooms.isEmpty()) {
            result = "empty";
        } else {
            result = "success";
        }

        returnObj.set(rooms, result);
        return returnObj;
    }

    public int getBookingRoomsCount(LocalDate checkin, LocalDate checkout, boolean showOnlyAvailable, Long roomClassId, Integer personsNumber) {
        return roomDao.getBookingRoomsCount(checkin, checkout, showOnlyAvailable, roomClassId, personsNumber);
    }

    public List<RoomEditDto> getAllRoomsEditDto() {
        return roomDao.getAllRoomsEditDto();
    }

    public List<Pair<String,String>> getImagePaths(Room room) {
        return roomDao.getImagePaths(room);
    }

    public void delete(long id) {
        Optional<Room> room = getRoomById(id);
        room.ifPresent(roomDao::delete);
    }

    public Optional<Room> getByNumber(String number) {
        return roomDao.getByNumber(number);
    }

    public String getEditErrorMessage(long id, String number) {
        Optional<Room> room = getByNumber(number.trim());
        return room.isPresent() && room.get().getId() != id ? "roomNumberIsInUse.message" : "";
    }

    public String getCreateErrorMessage(String number) {
        Optional<Room> room = getByNumber(number.trim());
        return room.isPresent() ? "roomNumberIsInUse.message" : "";
    }

    public void update(Room room) {
        roomDao.update(room);
    }

    public void create(Room room) {
        roomDao.create(room);
    }

    public String getImageRealPathDirectory(Room room){
        return roomDao.getImageRealPathDirectory(room);
    }

    public String deleteImage(String fileName){
        try {
            Files.delete(Paths.get(fileName));
        } catch (Exception e) {
            return "unable.file.delete.message";
        }
        return "";
    }


}
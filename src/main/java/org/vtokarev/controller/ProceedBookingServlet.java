package org.vtokarev.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.vtokarev.domain.BookingStatus;
import org.vtokarev.service.BookingService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.invoke.MethodHandles;

/**
 * {@code ProceedBookingServlet} class uses for proceed {@link org.vtokarev.domain.Booking} i.e. pay or cancel it by GUEST.
 * In the {@code doPost} method it implements pay and cancel operations for the booking. After the operation is completed, it forwards the control to {@code bookingList}.
 *
 * @author Vyacheslav Tokarev
 */

@WebServlet(urlPatterns = "/proceedBooking", name = "proceedBookingServlet")
public class ProceedBookingServlet extends HttpServlet {

    private final static Logger LOGGER = LogManager.getLogger(MethodHandles.lookup().lookupClass());

    private BookingService bookingService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        bookingService = (BookingService) config.getServletContext().getAttribute("app/BookingService");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        int bookingId;
        BookingStatus bookingStatus;

        if(req.getParameter("pay") == null && req.getParameter("cancel") == null) {
            resp.sendRedirect("bookingList");
            return;
        }

        bookingStatus = req.getParameter("pay") != null ? BookingStatus.BOOKED : BookingStatus.CANCELLED;

        if(req.getParameter("bookingId") != null){
            try {
                bookingId = Integer.parseInt(req.getParameter("bookingId"));
                bookingService.updateBookingStatusById(bookingId, bookingStatus);
            } catch (NumberFormatException e) {
                LOGGER.error("Failed to parse bookingId param, doPost");
            }
        }
        resp.sendRedirect("bookingList");
    }
}
